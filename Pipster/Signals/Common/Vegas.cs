﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;
using PipsterEA.Service;

namespace Signals.Common
{
    public class Vegas
    {
        private static int START_TRADE_AT = 200;
        public static int LeftBar = 3;

        private IndicatorService indicatorService = new IndicatorService();
        private Pipster Pipster { get; set; }

        private static AppliedPrice MA_APPLIED_PRICE = AppliedPrice.CLOSE;
        //public static int H4_MA_PERIOD = 144;  
        private static int MA_PERIOD = 89;
        private static MovingAverageType MA_TYPE = MovingAverageType.SMOOTHED;

        private static AppliedPrice SIGNAL_APPLIED_PRICE = AppliedPrice.CLOSE;
        private static int SIGNAL_PERIOD = 8;
        private static MovingAverageType SIGNAL_TYPE = MovingAverageType.EXPONENTIAL;

     

        private static int MACD_FAST_EMA = 244;
        private static int MACD_SLOW_EMA = 630;
        private static int MACD_SIGNAL = 13;

        public List<Double> Ma { get; set; }
      
        public List<Double> Signals { get; set; }
        public Double MacdSignal { get; set; }
        public Double Macd { get; set; }

        public Vegas(Pipster pipster)
        {
            Pipster = pipster;
          //  SignalsW1 = new List<Double>();
            Signals = new List<Double>();
            Ma = new List<Double>();
        }

       
        public void Refresh()
        {
           // SignalsW1.Clear();
            Signals.Clear();
            Ma.Clear();
                        
            for (int i = 0; i < LeftBar; i++)
            {
                Signals.Add(indicatorService.MovingAverage(Pipster.Symbol, TimeFrame.PERIOD_D1, SIGNAL_PERIOD, 0, SIGNAL_TYPE, SIGNAL_APPLIED_PRICE,i));

                Ma.Add(indicatorService.MovingAverage(Pipster.Symbol, TimeFrame.PERIOD_D1, MA_PERIOD, 0, MA_TYPE, MA_APPLIED_PRICE, i));
            }

            Macd = indicatorService.Macd(Pipster.Symbol, TimeFrame.PERIOD_D1, MACD_FAST_EMA, MACD_SLOW_EMA, MACD_SIGNAL, AppliedPrice.CLOSE, IndicatorLine.MAIN, 1);
            MacdSignal = indicatorService.Macd(Pipster.Symbol, TimeFrame.PERIOD_D1, MACD_FAST_EMA, MACD_SLOW_EMA, MACD_SIGNAL, AppliedPrice.CLOSE, IndicatorLine.SIGNAL, 1);      
        }

        public bool isMacdUptrend()
        {
            if (Macd > 0 && Macd > MacdSignal)
            {
                return true;
            }
            if (Macd < 0 && Macd > MacdSignal)
            {
                return true;
            }
            return false;
        }

        public bool isMacdDownTrend()
        {
            if (Macd < 0 && Macd < MacdSignal)
            {
                return true;
            }
            if (Macd > 0 && Macd < MacdSignal)
            {
                return true;
            }
            return false;
        }

        public bool IsUpTrend()
        {
            if (IsStandby())
            {
                return false;
            }
           // if (SignalsW1[0] > SignalsW1[1] && SignalsW1[1] > SignalsW1[2])
            if(Ma[0]>Ma[1] && Ma[1]>Ma[2] && isMacdUptrend())
            {
                return true;
            }

            return false;
        }

        public bool IsDownTrend()
        {
            if (IsStandby())
            {
                return false;
            }
            //if (SignalsW1[0] < SignalsW1[1] && SignalsW1[1] < SignalsW1[2])
            if (Ma[0] < Ma[1] && Ma[1] < Ma[2] && isMacdDownTrend())
            {
                return true;
            }

            return false;
        }

        public bool IsBuySignal()
        {                       
            if (Signals[0] > Signals[1])
            {
                return true;
            }

            return false;
        }

        public bool IsStrongBuySignal()
        {

            if (Signals[0] > Signals[1] && Signals[1] < Signals[2])            
            {
                return true;
            }

            return false;
        }

        public bool IsSellSignal()
        {            
            if (Signals[0] < Signals[1])
            {
                return true;
            }

            return false;
        }

        public bool IsStrongSellSignal()
        {
            if (Signals[0] < Signals[1] && Signals[1] > Signals[2])          
            {
                return true;
            }

            return false;
        }


        public Boolean IsStandby()
        {
            return false;
            double difference = Ma[0] - Signals[0];
            if (difference > 0)
            {
                if (difference < Pipster.Point*START_TRADE_AT)
                {
                    return true;
                }
            }

            if (difference < 0)
            {
                if (difference > -Pipster.Point * START_TRADE_AT)
                {
                    return true;
                }
            }

            return false;
        }                

    }
}
