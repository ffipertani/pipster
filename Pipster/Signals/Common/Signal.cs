﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Signals.Common
{
    public class Signal
    {
        public String Symbol { get; set; }
        public String Action { get; set; }
        public Double EntryPoint { get; set; }
        public Double StopLoss { get; set; }
        public Double TakeProfit { get; set; }
        public String Status { get; set; }
        public String Comments { get; set; }

        public Signal()
        {
        }
    }
}
