﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace Signals.CloseSignals
{
    public class StochasticClose:CloseSignal
    {
        public int FromPips { get; set; }
        public TimeFrame TimeFrame{get;set;}
        public int KPeriod { get; set; }
        public int DPeriod { get; set; }
        public int Slowing { get; set; }
        public Double Value { get; set; }
        public Double Signal { get; set; }         
        

        public StochasticClose():this(TimeFrame.PERIOD_D1,5,3,3){}

        public StochasticClose(TimeFrame timeFrame, int kperiod, int dperiod, int slowing)
        {
            TimeFrame = timeFrame;
            KPeriod = kperiod;
            DPeriod = dperiod;
            Slowing = slowing;
        }
        

        
        
        public override double CloseLots(Order order)
        {
            Value = indicatorService.Stochastic(Pipster.Symbol, TimeFrame, KPeriod, DPeriod, Slowing, MovingAverageType.SIMPLE, PriceField.LOW_HIGH, IndicatorLine.MAIN, 0);
            Signal = indicatorService.Stochastic(Pipster.Symbol, TimeFrame, KPeriod, DPeriod, Slowing, MovingAverageType.SIMPLE, PriceField.LOW_HIGH, IndicatorLine.SIGNAL, 0);

            if (order.Type == TradeOperation.BUY)
            {
                if (Value < Signal)
                {                     
                    return order.Lots;
                }
            }
            else if (order.Type == TradeOperation.SELL)
            {
                if (Value > Signal)
                {                    
                    return order.Lots;
                }
            }
           
            return 0;
        }

       
    }
}
