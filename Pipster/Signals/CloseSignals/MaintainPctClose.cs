﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace Signals.CloseSignals
{
    public class MaintainPctClose:CloseSignal
    {
        public int FromPips { get; set; }
        public int MaintainPct { get; set; }
        private Dictionary<int, Double> managedTickets = new Dictionary<int, Double>();

        public MaintainPctClose() : this(0, 0) { }
         
        public MaintainPctClose(int fromPips, int maintainPct)
        {
            FromPips = fromPips;
            MaintainPct = maintainPct;                                        
        }

        public override void Init()
        {
            Pipster.OrderClosed += new Pipster.OrderClosedEvent(Pipster_OrderClosed);            
        }

        public void Pipster_OrderClosed(Order order)
        {
            managedTickets.Remove(order.Ticket);
        }

        public override double CloseLots(Order order)
        {          
            Double pips = Pipster.CountPips(order);
            
            if (!isManaged(order))
            {
                if (pips >= FromPips)
                {
                    managedTickets.Add(order.Ticket,pips);
                }
            }

            if (isManaged(order))
            {
                Double max;
                managedTickets.TryGetValue(order.Ticket, out max);

                if (pips > max)
                {
                    managedTickets.Remove(order.Ticket);
                    managedTickets.Add(order.Ticket, pips);
                }

                if (pips < max * MaintainPct / 100)
                {
                    return order.Lots;
                }
               
            }

           
            return 0;
        }
        
       

        private Boolean isManaged(Order order)
        {            
            if (managedTickets.ContainsKey(order.Ticket))
            {
                return true;
            }
            
            return false;
        }
         

    }
}