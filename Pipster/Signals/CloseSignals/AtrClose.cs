﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace Signals.CloseSignals
{
    public class AtrClose : CloseSignal
    {
        public Int32 Days { get; set; }
        
        
        public override double CloseLots(Order order)
        {
            //Double atr = indicatorService.Atr(Pipster.Symbol, TimeFrame.PERIOD_D1, Days, 0);
            Double max = indicatorService.MediumMaxMovement(Pipster.Symbol, TimeFrame.PERIOD_D1, Days);
            Double min = indicatorService.MediumMinMovement(Pipster.Symbol, TimeFrame.PERIOD_D1, Days);
            max = max / Pipster.Point;
            min = min / Pipster.Point;

            Double pips = Pipster.CountPips(order);
         
            //if (Math.Abs(pips) >= atr )
            if (pips >= max )          
            {                
                return order.Lots;
            }
             
            if (pips < -min)
            // if (pips >= atr / 2)          
            {
                return order.Lots;
            }
            /*
            if (order.Type == TradeOperation.BUY)
            {
                Double stoploss = order.OpenPrice - (min * Pipster.Point);
                if (order.StopLoss != stoploss)
                {
                    order.StopLoss = stoploss;
                    tradingService.EditOrder(order);
                }
            }
            else if (order.Type == TradeOperation.SELL)
            {
                Double stoploss = order.OpenPrice + (min * Pipster.Point);
                if (order.StopLoss != stoploss)
                {
                    order.StopLoss = stoploss;
                    tradingService.EditOrder(order);
                }
            }
            */
            return 0;
        }
    }
}
