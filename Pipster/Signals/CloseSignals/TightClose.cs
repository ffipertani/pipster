﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace Signals.CloseSignals
{
    public class TightClose:CloseSignal
    {
        public TimeFrame TimeFrame { get; set; }
        public int BackPeriods { get; set; }
        public int ExtraMargin { get; set; }

        Dictionary<int, Double> stops = new Dictionary<int, double>();

        public TightClose() : this(TimeFrame.PERIOD_D1, 3,10) { }

        public TightClose(TimeFrame timeFrame, int count, int extraMargin)
        {
            TimeFrame = timeFrame;
            BackPeriods = count;
            ExtraMargin = extraMargin;
        }
        

        public override void Init()
        {
            Pipster.OrderClosed += new Pipster.OrderClosedEvent(Pipster_OrderClosed);
        }

        public void Pipster_OrderClosed(Order order)
        {
            stops.Remove(order.Ticket);
        }

        public override double CloseLots(Order order)
        {
            Double pips = Pipster.CountPips(order);

            if (!isManaged(order))
            {
                if (order.Type == TradeOperation.BUY)
                {
                    Double lowest = indicatorService.Lowest(Pipster.Symbol, TimeFrame, SeriesType.LOW, BackPeriods, 0);
                    if (lowest < 0)
                    {
                        lowest = 0;
                    }
                    stops.Add(order.Ticket, lowest);                  
                }
                else if (order.Type == TradeOperation.SELL)
                {
                    Double highest = indicatorService.Highest(Pipster.Symbol, TimeFrame, SeriesType.HIGH, BackPeriods, 0);
                    stops.Add(order.Ticket, highest);
                }               
            }

            if (isManaged(order))
            {
                if (order.Type == TradeOperation.BUY)
                {
                    Double lowest = 0;
                    stops.TryGetValue(order.Ticket, out lowest);                     
                    if (Pipster.Ask <= lowest - ExtraMargin * Pipster.Point)
                    {
                        return order.Lots;
                    }
                }
                else if (order.Type == TradeOperation.SELL)
                {
                    Double highest = 0;
                    stops.TryGetValue(order.Ticket, out highest);    
                    if (Pipster.Ask >= highest + ExtraMargin * Pipster.Point)
                    {
                        return order.Lots;
                    }
                }
            }


            return 0;
        }



        private Boolean isManaged(Order order)
        {
            foreach (int ticket in stops.Keys)
            {
                if (ticket == order.Ticket)
                {
                    return true;
                }

            }
            return false;
        }
      

    }
}
