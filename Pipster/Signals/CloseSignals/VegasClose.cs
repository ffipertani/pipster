﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using Signals.Common;
using PipsterEA.Model;

namespace Signals.CloseSignals
{
    public class VegasClose:CloseSignal
    {
        private Vegas Vegas { get; set; }

        

        public override void Init()
        {
            Vegas = new Vegas(Pipster);
        }

        public override double CloseLots(Order order)
        {
            Vegas.Refresh();

            if (order.Type == TradeOperation.BUY)
            {
                if (Vegas.IsStrongSellSignal())
                {
                    return order.Lots;
                }
            }
            if (order.Type == TradeOperation.SELL)
            {
                if (Vegas.IsStrongBuySignal())
                {
                    return order.Lots;
                }
            }
            return 0;
        }

    }
}
