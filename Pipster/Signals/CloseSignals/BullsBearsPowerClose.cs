﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace Signals.CloseSignals
{
    public class BullsBearsPowerClose:CloseSignal
    {
        private static int CLOSE_AFTER = -0;
        public TimeFrame TimeFrame { get; set; }
        public int Period { get; set; }
        public AppliedPrice AppliedPrice { get; set; }

        public BullsBearsPowerClose() : this(TimeFrame.PERIOD_D1, 14, AppliedPrice.TYPICAL) { }

        public BullsBearsPowerClose(TimeFrame timeFrame, int period, AppliedPrice appliedPrice)
        {
            TimeFrame = timeFrame;
            Period = period;
            AppliedPrice = appliedPrice;
        }
        public override double CloseLots(Order order)
        {
            double bulls = indicatorService.BullsPower(Pipster.Symbol, TimeFrame, Period, AppliedPrice, 0);
            double bears = indicatorService.BearsPower(Pipster.Symbol, TimeFrame, Period, AppliedPrice, 0);
            

            if (order.Type == TradeOperation.BUY)
            {
                if (bulls < CLOSE_AFTER * Pipster.Point)
                {
                    return order.Lots;
                }
            }

            if (order.Type == TradeOperation.SELL)
            {
                if (bears > -CLOSE_AFTER * Pipster.Point)
                {
                    return order.Lots;
                }
            }
            return 0;
        }
    }
}
