﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace Signals.CloseSignals
{
    public class MediumMaDifferenceClose:CloseSignal
    {
        private Double difference;
        private DateTime lastCheck;

        public AppliedPrice SlowAppliedPrice { get; set; }
        public MovingAverageType SlowType { get; set; }
        public int SlowPeriod { get; set; }

        public AppliedPrice FastAppliedPrice { get; set; }
        public MovingAverageType FastType { get; set; }
        public int FastPeriod { get; set; }
        public int BackPeriods{get;set;}
        public TimeFrame TimeFrame { get; set; }

        public MediumMaDifferenceClose() : this(TimeFrame.PERIOD_D1, AppliedPrice.CLOSE, MovingAverageType.SIMPLE, 10, AppliedPrice.CLOSE, MovingAverageType.SIMPLE, 20, 20) { }

        public MediumMaDifferenceClose(TimeFrame timeFrame, AppliedPrice slowAppliedPrice, MovingAverageType slowType, int slowPeriod,
            AppliedPrice fastAppliedPrice, MovingAverageType fastType, int fastPeriod, int count)
        {
            BackPeriods = count;
            TimeFrame = timeFrame;
            SlowAppliedPrice = slowAppliedPrice;
            SlowType = slowType;
            SlowPeriod = slowPeriod;
            FastAppliedPrice = fastAppliedPrice;
            FastType = fastType;
            FastPeriod = fastPeriod;
        }

        private void calcDifference()
        {
            difference = 0;
            Double sum = 0;
            for (int i = 0; i < BackPeriods; i++)
            {
                Double fast = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, FastPeriod, 0, FastType, FastAppliedPrice, i);
                Double slow = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, SlowPeriod, 0, SlowType, SlowAppliedPrice, i);
                difference = Math.Abs(fast - slow);

                sum += difference;
            }

            difference = sum / BackPeriods;
            lastCheck = Pipster.Now;
        }
       
        public override double CloseLots(Order order)
        {
             DateTime initBar = tradingService.Time(Pipster.Symbol, TimeFrame, 0);

            if(lastCheck==null){
                lastCheck = Pipster.Now;
            }

            if (lastCheck < initBar)
            {
                calcDifference();
            }
            
           // difference *= Pipster.Point;


            if (order.Type == TradeOperation.BUY)
            {
                
                if(Pipster.Bid < order.OpenPrice - difference)
                {
                    return order.Lots;
                }
                
            }
            else if (order.Type == TradeOperation.SELL)
            {
                 
                if (Pipster.Ask > order.OpenPrice + difference)
                {
                    return order.Lots;
                }
            }

           
            return 0;
        }
        
      

    }
}
