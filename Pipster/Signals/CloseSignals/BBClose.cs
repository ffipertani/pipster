﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace Signals.CloseSignals
{
    public class BBClose : CloseSignal
    {
        public int StartAfterPips { get; set; }
        public TimeFrame TimeFrame { get; set; }
        public int Period { get; set; }
        public AppliedPrice AppliedPrice { get; set; }
        
        private List<int> managedTickets = new List<int>();
       
        public BBClose() : this(TimeFrame.PERIOD_D1, 53, AppliedPrice.CLOSE,10) { }

        public BBClose(TimeFrame timeFrame, int period, AppliedPrice appliedPrice, int startAfterPips)
        {
            TimeFrame = timeFrame;
            Period = period;
            StartAfterPips = startAfterPips;            
            AppliedPrice = appliedPrice;
        }

        public override void Init()
        {
            Pipster.OrderClosed += new Pipster.OrderClosedEvent(Pipster_OrderClosed);
        }
        public void Pipster_OrderClosed(Order order)
        {
            managedTickets.Remove(order.Ticket);
        }

        public override double CloseLots(Order order)
        {

            Double Ma, Top, Bottom, Signal;
            Ma = indicatorService.BollingerBands(Pipster.Symbol, TimeFrame, Period, 1, 0, AppliedPrice, IndicatorLine.MAIN, 0);
            Signal = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, 2, 0, MovingAverageType.LINEAR_WEIGHTED, AppliedPrice, 0);
            Top = indicatorService.BollingerBands(Pipster.Symbol, TimeFrame, Period, 1, 0, AppliedPrice, IndicatorLine.MODE_UPPER, 0);
            Bottom = indicatorService.BollingerBands(Pipster.Symbol, TimeFrame, Period, 1, 0, AppliedPrice, IndicatorLine.MODE_LOWER, 0);

            Double pips = Pipster.CountPips(order);

            if (!isManaged(order))
            {
                if (pips >= StartAfterPips)
                {
                    managedTickets.Add(order.Ticket);
                }
            }

            if (isManaged(order))
            {
                if (order.Type == TradeOperation.BUY)
                {
                    if (Signal < Top)
                    {
                        return order.Lots;
                    }
                }
                else if (order.Type == TradeOperation.SELL)
                {
                    if (Signal > Bottom)
                    {
                        return order.Lots;
                    }
                }
            }


            return 0;
        }

        private Boolean isManaged(Order order)
        {
            foreach (int ticket in managedTickets)
            {
                if (ticket == order.Ticket)
                {
                    return true;
                }

            }
            return false;
        }

        
    }
}
