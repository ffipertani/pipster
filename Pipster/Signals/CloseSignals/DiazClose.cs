﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace Signals.CloseSignals
{
    public class DiazClose:CloseSignal
    {
        public TimeFrame TimeFrame { get; set; }
        
        public override double CloseLots(Order order)
        {
            Double Signal = indicatorService.Macd(Pipster.Symbol, TimeFrame, 8, 50, 9, AppliedPrice.CLOSE, IndicatorLine.SIGNAL, 0);
            Double Value = indicatorService.Macd(Pipster.Symbol, TimeFrame, 8, 50, 9, AppliedPrice.CLOSE, IndicatorLine.MAIN, 0);

            if (order.Type == TradeOperation.BUY)
            {
                if (Signal > Value)
                {
                    return order.Lots;
                }
            }

            if (order.Type == TradeOperation.SELL)
            {
                if (Signal < Value)
                {
                    return order.Lots;
                }
            }
            return 0;
        }

        
    }
}
