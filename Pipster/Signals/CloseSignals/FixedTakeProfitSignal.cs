﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace Signals.CloseSignals
{
    public class FixedTakeProfitSignal:CloseSignal
    {        
        public int Pips { get; set; }

        public FixedTakeProfitSignal() : this(20) { }

        public FixedTakeProfitSignal(int pips)          
        {
            Pips = pips;
        }
               
        public override Double CloseLots(Order order)
        {
            Double pips = Pipster.CountPips(order);
            if (pips > Pips)
            {                
                return order.Lots;
            }
            return 0;
        }
    }
}
