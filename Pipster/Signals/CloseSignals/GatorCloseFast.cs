﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace Signals.CloseSignals
{
    public class GatorCloseFast : CloseSignal
    {
        public TimeFrame TimeFrame { get; set; }
        public GatorCloseFast(TimeFrame timeFrame)
        {
            TimeFrame = timeFrame;
        }
        public override double CloseLots(Order order)
        {
            double up = indicatorService.Gator(Pipster.Symbol, TimeFrame, 5, 1, 3, 1, 3, 1, MovingAverageType.SMOOTHED, AppliedPrice.MEDIAN, IndicatorLine.MODE_UPPER, 0);
            double down = indicatorService.Gator(Pipster.Symbol, TimeFrame, 5, 1, 3, 1, 3, 1, MovingAverageType.SMOOTHED, AppliedPrice.MEDIAN, IndicatorLine.MODE_LOWER, 0);

            if (order.Type == TradeOperation.BUY)
            {
                if (up + down < 0)
                {
                    return order.Lots;
                }
            }

            if (order.Type == TradeOperation.SELL)
            {
                if (up + down > 0)
                {
                    return order.Lots;
                }
            }
            return 0;
        }
    }
}
