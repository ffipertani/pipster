﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace Signals.CloseSignals
{
    public class MinMaxClose:CloseSignal
    {        
        public int BackPeriods{get;set;}
        public TimeFrame TimeFrame { get; set; }

        public MinMaxClose() : this(TimeFrame.PERIOD_D1, 3) { }

        public MinMaxClose(TimeFrame timeFrame, int count)
        {
            BackPeriods = count;
            TimeFrame = timeFrame;                  
        }
       
        public override double CloseLots(Order order)
        {                      
            if (order.Type == TradeOperation.BUY)
            {
                Double lowest = indicatorService.Lowest(Pipster.Symbol, TimeFrame, SeriesType.LOW, BackPeriods, 1);
                if(Pipster.Bid < lowest)
                {
                    return order.Lots;
                }
                
            }
            else if (order.Type == TradeOperation.SELL)
            {
                Double highest = indicatorService.Highest(Pipster.Symbol, TimeFrame, SeriesType.HIGH, BackPeriods, 1);
                if (Pipster.Ask > highest)
                {
                    return order.Lots;
                }
            }

           
            return 0;
        }
        
      

    }
}
