﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace Signals.CloseSignals
{
    public class DreamClose:CloseSignal
    {
        private int TakeProfit { get; set; }
        private int StopLoss { get; set; }        

        public override double CloseLots(Order order)
        {
            /*
            if (isNewDay(order))
            {
                return order.Lots;
            }
             * */

            if (!isNewHour())
            {
                return 0;
            }


            if (isCameBack(order))
            {
                return order.Lots;
            }
           

            Double atr = indicatorService.Atr(Pipster.Symbol, TimeFrame.PERIOD_D1, 34, 0);
           
            
            int atrPips = getPips(atr);
            //openAfterPips = openAfterPips / 2;
            TakeProfit = atrPips;
            StopLoss = atrPips/3;

            Double pips = Pipster.CountPips(order);
            if (pips > TakeProfit)
            {                
                return order.Lots;
            }
            /*
            if (pips < -StopLoss)
            {
                return order.Lots;
            }
              */
            return 0;
        }

        private Boolean isCameBack(Order order)
        {
           Double price = indicatorService.Open(Pipster.Symbol, TimeFrame.PERIOD_D1, 0);
           if (order.Type == TradeOperation.BUY)
           {
               if (Pipster.Ask <= price)
               {
                   return true;
               }
           }
           if (order.Type == TradeOperation.SELL)
           {
               if (Pipster.Bid >= price)
               {
                   return true;
               }
           }
           
           return false;
        }

        private Boolean isNewHour()
        {
            DateTime now = Pipster.Now;
            if (now.Hour ==0)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

        private Boolean isNewDay(Order order)
        {
            DateTime now = Pipster.Now;            
            if ( now.Day != order.OpenTime.Day)
            {              
                return true;
            }
            else
            {               
                return false;
            }           
        }

        private int getPips(Double price)
        {
            String points = (price / Pipster.Point).ToString();
            if (points.IndexOf(",") > 0)
            {
                points = points.Substring(0, points.IndexOf(","));
            }
           return int.Parse(points);
        }
    }
}
