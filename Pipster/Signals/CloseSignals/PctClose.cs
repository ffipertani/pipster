﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace Signals.CloseSignals
{
    public class PctClose : CloseSignal
    {
        public Int32 Parts { get; set; }
        public Int32 Pips { get; set; }

        private List<int> Dones { get; set; }
        

        public override void Init()
        {
            Dones = new List<int>();             
        }

        private Boolean isDone(Order order)
        {          
            if (Dones.Contains(order.Ticket))
            {
                return true;
            }
            if (Dones.Contains(order.GetCreatedFrom()))
            {
                Dones.Add(order.Ticket);
                return true;
                
            }
            return false;
        }
        
        public override double CloseLots(Order order)
        {
            if (isDone(order))
            {
                return 0;
            }
            Double pips = Pipster.CountPips(order);
            if (pips >= Pips)
            {
                Double lots = order.Lots / Parts ;
                if(lots<0.01)
                {
                    lots = 0.01;
                }
                if (lots > 0)
                {
                    Dones.Add(order.Ticket);
                }
                return lots;
            }
            return 0;
        }
    }
}
