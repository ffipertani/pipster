﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace Signals.OpenSignals
{
    public class AllowDifferentOpen:OpenSignal
    {
        
        public override void RefreshData()
        {
            
        }
                
        public override Boolean CanBuy()
        {
            foreach (Order order in Pipster.ManagedOrders)
            {
                if (order.Type == TradeOperation.BUY)
                {
                    return false;
                }
            }
            return true;
        }


        public override Boolean CanSell()
        {
            foreach (Order order in Pipster.ManagedOrders)
            {
                if (order.Type == TradeOperation.SELL)
                {
                    return false;
                }
            }
            return true;
        }

     

    }
}
