﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;
using System.Xml.Serialization;

namespace Signals.OpenSignals
{
    [XmlInclude(typeof(MaxMinBreakout))]
    public class MaxMinBreakout : OpenSignal
    {        
        public TimeFrame TimeFrame { get; set; }
        public int BackPeriods { get; set; }
        public int ExtraMargin { get; set; }
        private Double Max { get; set; }
        private Double Min { get; set; }

        public MaxMinBreakout():this(TimeFrame.PERIOD_D1,3,2)
        {
        }

        public MaxMinBreakout(TimeFrame timeFrame, int count, int extraMargin)
        {
            TimeFrame = timeFrame;
            BackPeriods = count;
            ExtraMargin = extraMargin;
        }

        public override void RefreshData()
        {
            Max = indicatorService.Highest(Pipster.Symbol, TimeFrame, SeriesType.HIGH, BackPeriods, 1);
            Min = indicatorService.Lowest(Pipster.Symbol, TimeFrame, SeriesType.LOW, BackPeriods, 1);
        }

        public override Boolean CanBuy()
        {
            if (Pipster.Bid > Max + (ExtraMargin*Pipster.Point) )
            {
                return true;
            }
            return false;
        }


        public override Boolean CanSell()
        {
            if (Pipster.Bid - (Pipster.Spread * Pipster.Point) < Min - (ExtraMargin * Pipster.Point))
            {
                return true;
            }
            return false;
        }
    }
}
