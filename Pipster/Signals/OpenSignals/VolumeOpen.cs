﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace Signals.OpenSignals
{
    public class VolumeOpen : OpenSignal
    {
        public TimeFrame TimeFrame { get; set; }
        public int FromVolume { get; set; }
        Double Volume;

        public override void RefreshData()
        {
            Volume = indicatorService.Volume(Pipster.Symbol, TimeFrame, 0);
        }

        public override Boolean CanBuy()
        {
            if (Volume > FromVolume)
            {
                return true;
            }
            return false;
        }


        public override Boolean CanSell()
        {
            if (Volume > FromVolume)
            {
                return true;
            }
            return false;
        }
    }
}
