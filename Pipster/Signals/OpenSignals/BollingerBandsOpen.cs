﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;
using Signals.CloseSignals;

namespace Signals.OpenSignals
{
    public class BollingerBandsOpen:OpenSignal
    {
        public static int LeftBar = 3;
       
       
        public TimeFrame TimeFrame { get; set; }
        public int Period { get; set; }
        public int Deviation { get; set; }
        public int BandShift { get; set; }
        public AppliedPrice AppliedPrice { get; set; }
        
        private List<Double> Up { get; set; }
        private List<Double> Down { get; set; }
        private Double Price { get; set; }
        private List<Double> Max { get; set; }
        private List<Double> Min { get; set; }

       
        public BollingerBandsOpen() : this(TimeFrame.PERIOD_D1, 14, 2, 0, AppliedPrice.CLOSE) { }

        public BollingerBandsOpen(TimeFrame timeFrame, int period, int deviation, int band_shift, AppliedPrice appliedPrice)
        {
            TimeFrame = timeFrame;
            Period = period;
            Deviation = deviation;
            BandShift = band_shift;
            AppliedPrice = appliedPrice;

            
            Up = new List<Double>();
            Down = new List<Double>();
            Max = new List<Double>();
            Min = new List<Double>();
        }


       
        public override void RefreshData()
        {
            Up.Clear();
            Down.Clear();
            Max.Clear();
            Min.Clear();
            
            for (int i = 0; i < LeftBar; i++)
            {
                Up.Add(indicatorService.BollingerBands(Pipster.Symbol, TimeFrame, Period, Deviation, BandShift, AppliedPrice, IndicatorLine.MODE_UPPER, i));
                Down.Add(indicatorService.BollingerBands(Pipster.Symbol, TimeFrame, Period, Deviation, BandShift, AppliedPrice, IndicatorLine.MODE_LOWER, i));
                
                Max.Add(indicatorService.Lowest(Pipster.Symbol, TimeFrame, SeriesType.HIGH, 1, i));
                Min.Add(indicatorService.Lowest(Pipster.Symbol, TimeFrame, SeriesType.LOW, 1, i));               
            }
            
        }

        public override Boolean CanBuy()
        {         
            Boolean wasDown = false;
            for(int i=0;i<LeftBar;i++){
                if (Min[i] < Down[i])
                {
                    wasDown = true;
                    break;
                }
            }

            if (wasDown && Pipster.Ask > Down[0] &&  Up[0] - Pipster.Ask > BollingerBandsClose.GAP*2*Pipster.Point)
            {
                return true;
            }
            return false;
        }


        public override Boolean CanSell()
        {
            Boolean wasUp = false;
            for (int i = 0; i < LeftBar; i++)
            {
                if (Max[i] > Up[i])
                {
                    wasUp = true;
                    break;
                }
            }

            if (wasUp && Pipster.Bid < Up[0] && Pipster.Bid - Down[0] > BollingerBandsClose.GAP*2*Pipster.Point)
            {
                return true;
            }
            return false;
        }
    }
}
