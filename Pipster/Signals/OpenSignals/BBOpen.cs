﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Service;
using PipsterEA.Model;
using PipsterEA.Core;

namespace Signals.OpenSignals
{
    public class BBOpen:OpenSignal
    {              
        public TimeFrame TimeFrame{get;set;}
        public int Period{get;set;}        
        public AppliedPrice AppliedPrice{get;set;}
         

        Double Ma, Top, Bottom;
        Double LastMa, LastTop, LastBottom;
        Double Signal,LastSignal;

        public BBOpen() : this(TimeFrame.PERIOD_D1, 150,AppliedPrice.WEIGHTED) { }

        public BBOpen(TimeFrame timeFrame, int period,AppliedPrice appliedPrice)
        {            
            TimeFrame = timeFrame;
            Period = period;           
            AppliedPrice = appliedPrice;             
        }

        public override void RefreshData()
        {
            Ma = indicatorService.BollingerBands(Pipster.Symbol, TimeFrame, Period, 1, 0, AppliedPrice, IndicatorLine.MAIN, 0);
            LastMa = indicatorService.BollingerBands(Pipster.Symbol, TimeFrame, Period, 1, 0, AppliedPrice, IndicatorLine.MAIN, 1);
            Signal = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, 2, 0, MovingAverageType.LINEAR_WEIGHTED, AppliedPrice, 0);
            LastSignal = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, 2, 0, MovingAverageType.LINEAR_WEIGHTED, AppliedPrice, 1);

            Top = indicatorService.BollingerBands(Pipster.Symbol, TimeFrame, Period, 1, 0, AppliedPrice, IndicatorLine.MODE_UPPER, 0);
            Bottom = indicatorService.BollingerBands(Pipster.Symbol, TimeFrame, Period, 1, 0, AppliedPrice, IndicatorLine.MODE_LOWER, 0);
            LastTop = indicatorService.BollingerBands(Pipster.Symbol, TimeFrame, Period, 1, 0, AppliedPrice, IndicatorLine.MODE_UPPER, 1);
            LastBottom = indicatorService.BollingerBands(Pipster.Symbol, TimeFrame, Period, 1, 0, AppliedPrice, IndicatorLine.MODE_LOWER, 1);
        }

        public Boolean IsAbove()
        {
            if (Signal > Top)
            {
                return true;
            }
            return false;
        }

        public Boolean isBelow()
        {
            if (Signal < Bottom)
            {
                return true;
            }
            return false;
        }

        public Boolean IsInside()
        {
            if (Signal < Top && Signal > Bottom)
            {
                return true;
            }
            return false;
        }

        public Boolean IsInsideDown()
        {
            if (Signal < Ma && Signal > Bottom)
            {
                return true;
            }
            return false;
        }

        public Boolean IsInsideUp()
        {
            if (Signal < Top && Signal > Ma)
            {
                return true;
            }
            return false;
        }

        public override Boolean CanBuy()
        {
            if (LastSignal < LastTop && Signal > Top)
            {
                return true;
            }
            return false;
        }


        public override Boolean CanSell()
        {
            if (LastSignal > LastBottom && Signal < Bottom)
            {
                return true;
            }
            return false;
        }
    }
}
