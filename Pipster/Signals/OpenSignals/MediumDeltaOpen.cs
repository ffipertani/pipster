﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace Signals.OpenSignals
{
    public class MediumDeltaOpen:OpenSignal
    {
        int openAfterPips = 40;

        Boolean isNewHour = false;
        DateTime prevDate;
        DateTime currentDate;
        Boolean canBuy = false;
        Boolean canSell = false;
        Boolean buyed = false;
        Boolean selled = false;
        Double openingAsk = 0;
        Double openingBid = 0;
        int atrPips = 0;

        public override void RefreshData()
        {
            try
            {
                DateTime now = Pipster.Now;

                if (currentDate == null)
                {
                    currentDate = now;
                    currentDate.AddDays(-1);
                }


                if (now.Hour == 0 && now.Day != currentDate.Day)
                {
                    //Double atr = indicatorService.Atr(Pipster.Symbol, TimeFrame.PERIOD_D1, 34, 0);
                    Double atr = indicatorService.MediumMinMovement(Pipster.Symbol, TimeFrame.PERIOD_D1, 34);                    
                    String points = (atr / Pipster.Point).ToString();
                    if (points.IndexOf(",") > 0)
                    {
                        points = points.Substring(0, points.IndexOf(","));
                    }
                    atrPips =  int.Parse(points);
                    openAfterPips = atrPips + 10;
                   // openAfterPips = openAfterPips / 2;
                  

                    currentDate =  now;
                    canBuy = true;
                    canSell = true;
                  
                    openingAsk = Pipster.Ask;
                    openingBid = Pipster.Bid;
                }


                checkStartHour();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }


        /*     */

        private void checkStartHour()
        {
            DateTime now = Pipster.Now;
            if (now.Hour != prevDate.Hour || now.Day != prevDate.Day)
            {                
                isNewHour = true;                
            }
            else
            {
                isNewHour = false;
            }
            prevDate = now;
        }

        public override Boolean CanBuy()
        {
            if (canBuy && isNewHour)
            {
                if (Pipster.Ask > openingAsk + (openAfterPips * Pipster.Point))
                {
                    canBuy = false;
                    canSell = false;
                    openingAsk = Pipster.Ask + (atrPips * Pipster.Point);
                    return true;

                }
            }
            return false;
        }
       

        public override Boolean CanSell()
        {
            if (canSell && isNewHour)
            {
                if (Pipster.Bid < openingBid - (openAfterPips * Pipster.Point))
                {
                    canSell = false;
                    canBuy = false;
                    openingBid = Pipster.Bid - (atrPips * Pipster.Point);
                    return true;
                }                               
            }
            return false;
        }
     

        /*
        public override Boolean CanSell()
        {
            foreach (Order order in Pipster.ManagedOrders)
            {
                if (order.Type == TradeOperation.SELL)
                {
                    return false;
                }
            }
            
            if (Pipster.Ask > openingAsk + (openAfterPips * Pipster.Point))
            {
                if (canBuy)
                {
                    // canBuy = false;
                    openingAsk = Pipster.Ask + (160 * Pipster.Point);
                    return true;
                }
            }
            return false;
        }


        public override Boolean CanBuy()
        {
            foreach (Order order in Pipster.ManagedOrders)
            {
                if (order.Type == TradeOperation.BUY)
                {
                    return false;
                }
            }
            if (Pipster.Bid < openingBid - (openAfterPips * Pipster.Point))
            {
                if (canSell)
                {
                    //canSell = false;
                    openingBid = Pipster.Bid - (160 * Pipster.Point);
                    return true;
                }
            }
            return false;
        }
         */
    }
}
