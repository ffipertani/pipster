﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace Signals.OpenSignals
{
    public class GatorOpen:OpenSignal
    {
        public int Margin { get; set; }
        public TimeFrame TimeFrame{get;set;}

        private Double Up { get; set; }
        private Double Down { get; set; }
        
        public GatorOpen(TimeFrame timeFrame)
        {            
            TimeFrame = timeFrame;
            Margin = 1;
        }

        public override void RefreshData()
        {
            Up = indicatorService.Gator(Pipster.Symbol, TimeFrame, 13, 8, 8, 5, 5, 3, MovingAverageType.SMOOTHED, AppliedPrice.MEDIAN, IndicatorLine.MODE_UPPER,0);
            Down = indicatorService.Gator(Pipster.Symbol, TimeFrame, 13, 8, 8, 5, 5, 3, MovingAverageType.SMOOTHED, AppliedPrice.MEDIAN, IndicatorLine.MODE_LOWER, 0);
        }

        public override Boolean CanBuy()
        {
            if ((Up + Down > 0) && Up  > (Margin * Pipster.Point))
            {                                  
                return true;                 
            }
            return false;
        }


        public override Boolean CanSell()
        {
            if ((Up + Down < 0) && Down < -(50 *Pipster.Point))
            {
                return true;
            }
            return false;
        }
    }
}
