﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace Signals.OpenSignals
{
    public class RsiOpen:OpenSignal
    {
        public TimeFrame TimeFrame { get; set; }

        Double rsi;

        public override void RefreshData()
        {
            try
            {
                rsi = indicatorService.Rsi(Pipster.Symbol, TimeFrame, 14, PipsterEA.Model.AppliedPrice.CLOSE, 0);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }


        
        

        public override Boolean CanBuy()
        {
            if (rsi <= 80)
            {
                return true;
            }
            
            return false;
        }


        public override Boolean CanSell()
        {
            if (rsi >= 20)
            {
                return true;
            }
            return false;
        }

     

    }
}
