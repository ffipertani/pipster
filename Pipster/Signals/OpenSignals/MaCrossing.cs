﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Service;
using PipsterEA.Model;
using PipsterEA.Core;

namespace Signals.OpenSignals
{
    public class MaCrossing:OpenSignal
    {              
        public TimeFrame TimeFrame{get;set;}
        public int FastPeriod{get;set;}
        public int SlowPeriod{get;set;}
        

        private Double Slow { get; set; }
        private Double Fast { get; set; }
        private Double LastSlow { get; set; }
        private Double LastFast { get; set; }

        public MaCrossing() : this(TimeFrame.PERIOD_D1, 5, 3) { }

        public MaCrossing(TimeFrame timeFrame, int slowPeriod, int fastPeriod)
        {            
            TimeFrame = timeFrame;
            FastPeriod = fastPeriod;
            SlowPeriod = slowPeriod;
        }

        public override void RefreshData()
        {
            Slow = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, SlowPeriod, 0, MovingAverageType.SIMPLE, AppliedPrice.TYPICAL, 0);
            Fast = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, FastPeriod, 0, MovingAverageType.SIMPLE, AppliedPrice.TYPICAL, 0);

            LastSlow = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, SlowPeriod, 0, MovingAverageType.SIMPLE, AppliedPrice.TYPICAL, 1);
            LastFast = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, FastPeriod, 0, MovingAverageType.SIMPLE, AppliedPrice.TYPICAL, 1);
        }

        public override Boolean CanBuy()
        {            
            if (LastSlow>LastFast && Slow < Fast)
            {
                return true;
            }
            return false;
        }


        public override Boolean CanSell()
        {
            if (LastSlow < LastFast && Slow > Fast)
            {
                return true;
            }
            return false;
        }
    }
}
