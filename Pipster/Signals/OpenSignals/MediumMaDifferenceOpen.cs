﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;
using System.Xml.Serialization;

namespace Signals.OpenSignals
{
    [XmlInclude(typeof(MediumMaDifferenceOpen))]
    public class MediumMaDifferenceOpen : OpenSignal
    {
        private Double difference;
        private DateTime lastCheck;

        public AppliedPrice SlowAppliedPrice { get; set; }
        public MovingAverageType SlowType { get; set; }
        public int SlowPeriod { get; set; }

        public AppliedPrice FastAppliedPrice { get; set; }
        public MovingAverageType FastType { get; set; }
        public int FastPeriod { get; set; }
        public int BackPeriods { get; set; }
        public TimeFrame TimeFrame { get; set; }

        Double slowValue { get; set; }

         public MediumMaDifferenceOpen() : this(TimeFrame.PERIOD_D1, AppliedPrice.CLOSE, MovingAverageType.SIMPLE, 10, AppliedPrice.CLOSE, MovingAverageType.SIMPLE, 20, 20) { }

         public MediumMaDifferenceOpen(TimeFrame timeFrame, AppliedPrice slowAppliedPrice, MovingAverageType slowType, int slowPeriod,
            AppliedPrice fastAppliedPrice, MovingAverageType fastType, int fastPeriod, int count)
        {
            BackPeriods = count;
            TimeFrame = timeFrame;
            SlowAppliedPrice = slowAppliedPrice;
            SlowType = slowType;
            SlowPeriod = slowPeriod;
            FastAppliedPrice = fastAppliedPrice;
            FastType = fastType;
            FastPeriod = fastPeriod;
        }

        private void calcDifference()
        {
            difference = 0;
            Double sum = 0;
            for (int i = 0; i < BackPeriods; i++)
            {
                Double fast = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, FastPeriod, 0, FastType, FastAppliedPrice, i);
                Double slow = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, SlowPeriod, 0, SlowType, SlowAppliedPrice, i);
                difference = Math.Abs(fast - slow);

                sum += difference;
            }

            difference = sum / BackPeriods;
            lastCheck = Pipster.Now;
        }

        public override void RefreshData()
        {
            DateTime initBar = tradingService.Time(Pipster.Symbol, TimeFrame, 0);

            if (lastCheck == null)
            {
                lastCheck = Pipster.Now;
            }

            if (lastCheck < initBar)
            {
                calcDifference();
            }

            slowValue = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, SlowPeriod, 0, SlowType, SlowAppliedPrice, 0);
        }

        public override Boolean CanBuy()
        {
            if (Pipster.Ask > slowValue + difference)
            {
                return true;
            }
            return false;
        }


        public override Boolean CanSell()
        {
            if (Pipster.Bid < slowValue - difference)
            {
                return true;
            }
            return false;
        }
    }
}
