﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Service;
using PipsterEA.Model;
using PipsterEA.Core;

namespace Signals.OpenSignals
{
    public class TunnelOpen:OpenSignal
    {              
        public TimeFrame TimeFrame{get;set;}
        public int Period{get;set;}
        public MovingAverageType Type{get;set;}
        public AppliedPrice AppliedPrice{get;set;}
        public int Level{get;set;}

        Double Ma, Top, Bottom;
        Double LastMa, LastTop, LastBottom;
        Double Signal,LastSignal;

        public TunnelOpen() : this(TimeFrame.PERIOD_D1, 150, 100, MovingAverageType.LINEAR_WEIGHTED, AppliedPrice.WEIGHTED) { }

        public TunnelOpen(TimeFrame timeFrame, int period, int level, MovingAverageType type,AppliedPrice appliedPrice)
        {            
            TimeFrame = timeFrame;
            Period = period;
            Type = type;
            AppliedPrice = appliedPrice;
            Level = level;
        }

        public override void RefreshData()
        {
            Ma = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, Period, 0, Type, AppliedPrice, 0);
            LastMa = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, Period, 0, Type, AppliedPrice, 1);
            Signal = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, 5, 0, Type, AppliedPrice, 0);
            LastSignal = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, 5, 0, Type, AppliedPrice, 1);

            Top = Ma + (Level * Pipster.Point);
            Bottom = Ma - (Level * Pipster.Point);
            LastTop = LastMa + (Level * Pipster.Point);
            LastBottom = LastMa - (Level * Pipster.Point);
        }

        public Boolean IsInside()
        {
            if (Signal < Top && Signal > Bottom)
            {
                return true;
            }
            return false;
        }

        public Boolean IsInsideDown()
        {
            if (Signal < Ma && Signal > Bottom)
            {
                return true;
            }
            return false;
        }

        public Boolean IsInsideUp()
        {
            if (Signal < Top && Signal > Ma)
            {
                return true;
            }
            return false;
        }

        public override Boolean CanBuy()
        {
            if (LastSignal < LastTop && Signal > Top)
            {
                return true;
            }
            return false;
        }


        public override Boolean CanSell()
        {
            if (LastSignal > LastBottom && Signal < Bottom)
            {
                return true;
            }
            return false;
        }
    }
}
