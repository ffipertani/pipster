﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace Signals.OpenSignals
{
    public class MacdOpen:OpenSignal
    {
        Double Signal { get; set; }
        Double Value { get; set; }
        public TimeFrame TimeFrame { get; set; }
        public Boolean StrongOpen { get; set; }

        public int FastPeriod { get; set; }
        public int SlowPeriod { get; set; }
        public int SignalPeriod { get; set; }
        
        public MacdOpen() : this(false) { }

        public MacdOpen(Boolean strongOpen)
        {
            StrongOpen = strongOpen;
            FastPeriod = 8;
            SlowPeriod = 50;
            SignalPeriod = 9;
        }
        public override void RefreshData()
        {
            Signal = indicatorService.Macd(Pipster.Symbol, TimeFrame, FastPeriod, SlowPeriod, SignalPeriod, AppliedPrice.CLOSE, IndicatorLine.SIGNAL, 0);
            Value = indicatorService.Macd(Pipster.Symbol, TimeFrame, FastPeriod, SlowPeriod, SignalPeriod, AppliedPrice.CLOSE, IndicatorLine.MAIN, 0);
        }

        public override Boolean CanBuy()
        {
            if (Signal<Value)
            {
                if (StrongOpen)
                {
                    if (Signal > 0)
                    {
                        return true;
                    }
                }
                else
                {
                    return true;
                }
                
                
            }
            return false;
        }


        public override Boolean CanSell()
        {
            if (Signal > Value )
            {
                if (StrongOpen)
                {
                    if (Signal < 0)
                    {
                        return true;
                    }
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
    }
}
