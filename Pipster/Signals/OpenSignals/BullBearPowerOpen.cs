﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace Signals.OpenSignals
{
    public class BullsBearsPowerOpen:OpenSignal
    {
      
        public TimeFrame TimeFrame{get;set;}
        public int Period { get; set; }

        private Double Bulls { get; set; }
        private Double Bears { get; set; }

        public BullsBearsPowerOpen() : this(TimeFrame.PERIOD_D1, 13) { }

        public BullsBearsPowerOpen(TimeFrame timeFrame, int period)
        {            
            TimeFrame = timeFrame;
            Period = period;
        }

        public override void RefreshData()
        {
            Bulls = indicatorService.BullsPower(Pipster.Symbol, TimeFrame, Period, AppliedPrice.TYPICAL, 0);
            Bears = indicatorService.BearsPower(Pipster.Symbol, TimeFrame, Period, AppliedPrice.TYPICAL, 0);
        }

        public override Boolean CanBuy()
        {
            if (Bulls>0)
            {                                  
                return true;                 
            }
            return false;
        }


        public override Boolean CanSell()
        {
            if (Bears<0)
            {
                return true;
            }
            return false;
        }
    }
}
