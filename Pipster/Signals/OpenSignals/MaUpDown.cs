﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace Signals.OpenSignals
{
    public class MaUpDown:OpenSignal
    {
        private static int DELTA = 0;

        public TimeFrame TimeFrame{get;set;}
        public int SlowPeriod { get; set; }
        public int FastPeriod { get; set; }
        public MovingAverageType SlowType { get; set; }
        public MovingAverageType FastType { get; set; }

        private Double Slow { get; set; }
        private Double Fast { get; set; }
         

        public MaUpDown():this(TimeFrame.PERIOD_D1,20,MovingAverageType.SIMPLE,5,MovingAverageType.EXPONENTIAL){ }

        public MaUpDown(TimeFrame timeFrame, int slow_period,MovingAverageType slowType, int fast_period, MovingAverageType fastType)
        {
            TimeFrame = timeFrame;
            SlowPeriod = slow_period;
            FastPeriod = fast_period;
            SlowType = slowType;
            FastType = fastType;
        }

        public override void RefreshData()
        {
            Slow = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, SlowPeriod, 0, SlowType, AppliedPrice.TYPICAL, 0);
            Fast = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, FastPeriod, 0, FastType, AppliedPrice.TYPICAL, 0);           
        }

        public override Boolean CanBuy()
        {            
            if (Slow < Fast - DELTA * Pipster.Point)
            {
                return true;
            }
            return false;
        }


        public override Boolean CanSell()
        {
            if (Slow > Fast + DELTA * Pipster.Point)
            {
                return true;
            }
            return false;
        }
    }
}
