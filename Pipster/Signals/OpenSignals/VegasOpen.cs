﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;
using Signals.Common;

namespace Signals.OpenSignals
{
    public class VegasOpen : OpenSignal
    {        
        private Vegas Vegas { get; set; }
         
        public override void Init()
        {
            Vegas = new Vegas(Pipster);
        }

        public override void RefreshData()
        {
            Vegas.Refresh();
        }

        public override Boolean CanBuy()
        {
            if (Vegas.IsUpTrend())
            {
                if (Vegas.IsStrongBuySignal())
                {
                    return true;
                }
            }
            return false;
        }


        public override Boolean CanSell()
        {
            if (Vegas.IsDownTrend())
            {
                if (Vegas.IsStrongSellSignal())
                {
                    return true;
                }
            }
            return false;
        }
    }
}
