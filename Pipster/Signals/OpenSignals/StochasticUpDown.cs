﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace Signals.OpenSignals
{
    public class StochasticUpDown : OpenSignal
    {
        public TimeFrame TimeFrame{get;set;}

        private Double Value { get; set; }
        private Double Signal { get; set; }
        public int KPeriod { get; set; }
        public int DPeriod { get; set; }
        public int Slowing { get; set; }
       
        public StochasticUpDown():this(TimeFrame.PERIOD_D1,7,14,5){}

        public StochasticUpDown(TimeFrame timeFrame, int kperiod, int dperiod, int slowing)
        {
            TimeFrame = timeFrame;
            KPeriod = kperiod;
            DPeriod = dperiod;
            Slowing = slowing;
        }

        public override void RefreshData()
        {
            Value = indicatorService.Stochastic(Pipster.Symbol, TimeFrame, KPeriod, DPeriod, Slowing, MovingAverageType.SIMPLE, PriceField.LOW_HIGH, IndicatorLine.MAIN, 0);
            Signal = indicatorService.Stochastic(Pipster.Symbol, TimeFrame, KPeriod, DPeriod, Slowing, MovingAverageType.SIMPLE, PriceField.LOW_HIGH, IndicatorLine.SIGNAL, 0);
        }

        public override Boolean CanBuy()
        {
            if (Value > 20 && Value < 80 && Value > Signal)
            {
                return true;
            }
            return false;
        }


        public override Boolean CanSell()
        {
            if (Value < 80 && Value > 20 && Value < Signal)
            {
                return true;
            }
            return false;
        }
    }
}
