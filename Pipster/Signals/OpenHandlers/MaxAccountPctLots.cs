﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace Signals.OpenHandlers
{
    public class MaxAccountPctLots : OpenHandler
    {
        public int Pct { get; set; }

        public MaxAccountPctLots() : this(3) { }

        public MaxAccountPctLots(int pct)
        {
            Pct = pct;
        }


        public override Order CreateOrder(Order order)
        {
            Double size = tradingService.AccountSize();
            Double pctAccount = size * Pct;
            Double pips = 0;

            if (order.Type == TradeOperation.BUY)
            {
                pips = (Pipster.Ask - order.StopLoss) / Pipster.Point;
            }
            else if (order.Type == TradeOperation.SELL)
            {
                pips = (order.StopLoss - Pipster.Bid) / Pipster.Point;
            }
            Double lots = Pct * size / pips;//Pct:pips =x:Account ;// pctAccount = pctAccount 
            order.Lots = lots / 1000;
            return order;
        }

    }
}
