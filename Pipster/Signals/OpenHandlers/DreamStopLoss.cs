﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace Signals.OpenHandlers
{
    public class DreamStopLoss:OpenHandler
    {
        public int TakeProfit { get; set; }
        public int StopLoss { get; set; }

        public override Order CreateOrder(Order order)
        {
            Double atr = indicatorService.Atr(Pipster.Symbol, TimeFrame.PERIOD_D1, 34, 0);
         
            String points = (atr / Pipster.Point).ToString();
            if (points.IndexOf(",") > 0)
            {
                points = points.Substring(0, points.IndexOf(","));
            }
            int atrPips = int.Parse(points);
            //openAfterPips = openAfterPips / 2;
           // order.TakeProfit = atrPips;
          
            int Pips = (atrPips / 2);
           // int Pips = atrPips / 3 ;
            if (Pips == 0)
            {
                order.StopLoss = 0;
                return order;
            }
            if (order.Type == TradeOperation.BUY)
            {
                order.StopLoss = Math.Round(order.OpenPrice - Pips * Pipster.Point, (int)Pipster.Digits);
            }
            else if (order.Type == TradeOperation.SELL)
            {
                order.StopLoss = Math.Round(order.OpenPrice + Pips * Pipster.Point, (int)Pipster.Digits);
            }
                     
            return order;
        }
    }
}

