﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace Signals.OpenHandlers
{
    public class TightStopLoss:OpenHandler
    {        
        public TimeFrame TimeFrame { get; set; }
        public int BackPeriods { get; set; }
        public int ExtraMargin { get; set; }

        public TightStopLoss() : this(TimeFrame.PERIOD_D1, 0, 0) { }

        public TightStopLoss(TimeFrame timeFrame, int count, int extraMargin)
        {
            TimeFrame = timeFrame;
            BackPeriods = count;
            ExtraMargin = extraMargin;            
        }

        public override Order CreateOrder(Order order)
        {            
            if (order.Type == TradeOperation.BUY)
            {
                Double lowest = indicatorService.Lowest(Pipster.Symbol, TimeFrame, SeriesType.LOW, BackPeriods, 0);
                if (lowest < 0)
                {
                    lowest = 0;
                }
                order.StopLoss = lowest - ExtraMargin * Pipster.Point;
            }
            else if (order.Type == TradeOperation.SELL)
            {
                Double highest = indicatorService.Highest(Pipster.Symbol, TimeFrame, SeriesType.HIGH, BackPeriods, 0);
                order.StopLoss = highest + ExtraMargin * Pipster.Point;
            }
            return order;
        }
    }
}
