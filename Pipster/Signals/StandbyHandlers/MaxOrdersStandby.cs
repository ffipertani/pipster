﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;

namespace Signals.StandbyHandlers
{
    public class MaxOrdersStandby:StandbyHandler
    {
        public int Max { get; set; }

        public MaxOrdersStandby():this(1)
        {
        }

        public MaxOrdersStandby(int max)
        {
            Max = max;
        }

        public override Boolean IsStandby()
        {
            if (Pipster.ManagedTickets.Count >= Max)
            {
                return true;
            }
            return false;
        }
    }
}
