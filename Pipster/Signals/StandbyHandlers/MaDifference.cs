﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;
using Signals.Common;

namespace Signals.StandbyHandlers
{
    public class MaDifference :StandbyHandler
    {
        private static int START_TRADE_AT = 150;
        private Vegas Vegas{get;set;}

      

        public override void Init()
        {
            Vegas = new Vegas(Pipster);       
        }

        public override Boolean IsStandby()
        {           
            double difference = Vegas.Ma[0] - Vegas.Signals[0];
            if(difference>0){
                if (difference < START_TRADE_AT)
                {
                    return true;
                }
            }

            if (difference < 0)
            {
                if (difference > -START_TRADE_AT)
                {
                    return true;
                }
            }

            return false;
        }          
    

    }
}
