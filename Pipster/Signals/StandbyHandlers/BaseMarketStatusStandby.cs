﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace Signals.StandbyHandlers
{
    public abstract class BaseMarketStatusStandby:StandbyHandler
    {
        public MarketStatus MarketStatus { get; set; }
        public BaseMarketStatusStandby(MarketStatus marketStatus)
        {
            MarketStatus = marketStatus;
        }
        protected abstract MarketStatus getMarketStatus();

        public override Boolean IsStandby()
        {
            MarketStatus currentStatus = getMarketStatus();
            if (currentStatus == MarketStatus)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
