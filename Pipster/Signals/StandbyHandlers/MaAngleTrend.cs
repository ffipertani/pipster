﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace Signals.StandbyHandlers
{
    public class MaAngleTrend : BaseMarketStatusStandby
    {
        public TimeFrame TimeFrame { get; set; }
        public int Period { get; set; }
        public Double Angle { get; set; }

        public MaAngleTrend() : this(MarketStatus.Trending, TimeFrame.PERIOD_D1, 40, 30) { }

        public MaAngleTrend(MarketStatus marketStatus,TimeFrame timeFrame, int period, Double angle):base(marketStatus)
        {           
            TimeFrame = timeFrame;
            Period = period;
            Angle = angle;
        }

       
        protected override MarketStatus getMarketStatus()
        {            
            Double max = 0;
            Double min = 10000000;
            List<Double> vals = new List<Double>();
            /*
            for (int i = 0; i < Period; i++)
            {
                Double ma = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, Period, 0, MovingAverageType.SIMPLE, AppliedPrice.TYPICAL, i);
                max = Math.Max(max, ma);
                min = Math.Min(min, ma);
                vals.Add(ma);
            }
             */
            Double val0 = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, Period, 0, MovingAverageType.SIMPLE, AppliedPrice.TYPICAL, 0);
            Double val1 = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, Period, 0, MovingAverageType.SIMPLE, AppliedPrice.TYPICAL, Period);

            Double angle = Math.Atan2(val0 - val1, 1) - Math.Atan2(0, 0);
            //Double angle = CalcAngle(1, vals[0], 2, vals[1]);
            angle = angle * 180 / System.Math.PI;
            angle = angle *100;
            angle = Math.Abs(angle);
            if (angle > Angle)
            {
                return MarketStatus.Trending;
            }
            else
            {
                return MarketStatus.Sideway;
            }

        }

        public double CalcAngle(double px1, double py1, double px2, double py2)
        {
            // Negate X and Y values
            double pxRes = px2 - px1;
            double pyRes = py2 - py1;
            double angle = 0.0;
            // Calculate the angle
            if (pxRes == 0.0)
            {
                if (pxRes == 0.0)
                    angle = 0.0;
                else if (pyRes > 0.0) angle = System.Math.PI / 2.0;
                else
                    angle = System.Math.PI * 3.0 / 2.0;
            }
            else if (pyRes == 0.0)
            {
                if (pxRes > 0.0)
                    angle = 0.0;
                else
                    angle = System.Math.PI;
            }
            else
            {
                if (pxRes < 0.0)
                    angle = System.Math.Atan(pyRes / pxRes) + System.Math.PI;
                else if (pyRes < 0.0) angle = System.Math.Atan(pyRes / pxRes) + (2 * System.Math.PI);
                else
                    angle = System.Math.Atan(pyRes / pxRes);
            }
            // Convert to degrees
            angle = angle * 180 / System.Math.PI; return angle;

        }

    }
}
