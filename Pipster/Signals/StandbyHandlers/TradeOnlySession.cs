﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace Signals.StandbyHandlers
{
    public class TradeOnlySession : StandbyHandler
    {
        public TradingSession TradingSession { get; set; }
        Dictionary<TradingSession, TradingHours> tradingSessions = new Dictionary<TradingSession, TradingHours>();

        public TradeOnlySession()
        {
            tradingSessions.Add(PipsterEA.Model.TradingSession.American, new TradingHours(12, 20));
            tradingSessions.Add(PipsterEA.Model.TradingSession.European, new TradingHours(6, 14));
            tradingSessions.Add(PipsterEA.Model.TradingSession.Asian, new TradingHours(20, 4));
        }

        public override Boolean IsStandby()
        {
            TradingHours hours = null;
            tradingSessions.TryGetValue(TradingSession, out hours);
            if (hours.From < hours.To)
            {
                if (Pipster.Now.Hour < hours.From || Pipster.Now.Hour > hours.To)
                {
                    return true;
                }
            }
            else
            {
                if (Pipster.Now.Hour < hours.From && Pipster.Now.Hour > hours.To)
                {
                    return true;
                }
            }
            return false;
         }
    }
}
