﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace Signals.StandbyHandlers
{
    public class WaitAfterALoss:StandbyHandler
    {
        public int Minutes { get; set; }        
        private DateTime lastSl;
        private Boolean waiting = false;

        public WaitAfterALoss() : this(10) { }

        public WaitAfterALoss(int minutes)
        {             
            this.Minutes = minutes;           
        }

        public override void Init()
        {
            Pipster.OrderClosed += new Pipster.OrderClosedEvent(Pipster_OrderClosed);
        }

        void Pipster_OrderClosed(Order order)
        {
            String comment = order.Comment;
            if (order.Profit < 0)
            {               
                lastSl = Pipster.Now;
                waiting = true;
            }
        }

        public override Boolean IsStandby()
        {
            DateTime now = Pipster.Now;
            if (waiting)
            {
                if (now.Subtract(lastSl).TotalMinutes >= Minutes)
                {                   
                    waiting = false;                    
                }
            }
            return waiting;
        }          
    }
}
