﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace Signals.StandbyHandlers
{
    public class WaitNextPeriodAfterAClose:StandbyHandler
    {
        public TimeFrame TimeFrame { get; set; }        
        private DateTime lastClose;
        private Boolean waiting = false;

        public WaitNextPeriodAfterAClose() : this(TimeFrame.PERIOD_D1) { }

        public WaitNextPeriodAfterAClose(TimeFrame timeFrame)
        {
            this.TimeFrame = timeFrame;           
        }

        public override void Init()
        {
            Pipster.OrderClosed += new Pipster.OrderClosedEvent(Pipster_OrderClosed);
        }

        void Pipster_OrderClosed(Order order)
        {
            lastClose = Pipster.Now;
            waiting = true;
        }

        public override Boolean IsStandby()
        {
            DateTime initBar = tradingService.Time(Pipster.Symbol, TimeFrame, 0);
            
            if (waiting)
            {
                if (lastClose < initBar)
                {                   
                    waiting = false;                    
                }
            }
            return waiting;
        }          
    }
}
