﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Service;
using System.Reflection;

namespace PipsterEA.Core
{
    public abstract class BaseStrategy
    {
        public String Type
        {
            get
            {
                return GetType().ToString();
            }
        }

        protected PipsterEA.Core.Pipster Pipster { get; set; }
        protected IndicatorService indicatorService = new IndicatorService();
        protected TradingService tradingService = new TradingService();
      
        public virtual void Init()
        {

        }

        public override String ToString()
        {
            String str = "";
            foreach (PropertyInfo pid in GetType().GetProperties())
            {
                if (pid.Name.Equals("Type"))
                {
                    continue;
                }
                if (pid.CanRead)
                {
                    Object val = pid.GetValue(this, null);
                    if (val != null)
                    {
                        str = str + " " + pid.Name + ":" + val;
                    }
                }
            }
            return str;
        }

        public void SetPipster(Pipster pipster)
        {
            Pipster = pipster;
        }
    }
}
