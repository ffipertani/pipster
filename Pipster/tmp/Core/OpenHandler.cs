﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Service;

namespace PipsterEA.Core
{
    public abstract class OpenHandler:BaseStrategy
    {        
        public abstract Order CreateOrder(Order order);        
    }
}
