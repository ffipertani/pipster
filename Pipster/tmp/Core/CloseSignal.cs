﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Service;
using PipsterEA.Model;

namespace PipsterEA.Core
{
    public abstract class CloseSignal:BaseStrategy
    {      
        public abstract double CloseLots(Order order);        
    }
}
