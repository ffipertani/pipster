﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;

namespace PipsterEA.Core
{
    public abstract class StandbyHandler:BaseStrategy
    {
        public abstract Boolean IsStandby();
    }
}
