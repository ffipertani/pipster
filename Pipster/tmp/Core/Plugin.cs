﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace PipsterEA.Core
{
     
    public class Plugin
    {
        [XmlAttribute("name")]
        public String Name { get; set; }
    
        public String Description { get; set; }
     
        public String ClassName { get; set; }
    }
}
