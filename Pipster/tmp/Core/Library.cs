﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace PipsterEA.Core
{
     
    public class Library
    {
        [XmlAttribute("name")]
        public String Name { get; set; }

       
        public List<Plugin> Plugins = new List<Plugin>();
    }
}
