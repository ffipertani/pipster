﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace PipsterEA.Strategy.StandbyHandlers
{
    public class MaAngleTrend : BaseMarketStatusStandby
    {
        public TimeFrame TimeFrame { get; set; }
        public int Period { get; set; }
        public Double Angle { get; set; }

        public MaAngleTrend() : this(MarketStatus.Trending, TimeFrame.PERIOD_D1, 40, 30) { }

        public MaAngleTrend(MarketStatus marketStatus,TimeFrame timeFrame, int period, Double angle):base(marketStatus)
        {           
            TimeFrame = timeFrame;
            Period = period;
            Angle = angle;
        }

       
        protected override MarketStatus getMarketStatus()
        {            
            Double max = 0;
            Double min = 10000000;
            for (int i = 0; i < 5; i++)
            {
                Double ma = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, Period, 0, MovingAverageType.SMOOTHED, AppliedPrice.TYPICAL, i);
                max = Math.Max(max, ma);
                min = Math.Min(min, ma);
            }

            Double angle = Math.Atan(max - min) / Pipster.Point;

            if (angle > Angle)
            {
                return MarketStatus.Trending;
            }
            else
            {
                return MarketStatus.Sideway;
            }

        }

    }
}
