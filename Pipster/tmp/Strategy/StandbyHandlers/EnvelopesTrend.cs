﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace PipsterEA.Strategy.StandbyHandlers
{
    public class EnvelopesTrend : BaseMarketStatusStandby
    {
        public TimeFrame TimeFrame { get; set; }
        public int EnvelopesPeriod { get; set; }
        public int MaPeriod{ get; set; }
        public int Deviation { get; set; }
        public EnvelopesTrend() : this(MarketStatus.Trending, TimeFrame.PERIOD_D1, 20,1, 10) { }

        public EnvelopesTrend(MarketStatus marketStatus,TimeFrame timeFrame, int envPeriod, int deviation, int maPeriod):base(marketStatus)
        {
            TimeFrame = timeFrame;
            EnvelopesPeriod = envPeriod;
            MaPeriod = maPeriod;
            Deviation = deviation;
        }
        
        protected override MarketStatus getMarketStatus()
        {
            Double atr = indicatorService.Atr(Pipster.Symbol, TimeFrame, MaPeriod, 0);

        //    Double deviation = atr * 100;
            Double deviation = Deviation;
          //  deviation = deviation + 14;
            Double upEnv = indicatorService.Envelopes(Pipster.Symbol, TimeFrame, EnvelopesPeriod, MovingAverageType.SMOOTHED, 0, AppliedPrice.CLOSE, deviation, IndicatorLine.MODE_UPPER, 0);
            Double downEnv = indicatorService.Envelopes(Pipster.Symbol, TimeFrame, EnvelopesPeriod, MovingAverageType.SMOOTHED, 0, AppliedPrice.CLOSE, deviation, IndicatorLine.MODE_LOWER, 0);

            Double maEnv = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, MaPeriod, 0, MovingAverageType.EXPONENTIAL, AppliedPrice.CLOSE, 0);

            

            if (maEnv > downEnv && maEnv < upEnv)
            {
                return MarketStatus.Sideway;
            }
            else
            {
                return MarketStatus.Trending;
            }
        }

    }
}
