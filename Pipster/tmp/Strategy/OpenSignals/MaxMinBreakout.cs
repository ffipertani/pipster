﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;
using System.Xml.Serialization;

namespace PipsterEA.Strategy.OpenSignals
{
    [XmlInclude(typeof(MaxMinBreakout))]
    public class MaxMinBreakout : OpenSignal
    {        
        public TimeFrame TimeFrame { get; set; }
        public int BackPeriods { get; set; }
        
        private Double Max { get; set; }
        private Double Min { get; set; }

        public MaxMinBreakout():this(TimeFrame.PERIOD_D1,3)
        {
        }

        public MaxMinBreakout(TimeFrame timeFrame, int count)
        {
            TimeFrame = timeFrame;
            BackPeriods = count;
        }

        public override void RefreshData()
        {
            Max = indicatorService.Highest(Pipster.Symbol, TimeFrame, SeriesType.HIGH, BackPeriods, 1);
            Min = indicatorService.Lowest(Pipster.Symbol, TimeFrame, SeriesType.LOW, BackPeriods, 1);
        }

        public override Boolean CanBuy()
        {
            if (Pipster.Ask > Max)
            {
                return true;
            }
            return false;
        }


        public override Boolean CanSell()
        {
            if (Pipster.Bid < Min)
            {
                return true;
            }
            return false;
        }
    }
}
