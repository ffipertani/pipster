﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace PipsterEA.Strategy.OpenSignals
{
    public class GatorOpenFast:OpenSignal
    {
        public int Margin { get; set; }
        public TimeFrame TimeFrame{get;set;}

        private Double Up { get; set; }
        private Double Down { get; set; }

        public GatorOpenFast(TimeFrame timeFrame)
        {            
            TimeFrame = timeFrame;
            Margin = 1;
        }

        public override void RefreshData()
        {
            Up = indicatorService.Gator(Pipster.Symbol, TimeFrame, 5, 1, 3, 1, 3, 1, MovingAverageType.SMOOTHED, AppliedPrice.MEDIAN, IndicatorLine.MODE_UPPER,0);
            Down = indicatorService.Gator(Pipster.Symbol, TimeFrame, 5, 1, 3, 1, 3, 1, MovingAverageType.SMOOTHED, AppliedPrice.MEDIAN, IndicatorLine.MODE_LOWER, 0);
        }

        public override Boolean CanBuy()
        {
            if ((Up + Down > 0) && Up  > (Margin * Pipster.Point))
            {                                  
                return true;                 
            }
            return false;
        }


        public override Boolean CanSell()
        {
            if ((Up + Down < 0) && Down < -(Margin *Pipster.Point))
            {
                return true;
            }
            return false;
        }
    }
}
