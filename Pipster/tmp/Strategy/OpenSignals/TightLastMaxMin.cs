﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace PipsterEA.Strategy.OpenSignals
{
    public class TightLastMaxMin:OpenSignal
    {        
        public TimeFrame TimeFrame { get; set; }
        public int BackPeriods { get; set; }
        public int Max { get; set; }

        private double points;

        public TightLastMaxMin() : this(TimeFrame.PERIOD_D1, 0, 0) { }

        public TightLastMaxMin(TimeFrame timeFrame, int count, int max)
        {
            TimeFrame = timeFrame;
            BackPeriods = count;
            Max = max;           
        }

        public override void RefreshData()
        {
            points = Pipster.Point;
        }

        public override Boolean CanBuy()
        {
            Double price = Pipster.Ask;
            Double lowest = indicatorService.Lowest(Pipster.Symbol, TimeFrame, SeriesType.LOW, BackPeriods, 0);

            if ( (price>lowest) && (price - lowest > Max * points) )
            {
                return false;
            }
            return true;
        }

        public override Boolean CanSell()
        {
            Double price = Pipster.Bid;
            Double highest = indicatorService.Highest(Pipster.Symbol, TimeFrame, SeriesType.HIGH, BackPeriods, 0);

            if ( (price<highest) &&  (highest - price > Max * points) )
            {
                return false;
            }
            return true;
        }
    }
}
