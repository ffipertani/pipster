﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace PipsterEA.Strategy.OpenSignals
{
    public class MaUpDown:OpenSignal
    {
        public TimeFrame TimeFrame{get;set;}
        public int SlowPeriod { get; set; }
        public int FastPeriod { get; set; }

        private Double Slow { get; set; }
        private Double Fast { get; set; }
         

        public MaUpDown():this(TimeFrame.PERIOD_D1,20,5){ }

        public MaUpDown(TimeFrame timeFrame, int slow_period, int fast_period)
        {
            TimeFrame = timeFrame;
            SlowPeriod = slow_period;
            FastPeriod = fast_period;
        }

        public override void RefreshData()
        {
            Slow = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, SlowPeriod, 0, MovingAverageType.SIMPLE, AppliedPrice.TYPICAL, 0);
            Fast = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, FastPeriod, 0, MovingAverageType.EXPONENTIAL, AppliedPrice.TYPICAL, 0);           
        }

        public override Boolean CanBuy()
        {            
            if (Slow < Fast - 20 * Pipster.Point)
            {
                return true;
            }
            return false;
        }


        public override Boolean CanSell()
        {
            if (Slow > Fast + 20 * Pipster.Point)
            {
                return true;
            }
            return false;
        }
    }
}
