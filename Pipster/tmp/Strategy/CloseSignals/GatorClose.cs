﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace PipsterEA.Strategy.CloseSignals
{
    public class GatorClose : CloseSignal
    {
        public TimeFrame TimeFrame { get; set; }


        public GatorClose(TimeFrame timeFrame)
        {
            TimeFrame = timeFrame;
        }
        public override double CloseLots(Model.Order order)
        {
            double up = indicatorService.Gator(Pipster.Symbol, TimeFrame, 13, 8, 8, 5, 5, 3, MovingAverageType.SMOOTHED, AppliedPrice.MEDIAN, IndicatorLine.MODE_UPPER, 0);
            double down = indicatorService.Gator(Pipster.Symbol, TimeFrame, 13, 8, 8, 5, 5, 3, MovingAverageType.SMOOTHED, AppliedPrice.MEDIAN, IndicatorLine.MODE_LOWER, 0);

            if (order.Type == Model.TradeOperation.BUY)
            {
                if (up + down < 0)
                {
                    return order.Lots;
                }
            }

            if (order.Type == TradeOperation.SELL)
            {
                if (up + down > 0)
                {
                    return order.Lots;
                }
            }
            return 0;
        }
    }
}
