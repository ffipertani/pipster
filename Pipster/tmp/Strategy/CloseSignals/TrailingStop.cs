﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace PipsterEA.Strategy.CloseSignals
{
    public class TrailingStop:CloseSignal
    {
        public int FromPips { get; set; }
        public int StopAtPips { get; set; }
        private List<int> managedTickets = new List<int>();

        public TrailingStop() : this(10, 1) { }

        public TrailingStop(int fromPips, int minGain)
        {
            FromPips = fromPips;
            StopAtPips = minGain;                                        
        }

        public override void Init()
        {
            Pipster.OrderClosed += new Core.Pipster.OrderClosedEvent(Pipster_OrderClosed);
            
        }

        public void Pipster_OrderClosed(Order order)
        {
            managedTickets.Remove(order.Ticket);
        }

        public override double CloseLots(Order order)
        {          
            Double pips = Pipster.CountPips(order);
            
            if (!isManaged(order))
            {
                if (pips >= FromPips)
                {
                    managedTickets.Add(order.Ticket);
                }
            }

            if (isManaged(order))
            {
                if (pips <= StopAtPips)
                {
                    return order.Lots;
                }
            }

           
            return 0;
        }
        
       

        private Boolean isManaged(Order order)
        {
            foreach (int ticket in managedTickets)
            {
                if (ticket == order.Ticket)
                {
                    return true;
                }

            }
            return false;
        }
         

    }
}
