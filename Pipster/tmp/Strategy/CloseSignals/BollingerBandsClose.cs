﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace PipsterEA.Strategy.CloseSignals
{
    public class BollingerBandsClose : CloseSignal
    {
        public static int GAP = 50;

        public TimeFrame TimeFrame { get; set; }
        public int Period { get; set; }
        public int Deviation { get; set; }
        public int BandShift { get; set; }
        public AppliedPrice AppliedPrice { get; set; }

        public BollingerBandsClose() : this(TimeFrame.PERIOD_D1, 14, 2, 0, AppliedPrice.CLOSE) { }

        public BollingerBandsClose(TimeFrame timeFrame, int period, int deviation, int band_shift, AppliedPrice appliedPrice)
        {
            TimeFrame = timeFrame;
            Period = period;
            Deviation = deviation;
            BandShift = band_shift;
            AppliedPrice = appliedPrice;
        }

        public override double CloseLots(Model.Order order)
        {
            Double up = indicatorService.BollingerBands(Pipster.Symbol, TimeFrame, Period, Deviation, BandShift, AppliedPrice, IndicatorLine.MODE_UPPER, 0);
            Double down = indicatorService.BollingerBands(Pipster.Symbol, TimeFrame, Period, Deviation, BandShift, AppliedPrice, IndicatorLine.MODE_LOWER, 0);

            int gap = GAP;

            if (order.Type == Model.TradeOperation.BUY)
            {
                /*
                if (Pipster.Bid < down + (gap * Pipster.Point))
                {
                    return order.Lots;
                }
                 * */
                if (Pipster.Bid > up - (gap * Pipster.Point))
                {
                    return order.Lots;
                }
            }

            if (order.Type == TradeOperation.SELL)
            {
                /*
                if (Pipster.Ask > up - (gap * Pipster.Point))
                {
                    return order.Lots;
                }
                 * */
                if (Pipster.Ask < down + (gap * Pipster.Point))
                {
                    return order.Lots;
                }
            }
            return 0;
        }
    }
}
