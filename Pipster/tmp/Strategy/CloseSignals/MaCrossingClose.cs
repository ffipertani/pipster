﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace PipsterEA.Strategy.CloseSignals
{
    public class MaCrossingClose:CloseSignal
    {
        public int FromPips { get; set; }
        public TimeFrame TimeFrame{get;set;}
        public int SlowPeriod { get; set; }
        public int FastPeriod { get; set; }
     

        private List<int> managedTickets = new List<int>();
        

        public MaCrossingClose():this(TimeFrame.PERIOD_D1,20,5,100)
        {                        
        }

        public MaCrossingClose(TimeFrame timeFrame, int slow_period, int fast_period,int fromPips)
        {
            TimeFrame = timeFrame;
            SlowPeriod = slow_period;
            FastPeriod = fast_period;
            FromPips = fromPips;
        }
        

        private Boolean isManaged(Order order)
        {
            foreach (int ticket in managedTickets)
            {
                if (ticket == order.Ticket)
                {
                    return true;
                }

            }
            return false;
        }
        
        public override double CloseLots(Order order)
        {

            Double pips = Pipster.CountPips(order);

            if (!isManaged(order))
            {
                if (pips >= FromPips)
                {
                    managedTickets.Add(order.Ticket);
                }
            }

            if (isManaged(order))
            {
               Double Slow = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, SlowPeriod, 0, MovingAverageType.SIMPLE, AppliedPrice.WEIGHTED, 0);
               Double Fast = indicatorService.MovingAverage(Pipster.Symbol, TimeFrame, FastPeriod, 0, MovingAverageType.EXPONENTIAL, AppliedPrice.TYPICAL, 0);

                if (order.Type == TradeOperation.BUY)
                {
                    if (Fast < Slow)
                    {
                        managedTickets.Remove(order.Ticket);
                        return order.Lots;
                    }
                }
                else if (order.Type == TradeOperation.SELL)
                {
                    if (Fast > Slow)
                    {
                        managedTickets.Remove(order.Ticket);
                        return order.Lots;
                    }
                }
            }

           
            return 0;
        }

       
    }
}
