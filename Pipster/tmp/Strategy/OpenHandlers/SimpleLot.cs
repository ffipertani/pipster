﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace PipsterEA.Strategy.OpenHandlers
{
    public class SimpleLot:OpenHandler
    {
        public double Lots { get; set; }

        public SimpleLot() : this(0.01) { }

        public SimpleLot(double lots)
        {
            Lots = lots;
        }


        public override Order CreateOrder(Order order)
        {
            order.Lots = Lots;
            return order;
        }
     

    }
}
