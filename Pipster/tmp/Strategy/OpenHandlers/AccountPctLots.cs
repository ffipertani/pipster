﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;

namespace PipsterEA.Strategy.OpenHandlers
{
    public class AccountPctLots : OpenHandler
    {
        public int Pct { get; set; }

        public AccountPctLots() : this(3) { }

        public AccountPctLots(int pct)
        {
            Pct = pct;
        }


        public override Order CreateOrder(Order order)
        {
            Double size = tradingService.AccountSize();
            Double lots = size * Pct / 100;
            order.Lots = lots / 1000;
            return order;
        }

    }
}
