﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace PipsterEA.Strategy.OpenHandlers
{
    public class SimpleTakeProfit:OpenHandler
    {
        public int Pips { get; set; }

        public SimpleTakeProfit() : this(0) { }

        public SimpleTakeProfit(int pips)
        {
            Pips = pips;
        }


        public override Order CreateOrder(Order order)
        {
            if (Pips == 0)
            {
                order.TakeProfit = 0;
                return order;
            }
            if (order.Type == TradeOperation.BUY)
            {
                order.TakeProfit = Math.Round(order.OpenPrice + Pips * Pipster.Point, (int)Pipster.Digits); 
            }
            else if (order.Type == TradeOperation.SELL)
            {
                order.TakeProfit = Math.Round(order.OpenPrice - Pips * Pipster.Point, (int)Pipster.Digits); 
            }   
           
            return order;
        }
    }
}
