﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Core;

namespace PipsterEA.Strategy.OpenHandlers
{
    public class SimpleStopLoss:OpenHandler
    {
        public int Pips { get; set; }

        public SimpleStopLoss() : this(0) { }

        public SimpleStopLoss(int pips)
        {
            Pips = pips;
        }

        public override Order CreateOrder(Order order)
        {
            if (Pips == 0)
            {
                order.StopLoss = 0;
                return order;
            }
            if (order.Type == TradeOperation.BUY)
            {
                order.StopLoss =  Math.Round(order.OpenPrice - Pips * Pipster.Point,(int)Pipster.Digits);            
            }
            else if (order.Type == TradeOperation.SELL)
            {
                order.StopLoss = Math.Round(order.OpenPrice + Pips * Pipster.Point, (int)Pipster.Digits) ;            
            }
                     
            return order;
        }
    }
}
