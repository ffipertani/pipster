﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Strategy.OpenSignals;
using PipsterEA.Strategy.CloseSignals;
using PipsterEA.Strategy.OpenHandlers;
using PipsterEA.Strategy.StandbyHandlers;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace PipsterEA.Strategy
{
    public class StrategyRegistry
    {
        public static List<BaseStrategy> OpenSignals = new List<BaseStrategy>();
        public static List<BaseStrategy> CloseSignals = new List<BaseStrategy>();
        public static List<BaseStrategy> OpenHandlers = new List<BaseStrategy>();
        public static List<BaseStrategy> StandbyHandlers = new List<BaseStrategy>();

       static void readLibrary(XmlTextReader reader)
        {
            reader.MoveToFirstAttribute();
            String name = reader.Value;
            while (reader.ReadToFollowing("plugin"))
            {
                String pluginname, description,className;
                if (reader.ReadToFollowing("name"))
                {
                    pluginname = reader.ReadElementContentAsString();
                }
                if (reader.ReadToFollowing("description"))
                {
                    description = reader.ReadElementContentAsString();
                }
                if (reader.ReadToFollowing("className"))
                {
                    className = reader.ReadElementContentAsString();
                }
            }

            
        }

       private static void skip(object o, Object args)
       {
           String pippo;
           pippo = "2";
       }

      
 

       public static  void readConf()
        {
            String filename = "plugins.xml";

            XmlSerializer deserializer = new XmlSerializer(typeof(Configuration));
            XmlWriter writer = XmlWriter.Create(filename);
            Configuration configuration = new Configuration();
           Library l = new Library();
           l.Name="prova.dll";
           l.Plugins.Add(new Plugin(){ClassName="Pipster.prova", Description="Prova123", Name="Pipster"});
           l.Plugins.Add(new Plugin() { ClassName = "Pipster8.prova8", Description = "Prova128883", Name = "Pipster888" });
           configuration.Libraries.Add(l);
           deserializer.Serialize(writer, configuration);

            writer.Close();




           XmlReader textReader = XmlReader.Create(filename);



           
            Configuration libraries;
           XmlDeserializationEvents events = new XmlDeserializationEvents();
           events.OnUnknownAttribute = new XmlAttributeEventHandler(skip);
           events.OnUnknownNode = new XmlNodeEventHandler(skip);
           events.OnUnreferencedObject = new UnreferencedObjectEventHandler(skip);           
           libraries = (Configuration)deserializer.Deserialize(textReader, events);
            textReader.Close();

            



            XmlTextReader reader = new XmlTextReader(filename);

            while (reader.Read())
            {
                String name = reader.Name;
                if(name.Equals("plugins"))
                {
                    continue;
                }
                if (name.Equals("library"))
                {
                    readLibrary(reader);
                }
 
            }

           
        }


        static StrategyRegistry()
        {
            OpenSignals.Add(new BollingerBandsOpen());
            OpenSignals.Add(new BullsBearsPowerOpen());            
            OpenSignals.Add(new MaCrossing());
            OpenSignals.Add(new MaUpDown());
            OpenSignals.Add(new MaxMinBreakout());
            OpenSignals.Add(new StochasticUpDown());
            OpenSignals.Add(new TightLastMaxMin());

            CloseSignals.Add(new BollingerBandsClose());
            CloseSignals.Add(new BullsBearsPowerClose());
            CloseSignals.Add(new FixedStopLossSignal());
            CloseSignals.Add(new MaCrossingClose());
            CloseSignals.Add(new MinMaxClose());
            CloseSignals.Add(new StochasticClose());
            CloseSignals.Add(new TrailingStop());

            OpenHandlers.Add(new AccountPctLots());
            OpenHandlers.Add(new MaxAccountPctLots());
            OpenHandlers.Add(new SimpleLot());
            OpenHandlers.Add(new SimpleStopLoss());
            OpenHandlers.Add(new SimpleTakeProfit());
            OpenHandlers.Add(new TightStopLoss());
            
            StandbyHandlers.Add(new EnvelopesTrend());
            StandbyHandlers.Add(new MaAngleTrend());
            StandbyHandlers.Add(new MaxOrdersStandby());
            StandbyHandlers.Add(new WaitAfterALoss());

        }
        
    }
}
