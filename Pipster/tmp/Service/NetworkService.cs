﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Windows.Forms;
using System.Threading;

namespace PipsterEA.Service
{
    class NetworkService
    {
        private static NetworkService instance = new NetworkService();
        private TcpListener listener;
        private TcpClient client;       
        public  int Port {get;set;}
        public delegate void TickHandler();
        public event TickHandler Tick;
        public delegate void ClientEvent(TcpClient client);
        public event ClientEvent ClientConnected;
        public event ClientEvent ClientError;

        private NetworkService()
        {
            Port = 10000; //
        }

        public static NetworkService GetInstance()
        {
            return instance;
        }

        public void listen( )
        {
            Thread th = new Thread(new ThreadStart(listenThread));


            this.listener = new TcpListener(IPAddress.Any, Port);
            th.Start();         
        }

        public void listenThread()
        {
            try
            {
                this.listener.Start();
            }
            catch (Exception e) { MessageBox.Show("couldn't bind to port " + Port + " -> " + e.Message); return; }
            doStuff();
        }

        private void doStuff()
        {

            try
            {
                client = this.listener.AcceptTcpClient();
                client.ReceiveTimeout = 1000;
                if (ClientConnected != null)
                {
                    ClientConnected(client);
                }
                while (true)
                {
                    String[] response = readResponse();
                    if (response[0].Equals("TICK"))
                    {
                        if (Tick != null)
                        {
                            Tick();
                        }
                    }

                }                
            }
            catch (Exception e)
            {
                if (client.Connected)
                {
                    client.Close();
                }
                if (ClientError != null)
                {
                    ClientError(client);
                }
                doStuff();
            }
        }

        public void NextTick()
        {
            writeCommand(new String[]{"close"});
            /*
            if (client != null && client.Connected)
            {
                client.Close();
            }
             * */
        }

        public void writeCommand(String[] message)
        {                        
            String csvMessage = "";
            foreach(String s in message){
                csvMessage+=s+",";
            }
            csvMessage = csvMessage.Remove(csvMessage.Length-1);
            
            byte[] buffer = System.Text.ASCIIEncoding.ASCII.GetBytes(csvMessage);
            byte[] bufferSize = System.Text.ASCIIEncoding.ASCII.GetBytes(buffer.Length.ToString());
            byte[] newbuffersize = new byte[4];
            bufferSize.CopyTo(newbuffersize, 0);
            client.GetStream().Write(newbuffersize, 0, newbuffersize.Length);
            client.GetStream().Write(buffer, 0, buffer.Length);

            //buffer = new byte[102400];
            //client.GetStream().Read(buffer, 0, buffer.Length);

            //String toreturn = System.Text.ASCIIEncoding.ASCII.GetString(buffer);
            
        }

        public String[] readResponse()
        {
            int length = 0;
            do
            {
                byte[] blength = readAll(4);
                String toreturns = System.Text.ASCIIEncoding.ASCII.GetString(blength);
                toreturns = toreturns.Replace("\0","0");
                if (toreturns.Equals("0000") || toreturns.Equals("\0\0\0\0"))
                {
                    continue;
                }
                //length = blength[0] + blength[1] * 2 + blength[2] * 4 + blength[3] * 8;
                length = Int32.Parse(toreturns);
            } while (length == 0);

            byte[] breturn = readAll(length);
            String toreturn = System.Text.ASCIIEncoding.ASCII.GetString(breturn);
            return toreturn.Split(','); 
        }



        private byte[] readAll(int size)
        {
            byte[] buffer = new byte[size];
            int readed = 0;
            while (readed < size)
            {
                int res = client.GetStream().ReadByte();
                //int res = client.GetStream().Read(buffer, readed, size-readed);
                if (res == 0)
                {
                    continue;
                }
                buffer[readed++] = (byte)res;
                continue;
                if (res == 0)
                {                     
                    return null;
                }
                //Print("Letto " + res + " bytes");
                readed += res;
            }
            return buffer;
        }
    }
}
