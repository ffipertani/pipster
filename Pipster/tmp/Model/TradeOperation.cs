﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PipsterEA.Model
{
    public enum TradeOperation
    {        
        BUY = 0,
        SELL = 1,
        BUY_LIMIT =2,
        SELL_LIMIT =3,
        BUY_STOP =4,
        SELL_STOP = 5       
    }
}
