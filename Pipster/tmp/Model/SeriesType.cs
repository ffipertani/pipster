﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PipsterEA.Model
{
    public enum SeriesType
    {
        OPEN = 0,
        LOW = 1,
        HIGH = 2,
        CLOSE = 3,
        VOLUME = 4,
        TIME = 5
    }
}
