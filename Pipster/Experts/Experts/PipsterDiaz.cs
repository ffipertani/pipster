﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using Signals.OpenSignals;
using Signals.OpenHandlers;
using Signals.StandbyHandlers;
using Signals.CloseSignals;
using PipsterEA.Model;

namespace Experts.Experts
{
    public class PipsterDiaz:Pipster
    {
        public PipsterDiaz()
        {
            Enabled = true;
            RegisterOpenSignal(new MacdOpen() { TimeFrame = PipsterEA.Model.TimeFrame.PERIOD_H1, StrongOpen=true });
            RegisterOpenSignal(new MacdOpen() { TimeFrame = PipsterEA.Model.TimeFrame.PERIOD_D1 });
            RegisterOpenSignal(new TightLastMaxMin(PipsterEA.Model.TimeFrame.PERIOD_H1, 3, 30));
            RegisterOpenSignal(new MaxMinBreakout(TimeFrame.PERIOD_H1, 5,0));
            RegisterOpenSignal(new RsiOpen() { TimeFrame = TimeFrame.PERIOD_D1 });
          // RegisterOpenSignal(new StochasticUpDown(TimeFrame.PERIOD_H1, 13, 5, 13));
           //RegisterOpenSignal(new AllowDifferentOpen());
            /*
            RegisterCloseSignal(new TrailingStop(20, -10));
            
            
            */
            RegisterCloseSignal(new TrailingStop(100, 50));
            RegisterCloseSignal(new TrailingStop(50, 0));

          //  RegisterCloseSignal(new DiazClose());
            RegisterCloseSignal(new AtrClose() { Days = 21 });
            //RegisterCloseSignal(new TightClose(PipsterEA.Model.TimeFrame.PERIOD_H1, 3, 15));
            RegisterOpenHandler(new SimpleLot(0.1));
            //RegisterOpenHandler(new AccountPctLots(34));
            RegisterStandbyHandler(new MaxOrdersStandby(1));
           // RegisterStandbyHandler(new TradeOnlySession() { TradingSession = PipsterEA.Model.TradingSession.Asian });
            
          //  RegisterStandbyHandler(new MaAngleTrend(PipsterEA.Model.MarketStatus.Trending, PipsterEA.Model.TimeFrame.PERIOD_D1, 34, 13));
           // RegisterOpenHandler(new SimpleTakeProfit(200));
           // RegisterOpenHandler(new SimpleStopLoss(60));
           // RegisterOpenHandler(new TightStopLoss( PipsterEA.Model.TimeFrame.PERIOD_H1,5,15));
        }

      
    }
}
