﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;
using Signals.CloseSignals;
using Signals.OpenSignals;
using Signals.OpenHandlers;

namespace Experts.Experts
{
    public class PipsterDay : Pipster
    {
        int MEDIUM_DELTA_PERIOD = 5;
        DateTime currentDate;
        DateTime prevDate;
        int openAfterPips = 0;
        int atrPips = 0;
        Double openingAsk = 0;
        Double openingBid = 0;
        SimpleLot apl = new SimpleLot(0.2);
        SimpleLot aplSmall = new SimpleLot(0.01);
        private Boolean wasStopLoss = false;

        public PipsterDay()
        {
            Description = "PipsterDay";
            apl.SetPipster(this);
            aplSmall.SetPipster(this);
            OrderClosed += new OrderClosedEvent(PipsterDay_OrderClosed);
        }

        public override void Start()
        {
            refresh();
         //   atrPips = AtrPipsDelta();
            atrPips = AtrPipsDelta() + 10;

            if (ManagedTickets.Count > 0)
            {
                close();
            }
           
            if(ManagedTickets.Count==0)
            {
                open();
            }         
        }

        protected override void open()
        {
            try
            {
                DateTime now = Now;

                if (currentDate == null)
                {
                    currentDate = now;
                    currentDate.AddDays(-1);
                }


                if (now.Hour <=5 && now.Day != currentDate.Day && now.DayOfWeek != DayOfWeek.Friday)
                { 
                    openAfterPips = atrPips;
                    openingAsk = Ask;
                    openingBid = Bid;
                    currentDate = now;

                    directOpen();
                    //reverseOpen();
                }



            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        protected void directOpen()
        {
            MaUpDown ma = new MaUpDown(TimeFrame.PERIOD_D1, 5, MovingAverageType.SIMPLE, 3, MovingAverageType.EXPONENTIAL);
            ma.SetPipster(this);
            ma.RefreshData();

            if (true || ma.CanBuy())
            {

                Order order = new Order();
                order.OpenPrice = openingAsk + (openAfterPips * Point);
                order.TakeProfit = order.OpenPrice + ((AtrMaxPips() + AtrMinPips()) * Point);
                order.StopLoss = openingAsk;
                order.Type = TradeOperation.BUY_STOP;
                order.Lots = 0.1;
                order.Symbol = Symbol;
                order.Comment = Description;
                if (wasStopLoss)
                {
                    order = aplSmall.CreateOrder(order);
                }
                else
                {
                    order = apl.CreateOrder(order);
                }
                int ticket = tradingService.OpenOrder(order);

                ManagedTickets.Add(ticket);
            }

            if (true || ma.CanSell())
            {
                Order order = new Order();
                order.OpenPrice = openingAsk - (openAfterPips * Point);
                order.TakeProfit = order.OpenPrice - ((AtrMaxPips() - AtrMinPips()) * Point);
                order.StopLoss = openingAsk;
                order.Type = TradeOperation.SELL_STOP;
                order.Lots = 0.1;
                order.Comment = Description;
                order.Symbol = Symbol;

                if (wasStopLoss)
                {
                    order = aplSmall.CreateOrder(order);
                }
                else
                {
                    order = apl.CreateOrder(order);
                }
                int ticket = tradingService.OpenOrder(order);
                ManagedTickets.Add(ticket);


            }
        }

        void PipsterDay_OrderClosed(Order orderClosed)
        {
            /*
            if (orderClosed == null || orderClosed.IsStopLoss())
            {
                List<Order> tmpOrders = new List<Order>(ManagedOrders);
                foreach (Order order in tmpOrders)
                {
                    doClose(order,order.Lots);
                }
                
                directOpen();
            }
            */
            /*
            if (order.Profit < 0)
            {
                wasStopLoss = true;
            }
            else
            {
                wasStopLoss = false;
            }
            */
        }

        protected void reverseOpen()
        {
            MaUpDown ma = new MaUpDown(TimeFrame.PERIOD_D1, 5, MovingAverageType.SIMPLE, 3, MovingAverageType.EXPONENTIAL);
            ma.SetPipster(this);
            ma.RefreshData();

            if (true || ma.CanBuy())
            {

                Order order = new Order();
                order.OpenPrice = openingAsk + (openAfterPips * Point);
                order.StopLoss= order.OpenPrice + (AtrMaxPips() * Point);
                order.TakeProfit = openingAsk;
                order.Type = TradeOperation.SELL_LIMIT;
                order.Lots = 0.1;
                order.Symbol = Symbol;
                order.Comment = Description;

                order = apl.CreateOrder(order);

                int ticket = tradingService.OpenOrder(order);

                ManagedTickets.Add(ticket);
            }

            if (true || ma.CanSell())
            {
                Order order = new Order();
                order.OpenPrice = openingAsk - (openAfterPips * Point);
                order.StopLoss = order.OpenPrice - (AtrMaxPips() * Point);
                order.TakeProfit= openingAsk;
                order.Type = TradeOperation.BUY_LIMIT;
                order.Lots = 0.1;
                order.Comment = Description;
                order.Symbol = Symbol;

                order = apl.CreateOrder(order);
                int ticket = tradingService.OpenOrder(order);
                ManagedTickets.Add(ticket);


            }
        }

        protected override void close()
        {
            List<Order> tmpOrders = new List<Order>(ManagedOrders);
        /*
            Boolean active = false;
            foreach (Order order in tmpOrders)
            {
                if (!order.IsPending())
                {
                    active = true;
                    break;
                }
            }

            foreach (Order order in tmpOrders)
            {
                if (order.IsPending())
                {
                    tradingService.DeleteOrder(order.Ticket);
                }
            }
            */
            foreach (Order order in tmpOrders)
            {
                if (order.IsClosed())
                {
                    continue;
                }
                if (isNewDay(order))
                {                                          
                     doClose(order, order.Lots);                     
                }
                if (isCameBack(order))
                {
                   // doClose(order, order.Lots);
                }

                Double max = indicatorService.MediumMaxMovement(Symbol, TimeFrame.PERIOD_D1, MEDIUM_DELTA_PERIOD);
                Double min = indicatorService.MediumMinMovement(Symbol, TimeFrame.PERIOD_D1, MEDIUM_DELTA_PERIOD);
                Double atr = max - min;

                int atrPips = getPips(atr);
                //openAfterPips = openAfterPips / 2;
                Double TakeProfit = atrPips;
                Double StopLoss = getPips(min + max);

                Double pips = CountPips(order);
                if (pips > TakeProfit)
                {
                    doClose(order, order.Lots);
                }

                if (pips < -StopLoss)
                {
                    doClose(order, order.Lots);
                }


            }
        }

       
        protected override void doClose(Order order, Double lots)
        {
            if (order.IsPending())
            {
                tradingService.DeleteOrder(order.Ticket);
                ManagedTickets.Remove(order.Ticket);
                refreshOrders();
            }
            else
            {
                base.doClose(order, lots);
            }            
        }
        

        private Boolean isCameBack(Order order)
        {
            Double price = indicatorService.Open(Symbol, TimeFrame.PERIOD_D1, 0);
            if (order.Type == TradeOperation.BUY)
            {
                if (Ask <= price)
                {
                    return true;
                }
            }
            if (order.Type == TradeOperation.SELL)
            {
                if (Bid >= price)
                {
                    return true;
                }
            }

            return false;
        }

        private Boolean isNewDay(Order order)
        {
            DateTime now = Now;
            if (now.Day != order.OpenTime.Day)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private int getPips(Double price)
        {
            String points = (price / Point).ToString();
            if (points.IndexOf(",") > 0)
            {
                points = points.Substring(0, points.IndexOf(","));
            }
            return int.Parse(points);
        }


        private int AtrPipsDelta()
        {
            Double max = indicatorService.MediumMaxMovement(Symbol, TimeFrame.PERIOD_D1, MEDIUM_DELTA_PERIOD);
            Double min = indicatorService.MediumMinMovement(Symbol, TimeFrame.PERIOD_D1, MEDIUM_DELTA_PERIOD);
            Double atr = max - min;

            int atrPips = getPips(atr);
            return atrPips;
        }

        private int AtrMaxPips()
        {
            Double atr = indicatorService.MediumMaxMovement(Symbol, TimeFrame.PERIOD_D1, MEDIUM_DELTA_PERIOD);
            //  Double atr = indicatorService.Atr(Symbol, TimeFrame.PERIOD_D1, MEDIUM_DELTA_PERIOD, 0);
            String points = (atr / Point).ToString();
            if (points.IndexOf(",") > 0)
            {
                points = points.Substring(0, points.IndexOf(","));
            }
            int atrPips = int.Parse(points);
            return atrPips;
        }

        private int AtrMinPips()
        {
            Double atr = indicatorService.MediumMinMovement(Symbol, TimeFrame.PERIOD_D1, MEDIUM_DELTA_PERIOD);
            //  Double atr = indicatorService.Atr(Symbol, TimeFrame.PERIOD_D1, MEDIUM_DELTA_PERIOD, 0);
            String points = (atr / Point).ToString();
            if (points.IndexOf(",") > 0)
            {
                points = points.Substring(0, points.IndexOf(","));
            }
            int atrPips = int.Parse(points);
            return atrPips;
        }

        private Boolean isNewHour()
        {
            DateTime now = Now;
            if (now.Hour != prevDate.Hour || now.Day != prevDate.Day)
            {
                prevDate = now;
                return true;
            }
            else
            {
                prevDate = now;
                return false;
            }

        }
    }
}

