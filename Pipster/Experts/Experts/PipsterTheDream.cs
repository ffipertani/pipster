﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;
using Signals.CloseSignals;
using Signals.OpenHandlers;
using Signals.StandbyHandlers;

namespace Experts.Experts
{
    public class PipsterTheDream : Pipster
    {
        int MEDIUM_DELTA_PERIOD = 21;
        DateTime currentDate;
        DateTime prevDate;
        TrailingStop trailingStop = new TrailingStop(150,13);
        int openAfterPips=0;
        int atrPips= 0;
        Double openingAsk = 0;
        Double openingBid = 0;
        Boolean canOpen = false;
        

        public PipsterTheDream():base("EURUSD")
        {
            Description = "TheDream";
           RegisterCloseSignal(trailingStop);
           //RegisterCloseSignal(new TrailingStop(34, 2));
            RegisterOpenHandler(new AccountPctLots(34));
            RegisterStandbyHandler(new MaxOrdersStandby(1));
           // RegisterStandbyHandler(new MaAngleTrend(MarketStatus.Trending, TimeFrame.PERIOD_D1, 34, 21));
        }

        public override void Start()
        {
            base.Start();
            atrPips = AtrPips();
            
            if(!isNewHour()){
                return;
            }
            if (ManagedTickets.Count > 0)
            {
                close();
            }

            if (!isStandby() )
            {
                open();                
            }
        }

        protected override void open()
        {
            try
            {
                DateTime now = Now;

                if (currentDate == null)
                {
                    currentDate = now;
                    currentDate.AddDays(-1);
                }


                if (now.Hour == 0 && now.Day != currentDate.Day)
                {                     
                    openAfterPips = atrPips;                                                 
                    openingAsk = Ask;
                    openingBid = Ask;
                    currentDate = now;
                    canOpen = true;
                }

                if (canOpen)
                {
                    if (CanBuy())
                    {
                        buy();
                    }
                    if (!isStandby())
                    {
                        if (CanSell())
                        {
                            sell();
                        }
                    }
                }
            
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
      
        protected override void close()
        {
            List<Order> tmpOrders = new List<Order>(ManagedOrders);
            foreach (Order order in tmpOrders)
            {
                if (order.IsClosed())
                {
                    continue;
                }                
                int atrPips = AtrPips();

                Double TakeProfit = AtrPips();
                Double StopLoss = (AtrPipsDelta())*3;

                Double pips = CountPips(order);
                if (pips > TakeProfit)
                {
                    doClose(order, order.Lots);
                }

                if (pips < -StopLoss)
                {
                    doClose(order, order.Lots);
                }
                
                
               
            }

            base.close();
        }


        public Boolean CanBuy()
        {
            if (Ask > openingAsk + (openAfterPips * Point))
            {              
                openingAsk = Ask + (atrPips * Point);
                canOpen = false;
                return true;
            }
            
            return false;
        }


        public Boolean CanSell()
        {
            if (Ask < openingBid - (openAfterPips * Point))
            {                 
                openingBid = Ask - (atrPips * Point);
                canOpen = false;
                return true;
            }
            return false;
        }

        private int getPips(Double price)
        {
            String points = (price / Point).ToString();
            if (points.IndexOf(",") > 0)
            {
                points = points.Substring(0, points.IndexOf(","));
            }
            return int.Parse(points);
        }

        private int AtrPipsDelta()
        {
            Double max = indicatorService.MediumMaxMovement(Symbol, TimeFrame.PERIOD_D1, MEDIUM_DELTA_PERIOD);
            Double min = indicatorService.MediumMinMovement(Symbol, TimeFrame.PERIOD_D1, MEDIUM_DELTA_PERIOD);
            Double atr = max - min;

            int atrPips = getPips(atr);
            return atrPips;
        }

        private int AtrMaxPips()
        {
            Double atr = indicatorService.MediumMaxMovement(Symbol, TimeFrame.PERIOD_D1, MEDIUM_DELTA_PERIOD);
            int atrPips = getPips(atr);
            return atrPips;
        }

        private int AtrMinPips()
        {
            Double atr = indicatorService.MediumMinMovement(Symbol, TimeFrame.PERIOD_D1, MEDIUM_DELTA_PERIOD);
            int atrPips = getPips(atr);
            return atrPips;
        }


        private int AtrPips()
        {
            Double atr = indicatorService.Atr(Symbol, TimeFrame.PERIOD_D1, 34, 0);
            String points = (atr / Point).ToString();
            if (points.IndexOf(",") > 0)
            {
                points = points.Substring(0, points.IndexOf(","));
            }
            int atrPips = int.Parse(points);
            return atrPips;
        }

        private Boolean isNewHour()
        {
            DateTime now = Now;
            if (now.Hour != prevDate.Hour || now.Day != prevDate.Day)
            {
                prevDate = now;
                return true;
            }
            else
            {
                prevDate = now;
                return false;
            }

        }

    }
}
