﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Service;
using Signals;
using PipsterEA.Model;
using PipsterEA.Core;
using Signals.OpenSignals;
using Signals.OpenHandlers;
using Signals.CloseSignals;
using Signals.StandbyHandlers;

namespace PipsterEA.Experts
{
    public class WeeklyPipster : Pipster
    {
        private static String symbol = "EURUSD";
        private static TimeFrame timeFrame = TimeFrame.PERIOD_MN1;
        private static int waitHour = 50;

        private WaitAfterALoss waitAfterALoss = new WaitAfterALoss(60 * waitHour);


        private MarketStatus lastStatus;


        public WeeklyPipster()
            : base(symbol)
        {            
            RegisterStandbyHandler(new MaxOrdersStandby(1));
            RegisterStandbyHandler(new EnvelopesTrend(MarketStatus.Trending, timeFrame, 26, 6,10));
            RegisterStandbyHandler(new WaitAfterALoss(60 * 24 * 10));
            //RegisterOpenHandler(new SimpleLot(0.1));
            //  RegisterCloseHandler(waitAfterALoss);
            RegisterOpenHandler(new SimpleLot { Lots = 0.1 });
           // RegisterOpenHandler(new AccountPctLots(10));
            RegisterOpenHandler(new SimpleStopLoss(0));
            RegisterOpenHandler(new SimpleTakeProfit(1000));

           // RegisterOpenHandler(new TightStopLoss(timeFrame, 2, 50));
            RegisterOpenSignal(new MaxMinBreakout(timeFrame, 2,0));
            RegisterOpenSignal(new StochasticUpDown(timeFrame, 14, 5, 3));
            RegisterOpenSignal(new MaUpDown(timeFrame, 52, MovingAverageType.SIMPLE, 2,MovingAverageType.EXPONENTIAL));
            RegisterOpenSignal(new BullsBearsPowerOpen(timeFrame, 5));

            RegisterCloseSignal(new FixedStopLossSignal(200));
            RegisterCloseSignal(new BullsBearsPowerClose { TimeFrame = timeFrame, Period = 7 });
            RegisterCloseSignal(new TrailingStop(200, 50));
            RegisterCloseSignal(new TrailingStop(400, 100));
            RegisterCloseSignal(new TrailingStop(500, 200));
            //RegisterCloseSignal(new MinMaxClose(timeFrame, 7));

            
        }

       
         



    }
}
