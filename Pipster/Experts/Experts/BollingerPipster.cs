﻿using System;
using System.Collections.Generic;
using System.Text;
using Signals;
using PipsterEA.Model;
using PipsterEA.Core;
using Signals.OpenSignals;
using Signals.OpenHandlers;
using Signals.CloseSignals;
using Signals.StandbyHandlers;

namespace PipsterEA.Experts
{
    public class BollingerPipster : Pipster
    {
        private static String symbol = "EURUSD";
        private static TimeFrame timeFrame = TimeFrame.PERIOD_MN1;
        private static int waitHour = 50;

        private WaitAfterALoss waitAfterALoss = new WaitAfterALoss(60 * waitHour);

        
        public BollingerPipster()
            : base(symbol)
        {            
            RegisterStandbyHandler(new MaxOrdersStandby(1));
            RegisterStandbyHandler(new EnvelopesTrend(MarketStatus.Sideway, timeFrame, 36, 6,10));
            RegisterStandbyHandler(new WaitAfterALoss(60 * 24 * 10));
                        
            RegisterOpenHandler(new AccountPctLots(40));
            RegisterOpenHandler(new SimpleStopLoss(1000));
            RegisterOpenHandler(new SimpleTakeProfit(500));
            RegisterOpenHandler(new TightStopLoss(TimeFrame.PERIOD_D1, 3, 15));

           // RegisterOpenSignal(new MaxMinBreakout(TimeFrame.PERIOD_D1, 1));
            RegisterOpenSignal(new BollingerBandsOpen { TimeFrame = timeFrame });

            RegisterCloseSignal(new BollingerBandsClose { TimeFrame = timeFrame });
            
        }

       
         



    }
}
