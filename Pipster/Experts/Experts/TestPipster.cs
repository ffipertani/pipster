﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Service;
using Signals;
using PipsterEA.Model;
using PipsterEA.Core;
using Signals.OpenSignals;
using Signals.OpenHandlers;
using Signals.CloseSignals;
using Signals.StandbyHandlers;

namespace PipsterEA.Experts
{
    public class TestPipster : Pipster
    {
        private static String symbol = "EURUSD";
        private static TimeFrame timeFrame = TimeFrame.PERIOD_D1;
        private static int waitHour = 12;

        private WaitAfterALoss waitAfterALoss = new WaitAfterALoss(60 * waitHour);


        private MarketStatus lastStatus;


        public TestPipster()
            : base(symbol)
        {
            RegisterStandbyHandler(new MaxOrdersStandby(1));
            
            //RegisterOpenHandler(new AccountPctLots(40));
            RegisterOpenHandler(new SimpleLot(0.1));
            RegisterStandbyHandler(waitAfterALoss);
 

            registerTrendSignals();

           
          //  Tick += new TickEvent(TestPipster_Tick);

        }


        /*
        void TestPipster_Tick(Pipster sender)
        {
            if (ManagedTickets.Count == 0)
            {
                if (marketStatus == MarketStatus.Sideway)
                {
                    if (lastStatus != MarketStatus.Sideway)
                    {
                        registerSidewaySignals();
                    }

                }
                else if (marketStatus == MarketStatus.Trending)
                {
                    if (lastStatus != MarketStatus.Trending)
                    {
                        registerTrendSignals();
                    }
                }
                lastStatus = marketStatus;
            }
            
        }

       */
        
        private void registerTrendSignals()
        {
            ClearSignals();
            waitAfterALoss.Minutes = 60 * 8;
            RemoveHandler(typeof(SimpleStopLoss));
            RemoveHandler(typeof(SimpleTakeProfit));

            RegisterOpenHandler(new SimpleStopLoss(600));
            RegisterOpenHandler(new SimpleTakeProfit(1000));

            RegisterOpenHandler(new TightStopLoss(TimeFrame.PERIOD_D1, 3, 125));
            RegisterOpenSignal(new MaxMinBreakout(TimeFrame.PERIOD_D1, 1,0));
            RegisterOpenSignal(new MaUpDown(TimeFrame.PERIOD_D1, 56, MovingAverageType.SIMPLE, 2, MovingAverageType.EXPONENTIAL));
            RegisterOpenSignal(new BullsBearsPowerOpen { TimeFrame = TimeFrame.PERIOD_D1, Period = 14 });
            RegisterCloseSignal(new BullsBearsPowerClose { TimeFrame = TimeFrame.PERIOD_D1, Period = 14 });
            RegisterCloseSignal(new MinMaxClose(TimeFrame.PERIOD_D1, 4));
        //    RegisterCloseSignal(new MaCrossingClose(TimeFrame.PERIOD_D1,26, 2, 300));
           

        }

        private void registerSidewaySignals()
        {

            ClearSignals();

            waitAfterALoss.Minutes = 60 * 8;
            RemoveHandler(typeof(SimpleStopLoss));
            RemoveHandler(typeof(SimpleTakeProfit));
            RemoveHandler(typeof(TightStopLoss));

            RegisterOpenHandler(new SimpleStopLoss(1000));
            RegisterOpenHandler(new SimpleTakeProfit(500));

            RegisterOpenSignal(new MaxMinBreakout(TimeFrame.PERIOD_D1, 1,0));
            /*
           RegisterOpenSignal(new StochasticUpDown(TimeFrame.PERIOD_D1,14,7,5));
           RegisterCloseSignal(new StochasticClose(TimeFrame.PERIOD_D1,14,7,5));
             */

            RegisterOpenSignal(new BollingerBandsOpen { TimeFrame = TimeFrame.PERIOD_D1 });
            RegisterCloseSignal(new BollingerBandsClose { TimeFrame = TimeFrame.PERIOD_D1 });
          

           RegisterOpenHandler(new TightStopLoss(TimeFrame.PERIOD_D1, 3, 15));


                              
        }



    }
}
