﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using System.Windows.Forms;
using Signals.Common;
using PipsterEA.Utils;
using System.Collections;
using PipsterEA.Model;
using System.Net;
using System.IO;

namespace Experts.Experts
{
    public class PipsterFreeWebSignal : Pipster
    {

        List<Signal> toOpen = new List<Signal>();
        List<Signal> toClose = new List<Signal>();
        List<Signal> toUpdate = new List<Signal>();
        List<Signal> newSignals = new List<Signal>();

        Dictionary<int, Signal> signals = new Dictionary<int, Signal>();
        WebBrowser wb = new WebBrowser();
        
        static String URL = "http://foresignal.com/";
        //static String URL = "http://ninjaproxy.cc/webproxy.php?u=Oi8vZm9yZXNpZ25hbC5jb20v&b=1&f=norefer#totals";
          

        public PipsterFreeWebSignal()
        {
           //s WebRequest.DefaultWebProxy = new WebProxy();
            URLSecurityZoneAPI.InternetSetFeatureEnabled(URLSecurityZoneAPI.InternetFeaturelist.DISABLE_NAVIGATION_SOUNDS, URLSecurityZoneAPI.SetFeatureOn.PROCESS, true);
             
                wb.ScriptErrorsSuppressed = true;
                wb.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(wb_DocumentCompleted);
            

                wb.Url = new Uri(URL);
                wb.Refresh();
              
        }

        private void refresh()
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(URL);
            Uri uri = new Uri("http://123.150.159.242");
            webRequest.Proxy = new WebProxy(uri);

            HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();
            Stream receiveStream = response.GetResponseStream();

          
            wb.DocumentStream = receiveStream;
            wb_DocumentCompleted(null, null);
        }
       
      


        public override void Start()
        {
            Symbol = "USDJPY";
          // refresh();

            lock (this)
            {

                foreach (Signal signal in toClose)
                {
                    close(signal);
                }
                toClose.Clear();
                foreach (Signal signal in toOpen)
                {
                    open(signal);
                }
                toOpen.Clear();
                foreach (Signal signal in toUpdate)
                {
                    update(signal);
                }
                toUpdate.Clear();
            }
        }

        void wb_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            lock (this)
            {               
                var doc = wb.Document;
                readHtml(doc);
            }
        }

      

        private void addNewSignal(Signal signal)
        {           
            Signal toRemove = null;
            foreach (Signal tmp in newSignals)
            {
                if (isSameSignal(signal, tmp))
                {
                    toRemove = tmp;
                    break;
                }
            }
            if (toRemove != null)
            {
                newSignals.Remove(toRemove);               
            }
            newSignals.Add(signal);
        }

        private void readHtml(HtmlDocument doc)
        {
            //var z='.61 072384,59';function f(s){var i=0;for (i=0;i<s.length;i++){document.write(z.charAt(s.charCodeAt(i)-65-i));}}

            newSignals = new List<Signal>();
            toOpen = new List<Signal>();
            toClose = new List<Signal>();
            toUpdate = new List<Signal>();
             HtmlElement container = doc.GetElementById("container");
             HtmlElement table = container.GetElementsByTagName("table")[0];

            HtmlElementCollection rows = table.GetElementsByTagName("tr");
            for (int i = 0; i < rows.Count; i++)
            {                
                HtmlElementCollection cells = rows[i].GetElementsByTagName("td");
                foreach (HtmlElement td in cells)
                {
                    Signal signal = readSignal(td);
                    if (signal != null)
                    {
                        addNewSignal(signal);
                    }
                }
                i++;
            }
            
            List<Signal> savedSignals = new List<Signal>();
            IEnumerator<Signal> enumerator = signals.Values.GetEnumerator();
            while (enumerator.MoveNext())
            {
                savedSignals.Add(enumerator.Current);
            }
            foreach (Signal savedSignal in savedSignals)
            {
                foreach (Signal newSignal in newSignals)
                {
                    if (isSameSignal(newSignal, savedSignal))
                    {
                        if (isClosed(newSignal))
                        {
                            toClose.Add(newSignal);
                        }

                        if (isChanged(newSignal, savedSignal))
                        {
                            toUpdate.Add(newSignal);
                        }
                    }
                }
            }
            foreach (Signal newSignal in newSignals)
            {
                Boolean found = false;
                foreach (Signal savedSignal in savedSignals)
                {
                    if (isSameSignal(newSignal, savedSignal))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    if (isOpened(newSignal))
                    {
                        toOpen.Add(newSignal);
                    }
                }
            }
            wb.Refresh();
        }


        private Signal readSignal(HtmlElement td)
        {
            try
            {
                Signal signal = new Signal();
                signal.Symbol = getText(td.Id);
                signal.Action = getText(td.GetElementsByTagName("span")[1].InnerText);
                if (signal.Action == "Filled" || signal.Action == "Cancelled")
                {
                    signal.Status = "Chiuso";
                    HtmlElementCollection closedprices = td.GetElementsByTagName("div");
                    String entry = closedprices[0].InnerText;
                    entry = entry.Substring(entry.IndexOf("at "));
                    entry = entry.Substring(2, entry.IndexOf("\r\n") - 2);
                    signal.EntryPoint = Conversion.getDouble(entry);
                    return signal;
                }
                else
                {
                    signal.Status = "Attivo";
                }
                if (signal.Action == "Sell")
                {
                    signal.Action = "VENDI";
                }
                if (signal.Action == "Buy")
                {
                    signal.Action = "COMPRA";
                }
                HtmlElementCollection prices = td.GetElementsByTagName("font");
                signal.EntryPoint = Conversion.getDouble(prices[0].InnerText);
                signal.StopLoss = Conversion.getDouble(prices[2].InnerText);
                signal.TakeProfit = Conversion.getDouble(prices[1].InnerText);

                return signal;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private HtmlElement findElementByClass(HtmlElement parent, String css){
            foreach (HtmlElement child in parent.Children)
            {
                if (css.Equals(child.GetAttribute("class")))
                {
                    return child;
                }
                HtmlElement finded = findElementByClass(child, css);
                if (finded != null)
                {
                    return finded;
                }
            }
            return null;
        }

        private int getTicket(Signal signal)
        {
             IEnumerator<int> enumerator = signals.Keys.GetEnumerator();
             while (enumerator.MoveNext())
             {
                 int key = enumerator.Current;
                 Signal value;
                 signals.TryGetValue(key, out value);
                 if (isSameSignal(signal, value))
                 {
                     int ticket = key;
                     return ticket;
                 }
             }
             return -1;
        }

        private Order createOrder(Signal signal)
        {
            Order order = new Order();
            order.Lots = 0.1;
            if (signal.Action.Equals("VENDI"))
            {
                order.Type = TradeOperation.SELL;
                order.OpenPrice = tradingService.Bid(signal.Symbol);
            }
            else if (signal.Action.Equals("COMPRA"))
            {
                order.Type = TradeOperation.BUY;
                order.OpenPrice = tradingService.Ask(signal.Symbol);
            }

            order.Symbol = signal.Symbol;
            order.Comment = "By Signal";
            order.TakeProfit = signal.TakeProfit;
            order.StopLoss = signal.StopLoss;
         
            return order;
        }

        public void open(Signal signal)
        {
            Order order = createOrder(signal);
            order.StopLoss = 0;
            order.TakeProfit = 0;
            if (order.Type == TradeOperation.BUY)
            {
                if (tradingService.Ask(signal.Symbol) > signal.EntryPoint + 10 * tradingService.Point(signal.Symbol))
                {
                    return;
                }
            }
            if (order.Type == TradeOperation.SELL)
            {
                if (tradingService.Bid(signal.Symbol) < signal.EntryPoint - 10 * tradingService.Point(signal.Symbol))
                {
                    return;
                }
            }
            int ticket = tradingService.OpenOrder(order);
            if (ticket > 0)
            {
                signals.Add(ticket, signal);
                update(signal);
            }
       }

        public void update(Signal signal)
        {
            int ticket = getTicket(signal);
            if (ticket > 0)
            {
                Order order = createOrder(signal);
                order.Ticket = ticket;
                tradingService.EditOrder(order);
                signals.Add(ticket, signal);
            }
        }

        public void close(Signal signal)
        {
            int ticket = getTicket(signal);
            Order order = tradingService.LoadOrder(ticket);
            doClose(order, order.Lots);
            signals.Remove(ticket);
        }

        private Boolean isOpened(Signal signal)
        {
            if (signal.Status.Equals("Attivo"))
            {
                return true;
            }
            return false;
        }

        private Boolean isSameSignal(Signal s1, Signal s2)
        {
            if (s1.EntryPoint == s2.EntryPoint && s1.Symbol.Equals(s2.Symbol))
            {
                return true;
            }
            return false;
        }

        private Boolean isChanged(Signal s1, Signal s2)
        {
            if (s1.StopLoss != s2.StopLoss)
            {
                return true;
            }
            if (s2.TakeProfit != s2.TakeProfit)
            {
                return true;
            }
            return false;
        }

        private Boolean isClosed(Signal signal){
            if (signal.Status.Equals("Chiuso"))
            {
                return true;
            }
            return false;
        }


        private String getText(String source)
        {
            source = source.Replace("\r\n", "");
            source = source.Replace(" ", "");
            source = source.Replace("\"", "");
            return source;
        }
    }
     
   

    /*
    <table id="gvSignalsLong" >
			<tr >
				<th >Coppia</th><th>Azione</th><th>Punto  d&#39;ingresso</th><th>Stop Loss</th><th>Take Profit</th><th>Status</th><th>Commenti</th><th>&nbsp;</th><th>&nbsp;</th>
			</tr><tr>
				<td>USD/JPY</td><td><span>VENDI <span/></td><td></td><td>&nbsp;</td><td>&nbsp;</td><td>Prepararsi</td><td><span>&nbsp;<span/></td><td>&nbsp;</td><td></td>
			</tr>
            <tr>
				<td>GBP/USD</td><td><span>COMPRA <span/></td><td></td><td>&nbsp;</td><td>&nbsp;</td><td>Prepararsi</td><td><span>&nbsp;<span/></td><td>&nbsp;</td><td></td>
			</tr>
		</table>
     */
}
