﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using System.Windows.Forms;
using Signals.Common;
using PipsterEA.Utils;
using System.Collections;
using PipsterEA.Model;

namespace Experts.Experts
{
    public class PipsterWebSignal : Pipster
    {

        List<Signal> toOpen = new List<Signal>();
        List<Signal> toClose = new List<Signal>();
        List<Signal> toUpdate = new List<Signal>();
        List<Signal> newSignals = new List<Signal>();

        Dictionary<int, Signal> signals = new Dictionary<int, Signal>();
        WebBrowser wbLong = new WebBrowser();
        WebBrowser wbShort = new WebBrowser();
        
        public Boolean LongTerm = false;

        static String LONG_TERM_URL = "http://www.fxbrokersignals.com/longsignals_IT.aspx";
        static String SHORT_TERM_URL = "http://www.fxbrokersignals.com/shortsignals_IT.aspx";

        static String LONG_TERM_ELEMENT = "gvSignalsLong";
        static String SHORT_TERM_ELEMENT = "gvSignalsShort";


        public PipsterWebSignal()
        {
            URLSecurityZoneAPI.InternetSetFeatureEnabled(URLSecurityZoneAPI.InternetFeaturelist.DISABLE_NAVIGATION_SOUNDS, URLSecurityZoneAPI.SetFeatureOn.PROCESS, true);
            if (LongTerm)
            {
                wbLong.ScriptErrorsSuppressed = true;
                wbLong.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(wbLong_DocumentCompleted);
                wbLong.Url = new Uri(LONG_TERM_URL);
                wbLong.Refresh();
            }
            else
            {
                wbShort.ScriptErrorsSuppressed = true;
                wbShort.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(wbShort_DocumentCompleted);
                wbShort.Url = new Uri(SHORT_TERM_URL);
                wbShort.Refresh();                    
            }
                         
        }

        public override void Start()
        {
            Symbol = "USDJPY";
            refresh();

            lock (this)
            {

                foreach (Signal signal in toClose)
                {
                    close(signal);
                }
                toClose.Clear();
                foreach (Signal signal in toOpen)
                {
                    open(signal);
                }
                toOpen.Clear();
                foreach (Signal signal in toUpdate)
                {
                    update(signal);
                }
                toUpdate.Clear();
            }
        }

        void wbLong_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            lock (this)
            {               
                var doc = wbLong.Document;
                readHtml(doc);
            }
        }

        void wbShort_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            lock (this)
            {
                var doc = wbShort.Document;
                readHtml(doc);
            }
        }

        private void addNewSignal(Signal signal)
        {           
            Signal toRemove = null;
            foreach (Signal tmp in newSignals)
            {
                if (isSameSignal(signal, tmp))
                {
                    toRemove = tmp;
                    break;
                }
            }
            if (toRemove != null)
            {
                newSignals.Remove(toRemove);               
            }
            newSignals.Add(signal);
        }

        private void readHtml(HtmlDocument doc)
        {
            newSignals = new List<Signal>();
            toOpen = new List<Signal>();
            toClose = new List<Signal>();
            toUpdate = new List<Signal>();

            HtmlElement table = LongTerm ? doc.GetElementById(LONG_TERM_ELEMENT) : doc.GetElementById(SHORT_TERM_ELEMENT);
            HtmlElementCollection rows = table.GetElementsByTagName("tr");
            for (int i = 1; i < rows.Count; i++)
            {
                Signal signal = new Signal();
                HtmlElementCollection cells = rows[i].GetElementsByTagName("td");
                signal.Symbol = getText(cells[0].InnerText.Replace("/", ""));
                signal.Action = getText(cells[1].InnerText);
                signal.EntryPoint = Conversion.getDouble(cells[2].InnerText);
                signal.StopLoss = Conversion.getDouble(cells[3].InnerText);
                signal.TakeProfit = Conversion.getDouble(cells[4].InnerText);
                signal.Status = getText(cells[5].InnerText);
                addNewSignal(signal);
            }
            
            List<Signal> savedSignals = new List<Signal>();
            IEnumerator<Signal> enumerator = signals.Values.GetEnumerator();
            while (enumerator.MoveNext())
            {
                savedSignals.Add(enumerator.Current);
            }
            foreach (Signal savedSignal in savedSignals)
            {
                foreach (Signal newSignal in newSignals)
                {
                    if (isSameSignal(newSignal, savedSignal))
                    {
                        if (isClosed(newSignal))
                        {
                            toClose.Add(newSignal);
                        }

                        if (isChanged(newSignal, savedSignal))
                        {
                            toUpdate.Add(newSignal);
                        }
                    }
                }
            }
            foreach (Signal newSignal in newSignals)
            {
                Boolean found = false;
                foreach (Signal savedSignal in savedSignals)
                {
                    if (isSameSignal(newSignal, savedSignal))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    if (isOpened(newSignal))
                    {
                        toOpen.Add(newSignal);
                    }
                }
            }
        }
       

        private int getTicket(Signal signal)
        {
             IEnumerator<int> enumerator = signals.Keys.GetEnumerator();
             while (enumerator.MoveNext())
             {
                 int key = enumerator.Current;
                 Signal value;
                 signals.TryGetValue(key, out value);
                 if (isSameSignal(signal, value))
                 {
                     int ticket = key;
                     return ticket;
                 }
             }
             return -1;
        }

        private Order createOrder(Signal signal)
        {
            Order order = new Order();
            order.Lots = 0.1;
            if (signal.Action.Equals("VENDI"))
            {
                order.Type = TradeOperation.SELL;
                order.OpenPrice = Bid;
            }
            else if (signal.Action.Equals("COMPRA"))
            {
                order.Type = TradeOperation.BUY;
                order.OpenPrice = Ask;
            }

            order.Symbol = signal.Symbol;
            order.Comment = "By Signal";
            order.TakeProfit = signal.TakeProfit;
            order.StopLoss = signal.StopLoss;
         
            return order;
        }

        public void open(Signal signal)
        {
            Order order = createOrder(signal);
            order.StopLoss = 0;
            order.TakeProfit = 0;
            int ticket = tradingService.OpenOrder(order);
            if (ticket > 0)
            {
                signals.Add(ticket, signal);
                update(signal);
            }
       }

        public void update(Signal signal)
        {
            int ticket = getTicket(signal);
            if (ticket > 0)
            {
                Order order = createOrder(signal);
                order.Ticket = ticket;
                tradingService.EditOrder(order);
                signals.Add(ticket, signal);
            }
        }

        public void close(Signal signal)
        {
            int ticket = getTicket(signal);
            Order order = tradingService.LoadOrder(ticket);
            doClose(order, order.Lots);
            signals.Remove(ticket);
        }

        private Boolean isOpened(Signal signal)
        {
            if (signal.Status.Equals("Attivo"))
            {
                return true;
            }
            return false;
        }

        private Boolean isSameSignal(Signal s1, Signal s2)
        {
            if (s1.EntryPoint == s2.EntryPoint && s1.Symbol.Equals(s2.Symbol))
            {
                return true;
            }
            return false;
        }

        private Boolean isChanged(Signal s1, Signal s2)
        {
            if (s1.StopLoss != s2.StopLoss)
            {
                return true;
            }
            if (s2.TakeProfit != s2.TakeProfit)
            {
                return true;
            }
            return false;
        }

        private Boolean isClosed(Signal signal){
            if (signal.Status.Equals("Chiuso"))
            {
                return true;
            }
            return false;
        }


        private String getText(String source)
        {
            source = source.Replace("\r\n", "");
            source = source.Replace(" ", "");
            source = source.Replace("\"", "");
            return source;
        }
    }
     
   

    /*
    <table id="gvSignalsLong" >
			<tr >
				<th >Coppia</th><th>Azione</th><th>Punto  d&#39;ingresso</th><th>Stop Loss</th><th>Take Profit</th><th>Status</th><th>Commenti</th><th>&nbsp;</th><th>&nbsp;</th>
			</tr><tr>
				<td>USD/JPY</td><td><span>VENDI <span/></td><td></td><td>&nbsp;</td><td>&nbsp;</td><td>Prepararsi</td><td><span>&nbsp;<span/></td><td>&nbsp;</td><td></td>
			</tr>
            <tr>
				<td>GBP/USD</td><td><span>COMPRA <span/></td><td></td><td>&nbsp;</td><td>&nbsp;</td><td>Prepararsi</td><td><span>&nbsp;<span/></td><td>&nbsp;</td><td></td>
			</tr>
		</table>
     */
}
