﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Service;
using Signals;
using PipsterEA.Model;
using PipsterEA.Core;
using Signals.OpenSignals;
using Signals.OpenHandlers;
using Signals.CloseSignals;
using Signals.StandbyHandlers;

namespace PipsterEA.Experts
{
    public class LongTermTrendingPipster : Pipster
    {
        //private static String symbol = "GBPUSD";
        private static String symbol = "EURUSD";
        private static TimeFrame timeFrame = TimeFrame.PERIOD_D1;
        private static int waitHour = 12;

        private WaitAfterALoss waitAfterALoss = new WaitAfterALoss(60 * waitHour);


        private MarketStatus lastStatus;


        public LongTermTrendingPipster()
            : base(symbol)
        {
            RegisterStandbyHandler(new MaxOrdersStandby(1));
            
            RegisterOpenHandler(new AccountPctLots(40));
            RegisterStandbyHandler(waitAfterALoss);

            waitAfterALoss.Minutes = 60 * 8;

            RegisterOpenHandler(new SimpleStopLoss(400));
            RegisterOpenHandler(new SimpleTakeProfit(400));
            RegisterOpenHandler(new TightStopLoss(TimeFrame.PERIOD_D1, 3, 25));

            RegisterOpenSignal(new MaxMinBreakout(TimeFrame.PERIOD_D1, 1,0));
            RegisterOpenSignal(new MaUpDown(TimeFrame.PERIOD_D1, 26, MovingAverageType.SIMPLE, 2,MovingAverageType.EXPONENTIAL));
            RegisterOpenSignal(new GatorOpen(TimeFrame.PERIOD_D1));
            RegisterCloseSignal(new GatorClose(TimeFrame.PERIOD_D1));
            RegisterCloseSignal(new MaCrossingClose(TimeFrame.PERIOD_D1, 26, 2, 100));
            RegisterCloseSignal(new TrailingStop(200, -10));

        }

      
        
      



    }
}
