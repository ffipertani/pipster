﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using PipsterEA.Model;
using Signals.OpenHandlers;
using Signals.OpenSignals;
using Signals.CloseSignals;

namespace Experts.Experts
{
    public class TunnelPipster:Pipster
    {
        static int RETRY_AFTER_PIPS = 4;
        Dictionary<Order,Double> retryOrders = new Dictionary<Order,Double>();
        //TunnelOpen tunnelOpen;
        BBOpen tunnelOpen;
        
        public TunnelPipster()
            : base("EURUSD")
        {
            Description = "Tunnel Pipster";
           // tunnelOpen = new TunnelOpen(TimeFrame.PERIOD_M5, 53, 10, MovingAverageType.LINEAR_WEIGHTED, AppliedPrice.WEIGHTED);
            tunnelOpen = new BBOpen(TimeFrame.PERIOD_M5, 24, AppliedPrice.WEIGHTED);
            RegisterOpenSignal(tunnelOpen);

            RegisterOpenHandler(new SimpleLot(0.1));
            RegisterOpenSignal(new AllowDifferentOpen());
            //RegisterOpenSignal(new MaxMinBreakout( TimeFrame.PERIOD_M5,24,2));
            //RegisterCloseSignal(new TrailingStop(4, 1));
            RegisterCloseSignal(new TrailingStop(10, 8));
            RegisterCloseSignal(new TrailingStop(20, 16));
            RegisterCloseSignal(new TrailingStop(30, 24));
            
           // RegisterCloseSignal(new TrailingStop(30, 20));
           // RegisterCloseSignal(new TrailingStop(40, 37));
            //RegisterOpenHandler(new TightStopLoss(TimeFrame.PERIOD_M5, 2, 2));
            OrderClosed += new OrderClosedEvent(TunnelPipster_OrderClosed);
        }

        public override void Start()
        {             
            tunnelOpen.RefreshData();
            if ( tunnelOpen.IsInside())
            {
                retryOrders.Clear();
            }

            base.Start();
        }

        void TunnelPipster_OrderClosed(Order order)
        {
             

            if (CountPips(order) <= 5)
            {
                if (!tunnelOpen.IsInside())
                {
                    if (order.Type == TradeOperation.BUY)
                    {
                        retryOrders.Add(order, order.OpenPrice + (RETRY_AFTER_PIPS * Point));
                    }
                    else
                    {
                        retryOrders.Add(order, order.OpenPrice - (RETRY_AFTER_PIPS * Point));
                    }
                    
                }
            }
        }

        private void tryOpenWaiting()
        {
            foreach (Order order in retryOrders.Keys)
            {
                Double price;
                retryOrders.TryGetValue(order,out price);

                if (order.Type == TradeOperation.BUY)
                {
                    if (CanBuy())
                    {
                        if (Ask > price)
                        {
                            buy();
                        }
                        else
                        {
                            if (Ask < order.OpenPrice)
                            {
                                Double diff = order.OpenPrice - Ask;
                                if (diff > (RETRY_AFTER_PIPS * Point))
                                {
                                    diff = (RETRY_AFTER_PIPS * Point);
                                }
                                Double newPrice = order.OpenPrice + (RETRY_AFTER_PIPS * Point) - diff;
                               
                                if (newPrice < price)
                                {
                                    retryOrders.Remove(order);
                                    retryOrders.Add(order, newPrice);
                                }
                            }

                        }
                    }
                }
               
                if (order.Type == TradeOperation.SELL)
                {
                    if (CanSell())
                    {
                        if (Bid < price)
                        {
                            sell();
                        }
                        else
                        {
                            if (Bid > order.OpenPrice)
                            {
                                Double diff = Bid - order.OpenPrice;
                                if (diff > (RETRY_AFTER_PIPS * Point))
                                {
                                    diff = (RETRY_AFTER_PIPS * Point);
                                }
                                Double newPrice = order.OpenPrice - (RETRY_AFTER_PIPS * Point) + diff;
                                if (newPrice > price)
                                {
                                    retryOrders.Remove(order);
                                    retryOrders.Add(order,newPrice);
                                }
                            }

                        }
                    }
                }
            }
        }
      

        protected override void open()
        {
            try
            {
                BeforeOpen();

                if (CanBuy() && tunnelOpen.CanBuy())
                {                   
                    buy();                        
                }

                if (CanSell() && tunnelOpen.CanSell())
                {                    
                     sell();                        
                }
              
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            tryOpenWaiting();
        }

        protected override void close()
        {
            List<Order> tmpOrders = new List<Order>(ManagedOrders);
            foreach (Order order in tmpOrders)
            {
                if (order.IsClosed())
                {
                    continue;
                }

                if (order.Type == TradeOperation.BUY)
                {
                    if (tunnelOpen.IsInsideDown() || tunnelOpen.isBelow())
                    {
                        doClose(order, order.Lots);
                    }
                }

                if (order.Type == TradeOperation.SELL)
                {
                    if (tunnelOpen.IsInsideUp() || tunnelOpen.IsAbove())
                    {
                        doClose(order, order.Lots);
                    }
                } 
            }
            base.close();
        }

         

        private Double calcSignal(int shift)
        {
            return indicatorService.MovingAverage(Symbol, TimeFrame.PERIOD_M5, 5, 0, MovingAverageType.LINEAR_WEIGHTED, AppliedPrice.WEIGHTED, shift);
        }

        private Double calcSlowMa(int shift)
        {
            return indicatorService.MovingAverage(Symbol, TimeFrame.PERIOD_M5, 233, 0, MovingAverageType.LINEAR_WEIGHTED, AppliedPrice.WEIGHTED, shift);
        }

        private Double calcFastMa(int shift)
        {
            return indicatorService.MovingAverage(Symbol, TimeFrame.PERIOD_M5, 144, 0, MovingAverageType.LINEAR_WEIGHTED, AppliedPrice.WEIGHTED, shift);
        }

    }
}
