﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using PipsterEA.Core;
using System.Reflection;
using System.IO;
using PipsterEA.Utils;

namespace PipsterEA.Conf
{
   
    public class Configuration
    {
       
        public List<Library> Libraries = new List<Library>();

        public static List<BaseStrategy> OpenSignals = new List<BaseStrategy>();
        public static List<BaseStrategy> CloseSignals = new List<BaseStrategy>();
        public static List<BaseStrategy> OpenHandlers = new List<BaseStrategy>();
        public static List<BaseStrategy> StandbyHandlers = new List<BaseStrategy>();

        public static Type FindType(String name)
        {
            foreach (BaseStrategy bs in OpenSignals)
            {
                if (bs.GetType().FullName.Equals(name))
                {
                    return bs.GetType();
                }
            }
            foreach (BaseStrategy bs in OpenHandlers)
            {
                if (bs.GetType().FullName.Equals(name))
                {
                    return bs.GetType();
                }
            }
            foreach (BaseStrategy bs in CloseSignals)
            {
                if (bs.GetType().FullName.Equals(name))
                {
                    return bs.GetType();
                }
            }
            foreach (BaseStrategy bs in StandbyHandlers)
            {
                if (bs.GetType().FullName.Equals(name))
                {
                    return bs.GetType();
                }
            }
            return null;
        }
       
       
        public static void Read()
        {
            
    
            String filename = "conf.xml";
            /*
            XmlSerializer deserializer = new XmlSerializer(typeof(Configuration));
            XmlWriter writer = XmlWriter.Create(filename);
            Configuration configuration = new Configuration();
           Library l = new Library();
           l.Name="prova.dll";
           l.Plugins.Add(new Plugin(){ClassName="Pipster.prova", Description="Prova123", Name="Pipster"});
           l.Plugins.Add(new Plugin() { ClassName = "Pipster8.prova8", Description = "Prova128883", Name = "Pipster888" });
           configuration.Libraries.Add(l);
           deserializer.Serialize(writer, configuration);

            writer.Close();
            */
            XmlSerializer deserializer = new XmlSerializer(typeof(Configuration));
            XmlReader textReader = XmlReader.Create(filename);


            Configuration configuration = (Configuration)deserializer.Deserialize(textReader); 
                      
            textReader.Close();


           

            foreach (Library library in configuration.Libraries)
            {
                if (library.FullScan)
                {
                    List<Object> objs = Reflection.getAllInstance(library.Name);
                    foreach (Object o in objs)
                    {
                        addObject(o);
                    }
                }
               
                foreach (Plugin plugin in library.Plugins)
                {
                    Object signal = Reflection.getInstance(library.Name, plugin.ClassName);
                   
                    addObject(prepareSignal(plugin,signal));
                }
                 
            }
           

            
        }

        private static void addObject(Object signal)
        {
            if (signal == null)
            {
                return;
            }

            if(signal.GetType().IsSubclassOf(typeof(BaseStrategy)))
            {
                ((BaseStrategy)signal).Name = signal.GetType().Name;
            }

            if (signal.GetType().IsSubclassOf(typeof(OpenSignal)))
            {
                addType(OpenSignals, (Object)signal);
               // OpenSignals.Add((OpenSignal)signal);
            }
            else if (signal.GetType().IsSubclassOf(typeof(CloseSignal)))
            {
                addType(CloseSignals, (Object)signal);
              //  CloseSignals.Add((CloseSignal)signal);
            }
            else if (signal.GetType().IsSubclassOf(typeof(OpenHandler)))
            {
                addType(OpenHandlers, (Object)signal);
               // OpenHandlers.Add((OpenHandler)signal);
            }
            else if (signal.GetType().IsSubclassOf(typeof(StandbyHandler)))
            {
                addType(StandbyHandlers, (Object)signal);
               // StandbyHandlers.Add((StandbyHandler)signal);
            }
        }

        private static void addType(List<BaseStrategy> list,Object o)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].GetType() == o.GetType())
                {
                    list.Remove(list[i]);
                }
            }
            list.Add((BaseStrategy)o);

        }

        private static Object prepareSignal(Plugin plugin, Object o)
        {
            if (!o.GetType().IsSubclassOf(typeof(BaseStrategy)))
            {
                return null;
            }
            BaseStrategy strategy = (BaseStrategy)o;
            if (plugin.Description != null)
            {
                strategy.Description = plugin.Description;
            }

            if (plugin.Name != null)
            {
                strategy.Name = plugin.Name;
            }           
            return strategy;
        }
    }
}
