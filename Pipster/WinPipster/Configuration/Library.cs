﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace PipsterEA.Conf
{
     
    public class Library
    {
        [XmlAttribute("name")]
        public String Name { get; set; }
        [XmlAttribute("fullScan")]
        public Boolean FullScan { get; set; }
       
        public List<Plugin> Plugins = new List<Plugin>();
    }
}
