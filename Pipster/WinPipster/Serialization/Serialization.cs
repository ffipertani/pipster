﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;



namespace PipsterEA.Utils.Serialization
{
    public class Serialization
    {
        /*
        public static void Serialize(Pipster pipster, String filename)
        {
            try
            {
                if (File.Exists(filename))
                {
                    File.Delete(filename);
                }
                FileStream fs = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                SerializationPipster template = createTemplate(pipster);
                XmlSerializer serializer = new XmlSerializer(template.GetType()); ;
                serializer.Serialize(fs, template);
                fs.Close();
            }
            catch (Exception e)
            {
                String output = e.ToString();
                Console.WriteLine(output);
            }
        }
        */
        
        public static void Serialize(Pipster pipster, String filename)
        {           
            SerializationPipster template = createTemplate(pipster);
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
            FileStream fs = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);           
            String ser = JSONSerializer.SerializeObject(template);

            Byte[] buffer = System.Text.ASCIIEncoding.ASCII.GetBytes(ser);
            fs.Write(buffer, 0, buffer.Length);
            fs.Close();
        }
        
        public static void SerializeSystem(List<Pipster> pipsters, String filename)
        {
           
            List<SerializationPipster> templates = new List<SerializationPipster>();
            foreach (Pipster pipster in pipsters)
            {
                templates.Add(createTemplate(pipster));
            }
            String ser = JSONSerializer.SerializeObject(templates);
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
            FileStream fs = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            Byte[] buffer = System.Text.ASCIIEncoding.ASCII.GetBytes(ser);
            fs.Write(buffer, 0, buffer.Length);
            fs.Close();
        }

        private static SerializationPipster createTemplate(Pipster pipster)
        {
            SerializationPipster template = new SerializationPipster(pipster);
            foreach (OpenSignal os in pipster.OpenSignals)
            {
                template.OpenSignals.Add(createTemplate(os));
            }
            foreach (CloseSignal os in pipster.CloseSignals)
            {
                template.CloseSignals.Add(createTemplate(os));
            }

            foreach (OpenHandler os in pipster.OpenHandlers)
            {
                template.OpenHandlers.Add(createTemplate(os));
            }

            foreach (StandbyHandler os in pipster.StandbyHandlers)
            {
                template.StandbyHandlers.Add(createTemplate(os));
            }
            return template;
        }

        public static List<Pipster> DeserializeSystem(String filename)
        {
            List<Pipster> pipsters = new List<Pipster>();
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[fs.Length];
            fs.Read(buffer, 0, buffer.Length);
            String json = System.Text.ASCIIEncoding.ASCII.GetString(buffer);

            List<SerializationPipster> models = (List<SerializationPipster>)JSONSerializer.DeserializeObject(json, typeof(List<SerializationPipster>));
            foreach (SerializationPipster model in models)
            {
                pipsters.Add(createPipster(model));
            }
            return pipsters;
        }

        private static Pipster createPipster(SerializationPipster model)
        {
            Type pipsterType = typeof(Pipster);
            Pipster pipster = (Pipster)Activator.CreateInstance(pipsterType);

            pipster.ClearAllSignals();
            foreach (Dictionary<String, Object> os in model.OpenSignals)
            {
                pipster.RegisterOpenSignal((OpenSignal)clone(os));
            }

            foreach (Dictionary<String, Object> os in model.CloseSignals)
            {
                pipster.RegisterCloseSignal((CloseSignal)clone(os));
            }

            foreach (Dictionary<String, Object> os in model.OpenHandlers)
            {
                pipster.RegisterOpenHandler((OpenHandler)clone(os));
            }

            foreach (Dictionary<String, Object> os in model.StandbyHandlers)
            {
                pipster.RegisterStandbyHandler((StandbyHandler)clone(os));
            }

            pipster.Symbol = model.Symbol;
            pipster.Description = model.Description;
            pipster.Enabled = model.Enabled;
            return pipster;
        }

        public static Pipster Deserialize(String filename)
        {
            
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[fs.Length];
            fs.Read(buffer,0,buffer.Length);
            String json = System.Text.ASCIIEncoding.ASCII.GetString(buffer);


            SerializationPipster model = (SerializationPipster)JSONSerializer.DeserializeObject(json, typeof(SerializationPipster));

            Pipster pipster = createPipster(model);
            return pipster;
        }

       
        private static Dictionary<String, Object> createTemplate(BaseStrategy bs)
        {
            Dictionary<String, Object> dic = new Dictionary<string, object>();
            Type type = bs.GetType();
            foreach (PropertyInfo pi in type.GetProperties())
            {
                if(pi.CanRead){
                    dic.Add(pi.Name, pi.GetValue(bs, null));
                }
            }
            return dic;
        }
        
        private static BaseStrategy clone(Dictionary<String, Object> source)
        {
            object osType;
            source.TryGetValue("Type", out osType);

            Type t = Conf.Configuration.FindType((String)osType);
            BaseStrategy dest = (BaseStrategy)Activator.CreateInstance(t);

            Type sourceType = source.GetType();
            Type destType = dest.GetType();

            foreach (String key in source.Keys)
            {
                PropertyInfo destProperty = null;
                foreach (PropertyInfo pid in destType.GetProperties())
                {
                    if (pid.Name == key && pid.CanWrite)
                    {
                        destProperty = pid;
                        break;
                    }
                }

                if (destProperty != null)
                {
                    object val;
                    source.TryGetValue(key,out val);
                    if (val != null && val.GetType() == typeof(Int64))
                    {
                        val = Convert.ToInt32(val);
                    }
                    destProperty.SetValue(dest,val , null);
                }
            }

            return dest;
        }
    }
}
