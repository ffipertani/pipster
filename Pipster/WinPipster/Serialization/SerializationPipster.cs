﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Core;

namespace PipsterEA.Utils.Serialization
{
    public class SerializationPipster
    {
        public String Description { get; set; }
        public String Type { get; set; }
        public String Symbol { get; set; }
        public Boolean Enabled { get; set; }
        public List<Dictionary<String,Object>> OpenSignals = new List<Dictionary<string,object>>();
        public List<Dictionary<String, Object>> CloseSignals = new List<Dictionary<string,object>>();
        public List<Dictionary<String, Object>> OpenHandlers = new List<Dictionary<string,object>>();
        public List<Dictionary<String, Object>> StandbyHandlers = new List<Dictionary<string,object>>();

        public SerializationPipster() { }

        public SerializationPipster(Pipster pipster)
        {
            Type = pipster.GetType().ToString();
            Symbol = pipster.Symbol;
            Description = pipster.Description;
            Enabled = pipster.Enabled;
        }
    }
}
