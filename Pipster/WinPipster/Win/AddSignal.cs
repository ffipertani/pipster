﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using PipsterEA.Core;

namespace PipsterEA.Win
{
    public partial class AddSignal : Form
    {
        public delegate void StrategyEvent(BaseStrategy strategy);
        public event StrategyEvent StrategyAdded;

        private List<BaseStrategy> signals;
        public AddSignal(List<BaseStrategy> signals)
        {
            InitializeComponent();
            this.signals = signals;

            foreach (BaseStrategy bs in signals)
            {
                listView1.Items.Add(bs.Name);
            }
        }

        public void setSelectedStrategy(BaseStrategy bs)
        {
            int index = 0;
            foreach (BaseStrategy abs in signals)
            {               
                if(abs.GetType().Name.Equals(bs.GetType().Name))
                {
                    //listView1.SelectedIndices.Add(index);
                }
                index++;
            }
            propertyGrid1.SelectedObject = bs;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                int index = listView1.SelectedIndices[0];
                BaseStrategy bs = signals[index];
                BaseStrategy newbs  = (BaseStrategy)Activator.CreateInstance(bs.GetType());
                newbs.Name = bs.Name;
                newbs.Description = bs.Description;
                propertyGrid1.SelectedObject = newbs;
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (StrategyAdded != null)
            {
                StrategyAdded((BaseStrategy)propertyGrid1.SelectedObject);
            }
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }        
    }
}
