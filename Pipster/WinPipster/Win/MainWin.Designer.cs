﻿namespace PipsterEA.Win
{
    partial class MainWin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWin));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeaderSymbol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderDescription = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonCreatePipster = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonImportPipster = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDeletePipster = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonEditPipster = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonExport = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSaveSystem = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonLoadSystem = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonStartConnection = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusClient = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(0, 28);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.listView1);
            this.splitContainer1.Size = new System.Drawing.Size(874, 326);
            this.splitContainer1.SplitterDistance = 291;
            this.splitContainer1.TabIndex = 4;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderSymbol,
            this.columnHeaderDescription});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.FullRowSelect = true;
            this.listView1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(291, 326);
            this.listView1.TabIndex = 3;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // columnHeaderSymbol
            // 
            this.columnHeaderSymbol.Text = "Simbolo";
            // 
            // columnHeaderDescription
            // 
            this.columnHeaderDescription.Text = "Descrizione";
            this.columnHeaderDescription.Width = 303;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonCreatePipster,
            this.toolStripButtonImportPipster,
            this.toolStripButtonDeletePipster,
            this.toolStripButtonEditPipster,
            this.toolStripButtonExport,
            this.toolStripButtonSaveSystem,
            this.toolStripButtonLoadSystem,
            this.toolStripButtonStartConnection});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(874, 25);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonCreatePipster
            // 
            this.toolStripButtonCreatePipster.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCreatePipster.Image")));
            this.toolStripButtonCreatePipster.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCreatePipster.Name = "toolStripButtonCreatePipster";
            this.toolStripButtonCreatePipster.Size = new System.Drawing.Size(68, 22);
            this.toolStripButtonCreatePipster.Text = "Crea EA";
            this.toolStripButtonCreatePipster.Click += new System.EventHandler(this.toolStripButtonCreatePipster_Click);
            // 
            // toolStripButtonImportPipster
            // 
            this.toolStripButtonImportPipster.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonImportPipster.Image")));
            this.toolStripButtonImportPipster.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonImportPipster.Name = "toolStripButtonImportPipster";
            this.toolStripButtonImportPipster.Size = new System.Drawing.Size(86, 22);
            this.toolStripButtonImportPipster.Text = "Importa EA";
            this.toolStripButtonImportPipster.Click += new System.EventHandler(this.toolStripButtonImportPipster_Click);
            // 
            // toolStripButtonDeletePipster
            // 
            this.toolStripButtonDeletePipster.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDeletePipster.Image")));
            this.toolStripButtonDeletePipster.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDeletePipster.Name = "toolStripButtonDeletePipster";
            this.toolStripButtonDeletePipster.Size = new System.Drawing.Size(66, 22);
            this.toolStripButtonDeletePipster.Text = "Elimina";
            this.toolStripButtonDeletePipster.Click += new System.EventHandler(this.toolStripButtonDeletePipster_Click);
            // 
            // toolStripButtonEditPipster
            // 
            this.toolStripButtonEditPipster.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonEditPipster.Image")));
            this.toolStripButtonEditPipster.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonEditPipster.Name = "toolStripButtonEditPipster";
            this.toolStripButtonEditPipster.Size = new System.Drawing.Size(91, 22);
            this.toolStripButtonEditPipster.Text = "Modifica EA";
            this.toolStripButtonEditPipster.Click += new System.EventHandler(this.toolStripButtonEditPipster_Click);
            // 
            // toolStripButtonExport
            // 
            this.toolStripButtonExport.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonExport.Image")));
            this.toolStripButtonExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonExport.Name = "toolStripButtonExport";
            this.toolStripButtonExport.Size = new System.Drawing.Size(83, 22);
            this.toolStripButtonExport.Text = "Esporta EA";
            this.toolStripButtonExport.Click += new System.EventHandler(this.toolStripButtonExport_Click);
            // 
            // toolStripButtonSaveSystem
            // 
            this.toolStripButtonSaveSystem.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSaveSystem.Image")));
            this.toolStripButtonSaveSystem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSaveSystem.Name = "toolStripButtonSaveSystem";
            this.toolStripButtonSaveSystem.Size = new System.Drawing.Size(113, 22);
            this.toolStripButtonSaveSystem.Text = "Salva workspace";
            this.toolStripButtonSaveSystem.Click += new System.EventHandler(this.toolStripButtonSaveSystem_Click);
            // 
            // toolStripButtonLoadSystem
            // 
            this.toolStripButtonLoadSystem.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonLoadSystem.Image")));
            this.toolStripButtonLoadSystem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLoadSystem.Name = "toolStripButtonLoadSystem";
            this.toolStripButtonLoadSystem.Size = new System.Drawing.Size(119, 22);
            this.toolStripButtonLoadSystem.Text = "Carica workspace";
            this.toolStripButtonLoadSystem.Click += new System.EventHandler(this.toolStripButtonLoadSystem_Click);
            // 
            // toolStripButtonStartConnection
            // 
            this.toolStripButtonStartConnection.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonStartConnection.Image")));
            this.toolStripButtonStartConnection.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonStartConnection.Name = "toolStripButtonStartConnection";
            this.toolStripButtonStartConnection.Size = new System.Drawing.Size(98, 22);
            this.toolStripButtonStartConnection.Text = "Spegni Server";
            this.toolStripButtonStartConnection.Click += new System.EventHandler(this.toolStripButtonMaintainConnection_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusClient});
            this.statusStrip1.Location = new System.Drawing.Point(0, 357);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(874, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusClient
            // 
            this.toolStripStatusClient.Name = "toolStripStatusClient";
            this.toolStripStatusClient.Size = new System.Drawing.Size(97, 17);
            this.toolStripStatusClient.Text = "Client connected";
            // 
            // MainWin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 379);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "MainWin";
            this.Text = "MainWin";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWin_FormClosing);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonCreatePipster;
        private System.Windows.Forms.ToolStripButton toolStripButtonImportPipster;
        private System.Windows.Forms.ToolStripButton toolStripButtonDeletePipster;
        private System.Windows.Forms.ToolStripButton toolStripButtonEditPipster;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeaderDescription;
        private System.Windows.Forms.ColumnHeader columnHeaderSymbol;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusClient;
        private System.Windows.Forms.ToolStripButton toolStripButtonExport;
        private System.Windows.Forms.ToolStripButton toolStripButtonSaveSystem;
        private System.Windows.Forms.ToolStripButton toolStripButtonLoadSystem;
        private System.Windows.Forms.ToolStripButton toolStripButtonStartConnection;

    }
}