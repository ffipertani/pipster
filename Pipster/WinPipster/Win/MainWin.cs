﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using PipsterEA.Utils.Serialization;
using PipsterEA.Core;
using PipsterEA.Win.View;
using PipsterEA.Service;
using Experts.Experts;
using Signals.CloseSignals;

namespace PipsterEA.Win
{
    public partial class MainWin : Form
    {
        NetworkService networkService = NetworkService.GetInstance();
        List<Pipster> pipsters = new List<Pipster>();
        MainView mainView = new MainView();
        UserControl currentView;
        Boolean maintainConnection = true;

        public MainWin()
        {
            Conf.Configuration.Read();
            InitializeComponent();
            addView(mainView);
            networkService.ClientConnected += new NetworkService.ClientEvent(networkService_ClientConnected);
            networkService.ClientError += new NetworkService.ClientEvent(networkService_ClientError);
            networkService.Tick += new NetworkService.TickHandler(networkService_Tick);
            networkService.Listen();
            toolStripStatusClient.Text = "Wating for client";

           // PipsterScalper apipster = new PipsterScalper();
           // PipsterFreeWebSignal apipster = new PipsterFreeWebSignal();
            //PipsterDiaz apipster = new PipsterDiaz();
            TunnelPipster apipster = new TunnelPipster();
            TunnelPipster apipster2 = new TunnelPipster();
            apipster2.Description += " Fixed";
            apipster2.RegisterCloseSignal(new FixedTakeProfitSignal(45));
            TunnelPipster apipster3 = new TunnelPipster();
            apipster3.RegisterCloseSignal(new BBClose(Model.TimeFrame.PERIOD_M5, 24, Model.AppliedPrice.WEIGHTED, 5));
         //   PipsterDay apipster = new PipsterDay();
          //  PipsterWebSignal apipster = new PipsterWebSignal();
          //  PipsterTheDream apipster = new PipsterTheDream();
            createPipster(apipster);
            createPipster(apipster2);
            createPipster(apipster3);
            return;

            List<Pipster> apipsters = Serialization.DeserializeSystem("C:\\Users\\Francesco\\Documents\\Visual Studio 2010\\Projects\\Pipster\\WinPipster\\bin\\Debug\\VegasDaily.json");           
            foreach (Pipster pipster in apipsters)
            {
                createPipster(pipster);
            }
        }

        void networkService_Tick()
        {
            foreach (Pipster pipster in pipsters)
            {
                pipster.Start();
            }
            if (maintainConnection)
            {
                networkService.NextTick();
            }
        }

        void networkService_ClientError(System.Net.Sockets.TcpClient client)
        {
            try
            {
                this.Invoke((MethodInvoker)delegate
                {
                    toolStripStatusClient.Text = "Client error";
                });
            }
            catch (Exception e)
            {
            }
        }

        void networkService_ClientConnected(System.Net.Sockets.TcpClient client)
        {
            this.Invoke((MethodInvoker)delegate
            {
                toolStripStatusClient.Text = "Client connected";
            });
        }

       

        private void refreshItems()
        {
            listView1.Items.Clear();
            foreach (Pipster pipster in pipsters)
            {
                String[] items = new String[2];
                items[0] = pipster.Symbol;
                items[1] = pipster.Description;
                listView1.Items.Add(new ListViewItem(items));
            }
        }

        private void createPipster(Pipster pipster)
        {
            pipster.RegisterAll();
            pipsters.Add(pipster);
            refreshItems();    
            selectPipster(pipster);
        }

        private void selectPipster(Pipster pipster)
        {
            int index = 0;
            foreach (Pipster apipster in pipsters)
            {
                if (apipster == pipster)
                {
                    break;
                }
                index++;
            }
            listView1.SelectedIndices.Add(index);
            addView(new DetailPipster(pipster));
        }

      

        private void addView(UserControl ep)
        {
            if (currentView != null)
            {
                splitContainer1.Panel2.Controls.Remove(currentView);
            }
            splitContainer1.Panel2.Controls.Add(ep);
            ep.Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom;
            ep.Location = new Point(0, 0);
            ep.Width = splitContainer1.Panel2.Width;
            ep.Height = splitContainer1.Panel2.Height;
            currentView = ep;
        }

        private void toolStripButtonCreatePipster_Click(object sender, EventArgs e)
        {
            EditPipster ep = new EditPipster(new Pipster());
            ep.PipsterCreated += new EditPipster.PipsterEvent(ep_PipsterCreated);
            ep.PipsterCancelled += new EditPipster.PipsterEvent(ep_PipsterCancelled);
            addView(ep);
        }

        void ep_PipsterCancelled(Pipster sender)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                selectPipster(pipsters[listView1.SelectedIndices[0]]);
            }
            else
            {
                addView(mainView);
            }
        }

        void ep_PipsterCreated(Pipster pipster)
        {
            createPipster(pipster);
        }

        void ep_PipsterEdited(Pipster pipster)
        {
            int index = listView1.SelectedIndices[0];
            pipsters.RemoveAt(index);
            pipster.RegisterAll();
            pipsters.Insert(index,pipster);
            refreshItems();
            selectPipster(pipster);
        }
       
       

        private void toolStripButtonImportPipster_Click(object sender, EventArgs e)
        {
            OpenFileDialog of = new OpenFileDialog();
            DialogResult result = of.ShowDialog(this);
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                String filename = of.FileName;
                Pipster pipster = Serialization.Deserialize(filename);
                createPipster(pipster);
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(listView1.SelectedIndices.Count>0)
            {
                Pipster pipster = pipsters[listView1.SelectedIndices[0]];
                selectPipster(pipster);
            }else
            {
                addView(mainView);
            }
            
        }

        private void toolStripButtonExport_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                SaveFileDialog sf = new SaveFileDialog();
                DialogResult dr = sf.ShowDialog();
                if (dr == System.Windows.Forms.DialogResult.OK)
                {
                    Pipster pipster = pipsters[listView1.SelectedIndices[0]];
                    String filename = sf.FileName;
                    Serialization.Serialize(pipster, filename);
                }
            }
        }

        private void toolStripButtonDeletePipster_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                pipsters.RemoveAt(listView1.SelectedIndices[0]);
                refreshItems();
            }
        }

        private void toolStripButtonEditPipster_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                Pipster pipster = pipsters[listView1.SelectedIndices[0]];
                EditPipster ep = new EditPipster(pipster.Clone());               
                ep.PipsterCancelled += new EditPipster.PipsterEvent(ep_PipsterCancelled);
                ep.PipsterCreated +=new EditPipster.PipsterEvent(ep_PipsterEdited);
                addView(ep);
            }
           
        }

        private void toolStripButtonSaveSystem_Click(object sender, EventArgs e)
        {            
            SaveFileDialog sf = new SaveFileDialog();
            DialogResult dr = sf.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {                
                String filename = sf.FileName;
                Serialization.SerializeSystem(pipsters, filename);
            }             
        }

        private void toolStripButtonLoadSystem_Click(object sender, EventArgs e)
        {
            OpenFileDialog of = new OpenFileDialog();
            DialogResult result = of.ShowDialog(this);
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                String filename = of.FileName;
                List<Pipster> apipsters = Serialization.DeserializeSystem(filename);
                pipsters.Clear();
                foreach (Pipster pipster in apipsters)
                {
                    createPipster(pipster);
                }
            }
            

        }

        private void MainWin_FormClosing(object sender, FormClosingEventArgs e)
        {
            networkService.Kill();
        }

        private void toolStripButtonMaintainConnection_Click(object sender, EventArgs e)
        {
            if (toolStripButtonStartConnection.Text == "Attiva Server")
            {
                networkService.Listen();
                toolStripButtonStartConnection.Text = "Spegni Server";               
            }
            else
            {
                networkService.Kill();
                toolStripButtonStartConnection.Text = "Attiva Server";
            }
        }


        
    }
}
