﻿namespace PipsterEA.Win.View
{
    partial class EditPipster
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxSymbol = new System.Windows.Forms.ComboBox();
            this.tabPageCloseSignals = new System.Windows.Forms.TabPage();
            this.tabPageOpenHandlers = new System.Windows.Forms.TabPage();
            this.tabControlSignals = new System.Windows.Forms.TabControl();
            this.tabPageOpenSignals = new System.Windows.Forms.TabPage();
            this.tabPageStandbyHandlers = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.labelDescription = new System.Windows.Forms.Label();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.manageOpenSignals = new PipsterEA.Win.View.ManageSignals();
            this.manageCloseSignals = new PipsterEA.Win.View.ManageSignals();
            this.manageOpenHandlers = new PipsterEA.Win.View.ManageSignals();
            this.manageStandbyHandlers = new PipsterEA.Win.View.ManageSignals();
            this.tabPageCloseSignals.SuspendLayout();
            this.tabPageOpenHandlers.SuspendLayout();
            this.tabControlSignals.SuspendLayout();
            this.tabPageOpenSignals.SuspendLayout();
            this.tabPageStandbyHandlers.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            // 
            // comboBoxSymbol
            // 
            this.comboBoxSymbol.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxSymbol.FormattingEnabled = true;
            this.comboBoxSymbol.Items.AddRange(new object[] {
            "EURUSD",
            "GBPUSD"});
            this.comboBoxSymbol.Location = new System.Drawing.Point(84, 11);
            this.comboBoxSymbol.Name = "comboBoxSymbol";
            this.comboBoxSymbol.Size = new System.Drawing.Size(400, 21);
            this.comboBoxSymbol.TabIndex = 7;
            // 
            // tabPageCloseSignals
            // 
            this.tabPageCloseSignals.Controls.Add(this.manageCloseSignals);
            this.tabPageCloseSignals.Location = new System.Drawing.Point(4, 22);
            this.tabPageCloseSignals.Name = "tabPageCloseSignals";
            this.tabPageCloseSignals.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCloseSignals.Size = new System.Drawing.Size(468, 359);
            this.tabPageCloseSignals.TabIndex = 1;
            this.tabPageCloseSignals.Text = "Segnali di chiusura";
            this.tabPageCloseSignals.UseVisualStyleBackColor = true;
            // 
            // tabPageOpenHandlers
            // 
            this.tabPageOpenHandlers.Controls.Add(this.manageOpenHandlers);
            this.tabPageOpenHandlers.Location = new System.Drawing.Point(4, 22);
            this.tabPageOpenHandlers.Name = "tabPageOpenHandlers";
            this.tabPageOpenHandlers.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOpenHandlers.Size = new System.Drawing.Size(468, 359);
            this.tabPageOpenHandlers.TabIndex = 2;
            this.tabPageOpenHandlers.Text = "Gestione apertura";
            this.tabPageOpenHandlers.UseVisualStyleBackColor = true;
            // 
            // tabControlSignals
            // 
            this.tabControlSignals.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlSignals.Controls.Add(this.tabPageOpenSignals);
            this.tabControlSignals.Controls.Add(this.tabPageCloseSignals);
            this.tabControlSignals.Controls.Add(this.tabPageOpenHandlers);
            this.tabControlSignals.Controls.Add(this.tabPageStandbyHandlers);
            this.tabControlSignals.Location = new System.Drawing.Point(15, 83);
            this.tabControlSignals.Name = "tabControlSignals";
            this.tabControlSignals.SelectedIndex = 0;
            this.tabControlSignals.Size = new System.Drawing.Size(476, 385);
            this.tabControlSignals.TabIndex = 6;
            // 
            // tabPageOpenSignals
            // 
            this.tabPageOpenSignals.Controls.Add(this.manageOpenSignals);
            this.tabPageOpenSignals.Location = new System.Drawing.Point(4, 22);
            this.tabPageOpenSignals.Name = "tabPageOpenSignals";
            this.tabPageOpenSignals.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOpenSignals.Size = new System.Drawing.Size(468, 359);
            this.tabPageOpenSignals.TabIndex = 4;
            this.tabPageOpenSignals.Text = "Segnali di apertura";
            this.tabPageOpenSignals.UseVisualStyleBackColor = true;
            // 
            // tabPageStandbyHandlers
            // 
            this.tabPageStandbyHandlers.Controls.Add(this.manageStandbyHandlers);
            this.tabPageStandbyHandlers.Location = new System.Drawing.Point(4, 22);
            this.tabPageStandbyHandlers.Name = "tabPageStandbyHandlers";
            this.tabPageStandbyHandlers.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageStandbyHandlers.Size = new System.Drawing.Size(468, 359);
            this.tabPageStandbyHandlers.TabIndex = 3;
            this.tabPageStandbyHandlers.Text = "Gestione standby";
            this.tabPageStandbyHandlers.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Simbolo:";
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Location = new System.Drawing.Point(12, 46);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(65, 13);
            this.labelDescription.TabIndex = 8;
            this.labelDescription.Text = "Descrizione:";
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDescription.Location = new System.Drawing.Point(84, 43);
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(400, 20);
            this.textBoxDescription.TabIndex = 9;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(414, 474);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 11;
            this.buttonCancel.Text = "Annulla";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(333, 474);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 10;
            this.buttonOk.Text = "Ok";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // manageOpenSignals
            // 
            this.manageOpenSignals.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.manageOpenSignals.Location = new System.Drawing.Point(-4, 3);
            this.manageOpenSignals.Name = "manageOpenSignals";
            this.manageOpenSignals.pipsterList = null;
            this.manageOpenSignals.Size = new System.Drawing.Size(476, 356);
            this.manageOpenSignals.TabIndex = 0;
            this.manageOpenSignals.templates = null;
            // 
            // manageCloseSignals
            // 
            this.manageCloseSignals.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.manageCloseSignals.Location = new System.Drawing.Point(-4, 1);
            this.manageCloseSignals.Name = "manageCloseSignals";
            this.manageCloseSignals.pipsterList = null;
            this.manageCloseSignals.Size = new System.Drawing.Size(476, 356);
            this.manageCloseSignals.TabIndex = 1;
            this.manageCloseSignals.templates = null;
            // 
            // manageOpenHandlers
            // 
            this.manageOpenHandlers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.manageOpenHandlers.Location = new System.Drawing.Point(-4, 1);
            this.manageOpenHandlers.Name = "manageOpenHandlers";
            this.manageOpenHandlers.pipsterList = null;
            this.manageOpenHandlers.Size = new System.Drawing.Size(476, 356);
            this.manageOpenHandlers.TabIndex = 1;
            this.manageOpenHandlers.templates = null;
            // 
            // manageStandbyHandlers
            // 
            this.manageStandbyHandlers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.manageStandbyHandlers.Location = new System.Drawing.Point(-4, 1);
            this.manageStandbyHandlers.Name = "manageStandbyHandlers";
            this.manageStandbyHandlers.pipsterList = null;
            this.manageStandbyHandlers.Size = new System.Drawing.Size(476, 356);
            this.manageStandbyHandlers.TabIndex = 1;
            this.manageStandbyHandlers.templates = null;
            // 
            // EditPipster
            // 
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.textBoxDescription);
            this.Controls.Add(this.labelDescription);
            this.Controls.Add(this.comboBoxSymbol);
            this.Controls.Add(this.tabControlSignals);
            this.Controls.Add(this.label2);
            this.Name = "EditPipster";
            this.Size = new System.Drawing.Size(513, 500);
            this.tabPageCloseSignals.ResumeLayout(false);
            this.tabPageOpenHandlers.ResumeLayout(false);
            this.tabControlSignals.ResumeLayout(false);
            this.tabPageOpenSignals.ResumeLayout(false);
            this.tabPageStandbyHandlers.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxSymbol;
        private System.Windows.Forms.TabPage tabPageCloseSignals;
        private System.Windows.Forms.TabPage tabPageOpenHandlers;
        private System.Windows.Forms.TabControl tabControlSignals;
        private System.Windows.Forms.TabPage tabPageStandbyHandlers;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.TabPage tabPageOpenSignals;
        private ManageSignals manageOpenSignals;
        private ManageSignals manageCloseSignals;
        private ManageSignals manageOpenHandlers;
        private ManageSignals manageStandbyHandlers;
    }
}
