﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using PipsterEA.Core;
using System.Collections;

namespace PipsterEA.Win.View
{
    public partial class ManageSignals : UserControl
    {
        public List<BaseStrategy> templates { get; set; }
        public IList pipsterList { get; set; }

        public ManageSignals()
        {
            InitializeComponent();
        }

        public ManageSignals(List<BaseStrategy> templates, IList pipsterList)
        {
            InitializeComponent();
            this.templates = templates;
            this.pipsterList = pipsterList;
            RefreshSignals();
        }

        public void RefreshSignals()
        {
            listViewSignals.Items.Clear();
            foreach(BaseStrategy bs in pipsterList)
            {
                String[] item = new String[2];                
                item[0] = bs.GetType().Name;
                item[1] = bs.ToString();

                listViewSignals.Items.Add(new ListViewItem(item));                
            }
            listViewSignals.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            for (int i = 0; i < listViewSignals.Columns.Count; i++)
            {
                if (listViewSignals.Columns[i].Width < 80)
                {
                    listViewSignals.Columns[i].Width = 80;
                }
            }
            
        }

        private void toolStripButtonAddOpenSignal_Click(object sender, EventArgs e)
        {
            AddSignal addSignal = new AddSignal(templates);
            addSignal.StrategyAdded += new AddSignal.StrategyEvent(addSignal_StrategyAdded);
            addSignal.ShowDialog(this);
        }

        void addSignal_StrategyAdded(BaseStrategy strategy)
        {
            pipsterList.Add(strategy);
            RefreshSignals();
        }

        void addSignal_StrategyEdited(BaseStrategy strategy)
        {
            int index = listViewSignals.SelectedIndices[0];
            pipsterList.RemoveAt(index);
            pipsterList.Insert(index,strategy);
            RefreshSignals();
        }

        private void toolStripButtonDelete_Click(object sender, EventArgs e)
        {
            if (listViewSignals.SelectedIndices.Count > 0)
            {
                int index = listViewSignals.SelectedIndices[0];
                pipsterList.RemoveAt(index);
                RefreshSignals();
            }
        }

        private void toolStripButtonEdit_Click(object sender, EventArgs e)
        {
            if (listViewSignals.SelectedIndices.Count > 0)
            {
                int index = listViewSignals.SelectedIndices[0];
                BaseStrategy bs = (BaseStrategy)pipsterList[index];
                AddSignal addSignal = new AddSignal(templates);
                addSignal.setSelectedStrategy(bs);
                addSignal.StrategyAdded += new AddSignal.StrategyEvent(addSignal_StrategyEdited);
                addSignal.ShowDialog(this);
            }
        }
    }
}
