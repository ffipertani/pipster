﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using PipsterEA.Core;
using PipsterEA.Conf;

namespace PipsterEA.Win.View
{
    public partial class EditPipster : UserControl
    {
        public delegate void PipsterEvent(Pipster sender);
        public event PipsterEvent PipsterCreated;
        public event PipsterEvent PipsterCancelled;      

        private Pipster pipster;
        public EditPipster(Pipster pipster)
        {
            InitializeComponent();
            this.pipster = pipster;
            manageOpenSignals.templates = Configuration.OpenSignals;
            manageOpenSignals.pipsterList = pipster.OpenSignals;
            manageOpenSignals.RefreshSignals();

            manageCloseSignals.templates = Configuration.CloseSignals;
            manageCloseSignals.pipsterList = pipster.CloseSignals;
            manageCloseSignals.RefreshSignals();

            manageOpenHandlers.templates = Configuration.OpenHandlers;
            manageOpenHandlers.pipsterList = pipster.OpenHandlers;
            manageOpenHandlers.RefreshSignals();

            manageStandbyHandlers.templates = Configuration.StandbyHandlers;
            manageStandbyHandlers.pipsterList = pipster.StandbyHandlers;
            manageStandbyHandlers.RefreshSignals();

            comboBoxSymbol.Text = pipster.Symbol;
            textBoxDescription.Text = pipster.Description;
        }
        /*
        private void toolStripButtonAddOpenSignal_Click(object sender, EventArgs e)
        {
            AddSignal addSignal = new AddSignal(Configuration.OpenSignals);
            addSignal.StrategyAdded += new AddSignal.StrategyEvent(addSignal_StrategyAdded);
            addSignal.ShowDialog(this);
        }
        */
        void addSignal_StrategyAdded(BaseStrategy strategy)
        {
            if (strategy.GetType().IsSubclassOf(typeof(OpenSignal)))
            {
                pipster.RegisterOpenSignal((OpenSignal)strategy);
            }
            if (strategy.GetType().IsSubclassOf(typeof(OpenHandler)))
            {
                pipster.RegisterOpenHandler((OpenHandler)strategy);
            }
            if (strategy.GetType().IsSubclassOf(typeof(CloseSignal)))
            {
                pipster.RegisterCloseSignal((CloseSignal)strategy);
            }
            if (strategy.GetType().IsSubclassOf(typeof(StandbyHandler)))
            {
                pipster.RegisterStandbyHandler((StandbyHandler)strategy);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            if (PipsterCancelled != null)
            {
                PipsterCancelled(pipster);
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            pipster.Symbol = comboBoxSymbol.Text;
            pipster.Description = textBoxDescription.Text;
            if (PipsterCreated != null)
            {
                PipsterCreated(pipster);
            }
        }

    }
}
