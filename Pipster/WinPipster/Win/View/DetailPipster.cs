﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using PipsterEA.Core;
using WinPipster.Win.View;
using PipsterEA.Model;

namespace PipsterEA.Win.View
{
    public partial class DetailPipster : UserControl
    {
        private Pipster pipster;

        public DetailPipster(Pipster pipster)
        {
            InitializeComponent();
            this.pipster = pipster;
            refreshStatus();
            refreshManagedOrders();
            refreshOrders();
        }

        private void refreshManagedOrders()
        {
            labelManagedOrders.Text = pipster.ManagedTickets.Count +"";
        }

        private void refreshStatus()
        {
            if (pipster.Enabled)
            {
                labelStatus.Text = "ATTIVO";
                labelStatus.ForeColor = Color.ForestGreen;
                buttonDisable.Text = "Disattiva";
            }
            else
            {
                labelStatus.Text = "NON ATTIVO";
                labelStatus.ForeColor = Color.DarkRed;
                buttonDisable.Text = "Attiva";
            }
        }

        private void refreshOrders()
        {
            listView1.Items.Clear();
            foreach (int ticket in pipster.ManagedTickets)
            {
                String[] row = new String[2];
                row[0] = ticket.ToString();
               
                listView1.Items.Add(new ListViewItem(row));
            }
        }

        private void buttonDisable_Click(object sender, EventArgs e)
        {
            if (buttonDisable.Text == "Attiva")
            {
                pipster.Enabled = true;
            }
            else
            {
                pipster.Enabled = false;
            }
            refreshStatus();
        }

        private void buttonEmpty_Click(object sender, EventArgs e)
        {
            pipster.ManagedTickets.Clear();
        }

        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            AddOrder addOrder = new AddOrder();
            DialogResult res = addOrder.ShowDialog(this);
            if (res == DialogResult.OK)
            {
                String order = addOrder.GetOrder();
                Int32 intorder = Int32.Parse(order);
                pipster.ManagedTickets.Add(intorder);
                refreshOrders();
            }
           

        }

        private void toolStripButtonDelete_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                int index = listView1.SelectedIndices[0];
                pipster.ManagedTickets.RemoveAt(index);
                refreshOrders();
            }
        }
    }
}
