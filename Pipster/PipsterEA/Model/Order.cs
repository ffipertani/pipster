﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PipsterEA.Model
{
    public class Order
    {
        private static DateTime NULL_DATE = new DateTime(1970, 1, 1, 0, 0, 0);


        public DateTime CloseTime { get; set; }
        public double ClosePrice{get;set;}
        public String Comment{get;set;}
        public double Commission { get; set; }
        public DateTime Expiration { get; set; }
        public double Lots { get; set; }
        public int MagicNumber { get; set; }
        public double OpenPrice { get; set; }
        public DateTime OpenTime { get; set; }
        public double Profit { get; set; }
        public double StopLoss { get; set; }
        public double Swap { get; set; }
        public string Symbol { get; set; }
        public double TakeProfit { get; set; }
        public int Ticket { get; set; }
        public TradeOperation Type { get; set; }

        public Boolean IsStopLoss()
        {
            String comment = Comment;
            if (comment.EndsWith("]"))
            {
                comment = comment.Substring(comment.LastIndexOf("["));
                if (comment.Contains("sl"))
                {
                    return true;
                }
            }
            return false;
        }

        public Boolean IsTakeProfit()
        {
            String comment = Comment;
            if (comment.EndsWith("]"))
            {
                comment = comment.Substring(comment.LastIndexOf("["));
                if (comment.Contains("tp"))
                {
                    return true;
                }
            }
            return false;
        }

        public Boolean IsClosed()
        {
            if (CloseTime.Ticks != NULL_DATE.Ticks)
            {
                return true;
            }
            return false;
        }

        public int GetCreatedFrom()
        {
            if (Comment == null)
            {
                return -1;
            }
            int index = Comment.IndexOf("#");
            if (index > 0)
            {
                String sub = Comment.Substring(index+1);
                int endIndex = sub.IndexOf(" ");
                String numb = null;
                if(endIndex>0){
                    numb = sub.Substring(0,endIndex);
                }else{
                    numb = sub;
                }                
                return Int32.Parse(numb);
                
            }
            return -1;

        }

        public Boolean IsPending()
        {
            return !(Type == TradeOperation.BUY || Type == TradeOperation.SELL);
        }
    }
}
