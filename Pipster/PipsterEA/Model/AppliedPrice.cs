﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PipsterEA.Model
{
    public enum AppliedPrice
    {
        CLOSE = 0,
        OPEN = 1,
        HIGH = 2,
        LOW = 3,
        MEDIAN = 4,
        TYPICAL = 5,
        WEIGHTED = 6
    }
}
