﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PipsterEA.Model
{
    public class TradingHours
    {
        public int From { get; set; }
        public int To { get; set; }

        public TradingHours(int from, int to)
        {
            From = from;
            To = to;
        }
    }
}
