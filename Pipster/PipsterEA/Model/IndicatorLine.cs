﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PipsterEA.Model
{
    public enum IndicatorLine
    {
        MAIN = 0,
        SIGNAL = 1,
        MODE_UPPER = 1,
        MODE_LOWER = 2
    }
}
