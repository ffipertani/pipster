﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PipsterEA.Model
{
    public enum MovingAverageType
    {
        SIMPLE = 0,
        EXPONENTIAL = 1,
        SMOOTHED = 2,
        LINEAR_WEIGHTED = 3
    }
}
