﻿#include <winsock.mqh>
 
#import "user32.dll"
bool GetAsyncKeyState(int Key);
#import

#define ERROR "ERROR" 
#define OK "OK" 
#define DEFAULT_PORT 10000
#property show_inputs
string protocol="TCP";
extern int port=DEFAULT_PORT;
extern string server_addr="127.0.0.1";
extern bool maintain_connection=true;
extern bool DEBUG=true;
 
 
int conn_socket=0; 
 
bool isalpha(int c) {
  string t="0123456789";
  return(StringFind(t,CharToStr(c))<0);
  
}
 
void Usage() {
    Print("\t- The defaults are TCP, 2007, localhost");
    Print("\t- Hit ESC to terminate client script...");
    Print("\t- maxloop is the number of loops to execute 0=endless.");
    Print("\t- server_ip is the IP address of server");
    Print("\t- port is the port to listen on");
    Print("Where:\n\t- protocol is one of TCP or UDP");
    Print("Usage: protocol port server iterations");
}

int init(){
Log("INIT");
   if(maintain_connection){
      do_connect();
   }
}

int  start() {
 
  if(!maintain_connection){
      do_connect();
   }
 
   writeMessage("TICK");
   
    while(!IsStopped()) {  
      string size = readAll(4);            
      if(size==ERROR){
         Log("Read error. Exiting...");
         reinit();        
         break;
      }
    //  Log("SIZE = " + size);            
      string message = readAll(StrToInteger(size));
      if(message==ERROR){
         Log("Read error. Exiting...");
         reinit();         
         break;
      }
      
      int res = execCommand(message);
      if(res==-1){
         break;
      }
      //Sleep(1100);
      //if(GetAsyncKeyState(27)) break;
    }
    if(!maintain_connection){
      deinit();
    } 
      
    
    return(0);
}
 
int deinit() {
  if (conn_socket>0) closesocket(conn_socket);
    WSACleanup();
}

void reinit(){
  if (conn_socket>0) closesocket(conn_socket);
   init();
}

void Log(string s){
   if(DEBUG){
      Print(s);
   }
}

string readAll(int size){
   int Buffer[10000];
   int res = 0;    
   int readed = 0;
 //  Log("Devo leggere " +size+"bytes");
   while(readed<size){
      res =recv(conn_socket, Buffer, size-readed, readed);    
      if (res == SOCKET_ERROR) {
         Log("Client: recv() failed: error " + WSAGetLastError());
         return(ERROR);
      }        
      if (res == 0) {
        Log("Client: Server closed connection.");
        return(ERROR);
      }
    //  Log("Letto " +res+" bytes");
      readed += res;
   }
  // Log("Letto tutto");
   string toreturn = arr2String(Buffer,size);   
   return (toreturn); 
   
}

//*************************PIPSTER ****************************/

string arr2String(int Buffer[],int size){
 string res = "";
  int i=0;
   while(size>0){

   for(int k=0;k<4;k++){
    int val = (Buffer[i]>>(k*8))&0xFF;       
    res=res+CharToStr(val);            
    size--;
    if(size==0){
      return (res);
    }
   }
   i++;
  
  }
  return(res);
}


 

int execCommand(string message){  
   
   string command =csv_get(message,0);
   string restOfMessage = csv_delete(message,0);     
   //Log("Command:"+command);
  // Log("RestOfMessage:" + restOfMessage); 
   
   /**COMMANDS**/
   
   if(command=="listOrders"){
      listOrders();
   }else if(command=="loadOrder"){
      loadOrder(restOfMessage);
   }else if(command=="closeOrder"){
      closeOrder(restOfMessage);
   }else if(command=="openOrder"){
      openOrder(restOfMessage);
   }else if(command=="editOrder"){
      editOrder(restOfMessage);
   }else if(command=="ask"){
      ask(restOfMessage);
   }else if(command=="bid"){
      bid(restOfMessage);
   }else if(command=="point"){
      point(restOfMessage);
   }else if(command=="digits"){
      digits(restOfMessage);
   }else if(command=="spread"){
      spread(restOfMessage);
   }else if(command=="timeCurrent"){
     time_current(restOfMessage);   
   }else if(command=="accountSize"){
      accountSize();       
   }else if(command=="time"){
      time(restOfMessage);
   }else if(command=="close"){
      return (-1);   
   }else if(command=="deleteOrder"){
      deleteOrder(restOfMessage);
   }     
    
   
   /** INDICATORS **/
   if(command=="iMA"){
      ima(restOfMessage);
   }else if(command=="iStochastic"){
      istochastic(restOfMessage);
   }else if(command=="iGator"){
      igator(restOfMessage);
   }else if(command=="StandardDeviation"){
      standarddeviation(restOfMessage);   
   }else if(command=="BearsPower"){
      bearsPower(restOfMessage);
   }else if(command=="BullsPower"){
      bullsPower(restOfMessage);
   }else if(command=="BollingerBands"){
      bollinger(restOfMessage);
   }else if(command=="Atr"){
      atr(restOfMessage);
   }else if(command=="Adx"){
      adx(restOfMessage);
   }else if(command=="Macd"){
      macd(restOfMessage);
   }else if(command=="Envelopes"){
      envelopes(restOfMessage);
   }else if(command=="Rsi"){
      rsi(restOfMessage);
   }else if(command=="volume"){
      volume(restOfMessage);
   }else if(command=="highest"){
      highest(restOfMessage);
   }else if(command == "lowest"){
      lowest(restOfMessage);
   }else if(command=="ihigh"){
      high(restOfMessage);
   }else if(command=="ilow"){
      low(restOfMessage);
   }else if(command=="iopen"){
      open(restOfMessage);
   }
   return (0);   
}

void writeMessage(string message){

   int respBuffer[1000];
 
 //  Log("Message " + message);
   int size = StringLen(message);  
   string sizeStr ="";
  // Log("SIZE " + size); 
   if(size==0){
   //  Log("Scrivo NULL");
     message = "NULL";
     size = StringLen(message);  
   }
   int arrSize[4];   
   if(size<10){
      sizeStr = "000"+size;  
   }else if(size<100){
      sizeStr = "00"+size;
   }else if(size<1000){
      sizeStr = "0"+size;
   }
   
   str2struct(arrSize,ArraySize(arrSize)<<18,sizeStr);

   int res = send(conn_socket, arrSize, 4, 0);
  // Log("RISULTATO WRITE "+res);  
   for(int i=0;i<ArraySize(respBuffer);i++){
      respBuffer[i]=0;
   }
    str2struct(respBuffer,size<<18,message);
    send(conn_socket, respBuffer, size<<2, 0);
 //  str2struct(respBuffer,ArraySize(respBuffer)<<18,message);
  // send(conn_socket, respBuffer, ArraySize(respBuffer)<<2, 0);
 
}

 
/***********************INDICATORS**************************/
void writePrice(int type,int value){
  if(type==MODE_OPEN){
   writeMessage(Open[value]+""); 
  }else if(type==MODE_CLOSE){
   writeMessage(Close[value]+""); 
  }else if(type==MODE_HIGH){
   writeMessage(High[value]+""); 
  }else if(type==MODE_LOW){
   writeMessage(Low[value]+""); 
  }else if(type==MODE_VOLUME){
   writeMessage(Volume[value]+""); 
  }
}

void volume(string message){
    string symbol = csv_get(message,0);
   int timeframe = StrToInteger(csv_get(message,1));
   int shift = StrToInteger(csv_get(message,2));
   int volume = iVolume(symbol,timeframe,shift);
   writeMessage(volume+"");
}

void time(string message){
   string symbol = csv_get(message,0);
   int timeframe = StrToInteger(csv_get(message,1));
   int shift = StrToInteger(csv_get(message,2));
   datetime time = iTime(symbol,timeframe,shift);
  // Log("TIME " + time + "----" + TimeToStr(time, TIME_DATE|TIME_MINUTES));   
   writeMessage(TimeToStr(time));
}

void lowest(string message){
string symbol = csv_get(message,0);
int timeframe = StrToInteger(csv_get(message,1));
int type = StrToInteger(csv_get(message,2));
int count = StrToInteger(csv_get(message,3));
int from = StrToInteger(csv_get(message,4));
  double value = iLowest(symbol,timeframe,type,count,from);
 writePrice(type,value);
  
}

void highest(string message){
string symbol = csv_get(message,0);
int timeframe = StrToInteger(csv_get(message,1));
int type = StrToInteger(csv_get(message,2));
int count = StrToInteger(csv_get(message,3));
int from = StrToInteger(csv_get(message,4));
  double value = iHighest(symbol,timeframe,type,count,from);
   writePrice(type,value);
}

void high(string message){
string symbol = csv_get(message,0);
int timeframe = StrToInteger(csv_get(message,1));
int shift = StrToInteger(csv_get(message,2));
double value = iHigh(symbol,timeframe,shift);
writeMessage(value);
}

void low(string message){
string symbol = csv_get(message,0);
int timeframe = StrToInteger(csv_get(message,1));
int shift = StrToInteger(csv_get(message,2));
double value = iLow(symbol,timeframe,shift);
writeMessage(value);
}

void open(string message){
string symbol = csv_get(message,0);
int timeframe = StrToInteger(csv_get(message,1));
int shift = StrToInteger(csv_get(message,2));
double value = iOpen(symbol,timeframe,shift);
writeMessage(value);
}

void macd(string message){
string symbol = csv_get(message,0);
int timeframe = StrToInteger(csv_get(message,1));
//fast_ema_period,slow_ema_period,signal_period,applied_price,mode,shift
int fast_ema_period = StrToInteger(csv_get(message,2));
int slow_ema_period =  StrToInteger(csv_get(message,3));
int signal_period =  StrToInteger(csv_get(message,4));
int applied_price =  StrToInteger(csv_get(message,5));
int mode =  StrToInteger(csv_get(message,6));
int shift =  StrToInteger(csv_get(message,7));
//iMACD(NULL,0,244,630,13,PRICE_CLOSE,MODE_MAIN,0);
double value = iMACD(symbol,timeframe,fast_ema_period,slow_ema_period,signal_period,applied_price,mode,shift);

//Log("Sending MACD " + value + " " + mode + " " + fast_ema_period);
writeMessage(value+"");
}

void atr(string message){
string symbol = csv_get(message,0);
int timeframe = StrToInteger(csv_get(message,1));
int period = StrToInteger(csv_get(message,2));
int shift =  StrToInteger(csv_get(message,3));
double value = iATR(symbol,timeframe,period,shift);
//Log("ATR " + value);
writeMessage(value+"");
}

void adx(string message){
string symbol = csv_get(message,0);
int timeframe = StrToInteger(csv_get(message,1));
int period = StrToInteger(csv_get(message,2));
int appliedPrice = StrToInteger(csv_get(message,2));
int mode = StrToInteger(csv_get(message,2));
int shift =  StrToInteger(csv_get(message,3));
double value = iADX(symbol,timeframe,period,appliedPrice, mode, shift);
writeMessage(value+"");
}

void envelopes(string message){
string symbol = csv_get(message,0);
int timeframe = StrToInteger(csv_get(message,1));
int period = StrToInteger(csv_get(message,2));
int type = StrToInteger(csv_get(message,3));
int ma_shift = StrToInteger(csv_get(message,4));
int appliedPrice = StrToInteger(csv_get(message,5));
double deviation = StrToDouble(csv_get(message,6));
int indicatorLine = StrToInteger(csv_get(message,7));
int shift = StrToInteger(csv_get(message,8));
double value = iEnvelopes(symbol,timeframe,period,type,ma_shift,appliedPrice,deviation,indicatorLine,shift);
writeMessage(value+"");
}

void bullsPower(string message){
   string symbol = csv_get(message,0);
int timeframe = StrToInteger(csv_get(message,1));
int period = StrToInteger(csv_get(message,2));
int appliedPrice =StrToInteger(csv_get(message,3));
int shift = StrToInteger(csv_get(message,4));
double value = iBullsPower(symbol,timeframe,period,appliedPrice,shift);
writeMessage(value+"");
}

void bearsPower(string message){
   string symbol = csv_get(message,0);
int timeframe = StrToInteger(csv_get(message,1));
int period = StrToInteger(csv_get(message,2));
int appliedPrice =StrToInteger(csv_get(message,3));
int shift = StrToInteger(csv_get(message,4));
double value = iBullsPower(symbol,timeframe,period,appliedPrice,shift);
writeMessage(value+"");
}


void ima(string message){
string symbol = csv_get(message,0);
int timeframe = StrToInteger(csv_get(message,1));
int period = StrToInteger(csv_get(message,2));
int ma_shift = StrToInteger(csv_get(message,3));
int ma_method = StrToInteger(csv_get(message,4));
int applied_price = StrToInteger(csv_get(message,5));
int shift = StrToInteger(csv_get(message,6));
double val = iMA(symbol, timeframe, period, ma_shift, ma_method, applied_price, shift);

//Log("Sending IMA " + val);
writeMessage(val+"");
} 
 
void rsi(string message){
string symbol = csv_get(message,0);
int timeframe = StrToInteger(csv_get(message,1));
int period = StrToInteger(csv_get(message,2));
int applied_price = StrToInteger(csv_get(message,3));
int shift = StrToInteger(csv_get(message,4));

double val = iRSI(symbol,timeframe,period,applied_price,shift);
writeMessage(val+"");
}
 
void istochastic(string message){
string symbol = csv_get(message,0);
int timeframe = StrToInteger(csv_get(message,1));
int kperiod = StrToInteger(csv_get(message,2));
int dperiod = StrToInteger(csv_get(message,3));
int slowing = StrToInteger(csv_get(message,4));
int method = StrToInteger(csv_get(message,5));
int price_field = StrToInteger(csv_get(message,6));
int indicator_line = StrToInteger(csv_get(message,7));
int shift = StrToInteger(csv_get(message,8));
double val = iStochastic(symbol,timeframe,kperiod,dperiod,slowing,method,price_field,indicator_line,shift);
writeMessage(val+"");
} 

void igator(string message){
string symbol = csv_get(message,0);
int timeframe = StrToInteger(csv_get(message,1));
int jaw_period = StrToInteger(csv_get(message,2));
int jaw_shift = StrToInteger(csv_get(message,3));
int teeth_period = StrToInteger(csv_get(message,4));
int teeth_shift = StrToInteger(csv_get(message,5));
int lips_period = StrToInteger(csv_get(message,6));
int lips_shift = StrToInteger(csv_get(message,7));
int ma_method = StrToInteger(csv_get(message,8));
int appliedPrice = StrToInteger(csv_get(message,9));
int indicatorLine = StrToInteger(csv_get(message,10));
int shift = StrToInteger(csv_get(message,11));
double val = iGator(symbol,timeframe,jaw_period,jaw_shift,teeth_period,teeth_shift,lips_period,lips_shift,ma_method,appliedPrice,indicatorLine,shift);
writeMessage(val+"");
} 


void standarddeviation(string message){
string symbol = csv_get(message,0);
int timeframe = StrToInteger(csv_get(message,1));
int ma_period = StrToInteger(csv_get(message,2));
int ma_shift = StrToInteger(csv_get(message,3));
int ma_method = StrToInteger(csv_get(message,4));
int appliedPrice = StrToInteger(csv_get(message,5));
int shift = StrToInteger(csv_get(message,6));
double val = iStdDev(symbol,timeframe,ma_period,ma_shift, ma_method, appliedPrice, shift);
writeMessage(val+"");
} 

void bollinger(string message){
string symbol = csv_get(message,0);
int timeframe = StrToInteger(csv_get(message,1));
int period = StrToInteger(csv_get(message,2));
int deviation = StrToInteger(csv_get(message,3));
int bands_shift = StrToInteger(csv_get(message,4));
int applied_price = StrToInteger(csv_get(message,5));
int indicatorLine = StrToInteger(csv_get(message,6));
int shift = StrToInteger(csv_get(message,7));
double val = iBands(symbol,timeframe,period,deviation, bands_shift, applied_price, indicatorLine,shift);
writeMessage(val+"");
} 
 
/***********************TRADING FUNCTIONS********************/ 
 
void ask(string symbol){
   //Log("Ask "+ symbol);
   RefreshRates();
   double res = MarketInfo(symbol,MODE_ASK);
   writeMessage(res+"");
} 

void bid(string symbol){
   RefreshRates();
   double res = MarketInfo(symbol,MODE_ASK);
   writeMessage(res+"");
}

void point(string symbol){
   double res = MarketInfo(symbol,MODE_POINT);
   writeMessage(res+"");
}

void digits(string symbol){
   double res = MarketInfo(symbol,MODE_DIGITS);
   writeMessage(res+"");
}

void spread(string symbol){
   double res = MarketInfo(symbol,MODE_SPREAD);
   writeMessage(res+"");
}
 
void time_current(string message){
   datetime current = TimeCurrent();
   writeMessage(current+"");
}

void deleteOrder(string message){
int ticket = StrToInteger(csv_get(message,0));
OrderDelete(ticket);
}

void loadOrder(string message){
int ticket = StrToInteger(csv_get(message,0));
if(OrderSelect(ticket,SELECT_BY_TICKET)==false) writeMessage(ERROR); 
Log("Open:" + OrderOpenPrice());
Log("Close:"+OrderClosePrice());
Log("OrderComment:"+OrderComment());
string order = OrderClosePrice()+","+OrderComment()+","+OrderCommission()+","+TimeToStr(OrderExpiration())+","+OrderLots()+","+OrderMagicNumber()+","+OrderOpenPrice()+","+TimeToStr(OrderOpenTime())+","+OrderProfit()+","+OrderStopLoss()+","+OrderSwap()+","+OrderSymbol()+","+OrderTakeProfit()+","+OrderTicket()+","+OrderType()+","+TimeToStr(OrderCloseTime())+",";
writeMessage(order);
Log(order);
}

void editOrder(string message){
double price = StrToDouble(csv_get(message,6));
double stoploss = StrToDouble(csv_get(message,9));
double takeprofit =StrToDouble(csv_get(message,12));
int ticket =StrToInteger(csv_get(message,13));

bool success = OrderModify(ticket,price,stoploss,takeprofit,NULL);
if(!success){
   Log("Error modifying order "+ GetLastError());
}
}

void openOrder(string message){
string comment = csv_get(message,1);
int type = StrToInteger(csv_get(message,14));
double lots = StrToDouble(csv_get(message,4));
string symbol = csv_get(message,11);
double price = StrToDouble(csv_get(message,6));
double stoploss = StrToDouble(csv_get(message,9));
double takeprofit =StrToDouble(csv_get(message,12));
 
  
RefreshRates();
if(type == OP_BUY){
   price = Ask;
}else if(type == OP_SELL){
   price = Bid;
}
Log("takePRofit:"+takeprofit);
int res = OrderSend(symbol,type,lots,price,30,stoploss,takeprofit,comment); 
writeMessage(res+"");
if(res<0) 
{
  Log("OrderSend failed with error #" + GetLastError());
  return(0);
}
//RefreshRates();
//Log(MarketInfo(symbol,MODE_BID));  
}

void closeOrder(string message){
 int ticket = StrToInteger(csv_get(message,0));
 double lots = StrToDouble(csv_get(message,1));
 double price = StrToDouble(csv_get(message,2));
 //Log("Lots..." + lots);
 bool res = OrderClose(ticket,lots,price,3,Red);
 if(res){
   writeMessage(OK);
 }else{
   writeMessage(ERROR);
 }
} 
 
void listOrders(){     
   int total=OrdersTotal();
   string orders = "";
   // write open orders
   for(int pos=0;pos<total;pos++)
   {
      if(OrderSelect(pos,SELECT_BY_POS)==false) continue; 
      string order = OrderClosePrice()+","+OrderComment()+","+OrderCommission()+","+TimeToStr(OrderExpiration())+","+OrderLots()+","+OrderMagicNumber()+","+OrderOpenPrice()+","+TimeToStr(OrderOpenTime())+","+OrderProfit()+","+OrderStopLoss()+","+OrderSwap()+","+OrderSymbol()+","+OrderTakeProfit()+","+OrderTicket()+","+OrderType()+","+TimeToStr(OrderCloseTime());
     // Log(order);
      if(orders==""){
         orders = order;
      }else{
         orders = orders+","+order;
      }
   }
   writeMessage(orders);   
} 



void accountSize(){
double balance = AccountBalance();
writeMessage(balance);
}











/************ CSV *****************/
string csv_get(string str, int index){
int count = 0;
int startIndex = 0;
int endIndex = StringLen(str);

//Log("GETTING csv " +str +", Index " + index);

for(int i=0;i<StringLen(str);i++){
   if(count==index){
      startIndex = i;
      break;
   }
   if(StringGetChar(str,i)==','){
      count++;
   }        
}
string tmp = StringSubstr(str,startIndex);
for(int k=0;k<StringLen(tmp);k++){
   if(StringGetChar(tmp,k)==','){
       endIndex = k;
       break;
   }
}
//Log("StartIndex " + startIndex);      
//   Log("EndIndex " + endIndex);
   string toreturn = StringSubstr(tmp,0,endIndex);
//   Log("Result: "+toreturn);
return(toreturn);

}


string csv_delete(string str, int index){
   int count = 0;
   int startIndex = 0;
   int endIndex = StringLen(str);
//   Log("Deleting csv " +str +", Index " + index);
   for(int i=0;i<StringLen(str);i++){
      if(count==index){
         startIndex = i;
         break;
      }
      if(StringGetChar(str,i)==','){
         count++;
      }              
   }
     
   for(int k=i;k<StringLen(str);k++){
      if(StringGetChar(str,k)==','){
          endIndex = k;
          break;
      }
   }
   
 //  Log("StartIndex " + startIndex);      
  // Log("EndIndex " + endIndex);
   
   string beforeString = StringSubstr(str,0,startIndex);
   string afterString = StringSubstr(str,endIndex+1);
   
 //  Log("BeforeString " + beforeString);
 //  Log("AfterString " + afterString);
   
   if(startIndex!=0){
      return(beforeString+afterString);   
   }else{
      return (afterString);
   }
  
   
}





int do_connect(){
    int Buffer[10000];
    int retval;
    int i;
    int addr[1];
    int socket_type;
    int server[sockaddr_in];
    int hp;
    int wsaData[WSADATA];
 
     if (protocol=="TCP")
      socket_type = SOCK_STREAM; 
    else if (protocol=="UDP")
      socket_type = SOCK_DGRAM;
    else {
      Usage();
      return(-1);
    }    
    if (port==0) {
      Usage();
      return(-1);
    }    
    retval = WSAStartup(0x202, wsaData);
    if (retval != 0) {
       Log("Client: WSAStartup() failed with error "+ retval);
       return(-1);
    }  
 
    if (isalpha(StringGetChar(server_addr,0))){
        Log("Client: IP address should be given in numeric form in this version.");
        return(-1);
    } else { 
        addr[0] = inet_addr(server_addr); 
        //hp = gethostbyaddr(addr, 4, AF_INET);
        hp = addr[0];
        //Log("server addr:"+addr[0]+" hp"+hp);
    }
    if (hp == 0 ) {
       Log("Client: Cannot resolve address \""+server_addr+"\": Error :"+WSAGetLastError());
       return(-1);
    }
    
    int2struct(server,sin_addr,addr[0]);
    int2struct(server,sin_family,AF_INET);
    int2struct(server,sin_port,htons(port));
 
    conn_socket = socket(AF_INET, socket_type, 0); 
    if (conn_socket <0 ) {
        Log("Client: Error Opening socket: Error "+ WSAGetLastError());
        return(-1);
    }  
 
   // Log("Client: Client connecting to: "+ server_addr);
    retval=connect(conn_socket, server, ArraySize(server)<<2);
    if (retval == SOCKET_ERROR) {
        Log("Client: connect() failed: " +  WSAGetLastError());
        return(-1);
    }
    return (0);    
}