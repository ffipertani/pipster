﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Service;
using System.Runtime.CompilerServices;
using PipsterEA.Utils;

namespace PipsterEA.Core
{
    public class Pipster
    {        
        protected TradingService tradingService = new TradingService();
        protected IndicatorService indicatorService = new IndicatorService();
        public List<Order> Orders = new List<Order>();
        public DateTime Now { get; set; }        
        public String Symbol { get; set; }
        public Double Point { get; set; }
        public Double Ask { get; set; }
        public Double Bid { get; set; }
        public Double Digits { get; set; }
        public Double Spread { get; set; }
        public List<int> ManagedTickets = new List<int>();
        public String Description { get; set; }
        public Boolean Enabled { get; set; }
                     
        public List<OpenSignal> OpenSignals = new List<OpenSignal>();                
        public List<CloseSignal> CloseSignals = new List<CloseSignal>();                
        public List<OpenHandler> OpenHandlers = new List<OpenHandler>();
        public List<StandbyHandler> StandbyHandlers = new List<StandbyHandler>();
        


        public delegate void TickEvent(Pipster sender);
        public event TickEvent Tick;              
        public delegate void OrderPlacedEvent(Order order);
        public event OrderPlacedEvent OrderPlaced;
        public delegate void OrderClosedEvent(Order order);
        public event OrderClosedEvent OrderClosed;
        
       

        public List<Order> ManagedOrders
        {
            get
            {
                List<Order> managed = new List<Order>();
                foreach(int ticket in ManagedTickets)
                {
                    foreach(Order order in Orders)
                    {
                        if (order.Ticket == ticket)
                        {
                            managed.Add(order);
                        }
                    }
                }
                return managed;
            }
        }

        public Boolean isStandby()
        {
            Boolean stanby = false;
            foreach (StandbyHandler sh in StandbyHandlers)
            {
                stanby = sh.IsStandby();
                if (stanby)
                {
                    return true;
                }
            }
            return stanby;
        }

        public Pipster():this("EURUSD"){}
        
        public Pipster(String symbol)
        {            
            Symbol = symbol;
        /*
            RegisterStandbyHandler(new MaxOrdersStandby(1));
            RegisterOpenHandler(new SimpleStopLoss(10));
            RegisterOpenHandler(new SimpleTakeProfit(20));
            RegisterOpenHandler(new SimpleLot(0.01));
         */
        }


        
        
        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual void Start()           
        {
          
                refresh();
              

                if (Tick != null)
                {
                    Tick(this);
                }

                if (!Enabled)
                {
                    return;
                }


                if (ManagedTickets.Count > 0)
                {
                    close();
                }

               

                if (!isStandby())
                {
                    open();
                }
           
                                  
        }


        protected virtual void close()
        {
            List<Order> tmpOrders = new List<Order>(ManagedOrders);
            foreach(Order order in tmpOrders)
            {
                if (order.IsClosed())
                {                   
                    continue;
                }
                double lots = 0;
                foreach (CloseSignal closeSignal in CloseSignals)
                {
                    lots += closeSignal.CloseLots(order);
                }

                if (lots > 0)
                {
                    if (lots > order.Lots)
                    {
                        lots = order.Lots;
                    }
                    doClose(order, lots);                   
                }

            }
        }

        protected virtual void doClose(Order order, Double lots)
        {
            Boolean succeded = false;
            int retry = 5;

            while (!succeded && retry > 0)
            {
                if (order.Type == TradeOperation.BUY)
                {
                    succeded = tradingService.CloseOrder(order.Ticket, lots, order.ClosePrice);
                }
                else if (order.Type == TradeOperation.SELL)
                {
                    succeded = tradingService.CloseOrder(order.Ticket, lots, order.ClosePrice);
                }
                refreshOrders();
                retry--;
            }
        }

        protected virtual void BeforeOpen()
        {
            foreach (OpenSignal signal in OpenSignals)
            {
                signal.RefreshData();
            }
        }

        protected virtual Boolean CanBuy()
        {
            Boolean canBuy = OpenSignals.Count > 0;
            foreach (OpenSignal signal in OpenSignals)
            {
                canBuy = canBuy && signal.CanBuy();
            }
            return canBuy;
        }

        protected virtual Boolean CanSell()
        {
            Boolean canSell = OpenSignals.Count > 0;
            foreach (OpenSignal signal in OpenSignals)
            {
                canSell = canSell && signal.CanSell();
            }
            return canSell;
        }

        protected virtual void open()
        {
            BeforeOpen();

            Boolean canBuy = CanBuy();
            
            if (canBuy)
            {
                buy();
            }

            if (isStandby())
            {
                return;
            }

            Boolean canSell = CanSell();            
            if (canSell)
            {
                sell();
            }       

        }

        protected virtual void buy()
        {
            Order order = new Order();
            order.Type = TradeOperation.BUY;
            order.OpenPrice = tradingService.Ask(Symbol);                                                             
            order.Comment = "Pipster " + Description;
            order.Symbol = Symbol;

            order = doOpen(order);

            int managedTicket = tradingService.OpenOrder(order);
            if (managedTicket > 0)
            {
                if (OrderPlaced != null)
                {
                    OrderPlaced(order);
                }
                ManagedTickets.Add(managedTicket);
                refreshOrders();
            }
            
        }

        protected virtual void sell()
        {
            Order order = new Order();
            order.Type = TradeOperation.SELL;
            order.OpenPrice = tradingService.Bid(Symbol);
            order.Comment = "Pipster " + Description;
            order.Symbol = Symbol;

            order = doOpen(order);

            int managedTicket = tradingService.OpenOrder(order);
            if (managedTicket > 0)
            {
                if (OrderPlaced != null)
                {
                    OrderPlaced(order);
                }
                ManagedTickets.Add(managedTicket);
                refreshOrders();
            }
        }

        protected virtual Order doOpen(Order order)
        {
            foreach (OpenHandler openHandler in OpenHandlers)
            {
                order = openHandler.CreateOrder(order);
            }
            return order;
        }
        
        protected void refresh()
        {
            refreshOrders();
            Now = tradingService.getNow();
            Point = tradingService.Point(Symbol);
            Spread = tradingService.Spread(Symbol);
            Ask = tradingService.Ask(Symbol);
            Bid = tradingService.Bid(Symbol);
            Digits = tradingService.Digits(Symbol);            
        }
        
       
        
        protected virtual void refreshOrders()
        {
            Orders = tradingService.ListOrders();
            for (int i = 0; i < ManagedTickets.Count; i++) 
            {
                Boolean found = false;
                foreach (Order order in Orders)
                {
                    if (order.Ticket == ManagedTickets[i])
                    {
                        found = true;
                        break;
                    }
                    if(order.Comment.Contains(ManagedTickets[i].ToString()))
                    {
                        ManagedTickets.Add(order.Ticket);                        
                        break;
                    }
                }
                if (!found)
                {
                    Order closed = tradingService.LoadOrder(ManagedTickets[i]);
                    if (OrderClosed != null)
                    {
                        OrderClosed(closed);
                    }
                    ManagedTickets.RemoveAt(i);
                    i--;
                }
            }
        }

        public void RegisterStrategy(BaseStrategy strategy)
        {
            strategy.SetPipster(this);           
            strategy.Init();
        }



        public void RegisterOpenHandler(OpenHandler oh)
        {
            RegisterStrategy(oh);
            OpenHandlers.Add(oh);
        }

        public void RegisterOpenSignal(OpenSignal os)
        {
            RegisterStrategy(os);
            OpenSignals.Add(os);
        }

        public void RegisterCloseSignal(CloseSignal cs)
        {
            RegisterStrategy(cs);
            CloseSignals.Add(cs);
        }

        public void RegisterStandbyHandler(StandbyHandler sh)
        {
            RegisterStrategy(sh);
            StandbyHandlers.Add(sh);
        }

        public void RegisterAll()
        {
            foreach (OpenSignal os in OpenSignals)
            {
                RegisterStrategy(os);
            }
            foreach (CloseSignal os in CloseSignals)
            {
                RegisterStrategy(os);
            }
            foreach (OpenHandler os in OpenHandlers)
            {
                RegisterStrategy(os);
            }
            foreach (StandbyHandler os in StandbyHandlers)
            {
                RegisterStrategy(os);
            }
        }
      

        public Double CountPips(Order order)
        {
            String symbol = order.Symbol;
            Double pips = -1;            
            if (order.Type == TradeOperation.BUY)
            {                
                pips = (order.ClosePrice - order.OpenPrice) / Point;

            }
            else if (order.Type == TradeOperation.SELL)
            {                
                pips = (order.OpenPrice - order.ClosePrice) / Point;
            }

            return pips;
        }
        
        public void ClearAllSignals()
        {            
                OpenHandlers.Clear();
                OpenSignals.Clear();
                StandbyHandlers.Clear();
                CloseSignals.Clear();             
        }
        
        public void ClearSignals()
        {             
                OpenSignals.Clear();
                CloseSignals.Clear();             
        }
       
        public void RemoveSignal(Type type)
        {
            List<OpenSignal> tmpSignal = new List<OpenSignal>(OpenSignals);

            foreach (OpenSignal signal in tmpSignal) 
            {
                if (signal.GetType() == type)
                {
                    OpenSignals.Remove(signal);
                }
            }

            List<CloseSignal> tmpSignalClose = new List<CloseSignal>(CloseSignals);

            foreach (CloseSignal signal in tmpSignalClose)
            {
                if (signal.GetType() == type)
                {
                    CloseSignals.Remove(signal);
                }
            }
        }
        
        public void RemoveHandler(Type type)
        {
            List<OpenHandler> tmpSignal = new List<OpenHandler>(OpenHandlers);

            foreach (OpenHandler signal in tmpSignal)
            {
                if (signal.GetType() == type)
                {
                    OpenHandlers.Remove(signal);
                }
            }

            List<StandbyHandler> tmpSignalStandby = new List<StandbyHandler>(StandbyHandlers);

            foreach (StandbyHandler signal in tmpSignalStandby)
            {
                if (signal.GetType() == type)
                {
                    StandbyHandlers.Remove(signal);
                }
            }
        }

        public Pipster Clone()
        {
            Pipster newPipster = (Pipster)Reflection.Clone(this);
            newPipster.ClearAllSignals();
            foreach(BaseStrategy bs in OpenSignals)
            {
                newPipster.RegisterOpenSignal((OpenSignal)Reflection.Clone(bs));
            }
            foreach (BaseStrategy bs in CloseSignals)
            {
                newPipster.RegisterCloseSignal((CloseSignal)Reflection.Clone(bs));
            }
            foreach (BaseStrategy bs in OpenHandlers)
            {
                newPipster.RegisterOpenHandler((OpenHandler)Reflection.Clone(bs));
            }
            foreach (BaseStrategy bs in StandbyHandlers)
            {
                newPipster.RegisterStandbyHandler((StandbyHandler)Reflection.Clone(bs));
            }
            return newPipster;
        }
    }
}
