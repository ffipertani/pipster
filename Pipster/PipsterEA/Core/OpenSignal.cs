﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Service;

namespace PipsterEA.Core
{
    public abstract class OpenSignal:BaseStrategy
    {            
        public abstract void RefreshData();
        public abstract Boolean CanBuy();
        public abstract Boolean CanSell();
        
       
    }
}
