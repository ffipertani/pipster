﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Utils;

namespace PipsterEA.Service
{
    public class IndicatorService
    {
        NetworkService networkService = NetworkService.GetInstance();

        public Double MovingAverage(String symbol, TimeFrame timeFrame, int period, int ma_shift, MovingAverageType type, AppliedPrice appliedPrice, int shift)
        {
            String[] message = new String[]{"iMA",symbol,((Int32)timeFrame).ToString(),period.ToString(),ma_shift.ToString(),((int)type).ToString(),((int)appliedPrice).ToString(),shift.ToString()};

           networkService.writeCommand(message);
           String[] response = networkService.readResponse();
           return Conversion.getDouble(response[0]);
        }

        public Double Macd(String symbol, TimeFrame timeFrame, int fast_period, int slow_period, int signal_period, AppliedPrice appliedPrice, IndicatorLine indicatorLine, int shift)
        {
            String[] message = new String[] { "Macd", symbol, ((Int32)timeFrame).ToString(), fast_period.ToString(), slow_period.ToString(), signal_period.ToString(), ((int)appliedPrice).ToString(), ((int)indicatorLine).ToString(), shift.ToString() };

            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public Double Stochastic(String symbol, TimeFrame timeFrame, int kperiod, int dperiod, int slowing, MovingAverageType type, PriceField priceField, IndicatorLine indicatorLine, int shift)
        {
            String[] message = new String[] { "iStochastic", symbol, ((Int32)timeFrame).ToString(), kperiod.ToString(), dperiod.ToString(), slowing.ToString(), ((int)type).ToString(), ((int)priceField).ToString(), ((int)indicatorLine).ToString(), shift.ToString() };

            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public Double Gator(String symbol, TimeFrame timeFrame, int jaw_period, int jaw_shift, int teeth_period, int teeth_shift, int lips_period, int lips_shift, MovingAverageType ma_method, AppliedPrice appliedPrice, IndicatorLine indicatorLine, int shift) 
        {
            String[] message = new String[] { "iGator", symbol, ((Int32)timeFrame).ToString(), jaw_period.ToString(), jaw_shift.ToString(),teeth_period.ToString(), teeth_shift.ToString(),lips_period.ToString(), lips_shift.ToString(),((int)ma_method).ToString(),((int)appliedPrice).ToString(),((int)indicatorLine).ToString(),shift.ToString() };
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public Double StandardDeviation(String symbol, TimeFrame timeFrame, int ma_period, int ma_shift, MovingAverageType ma_method, AppliedPrice appliedPrice, int shift)
        {
            String[] message = new String[] { "StandardDeviation", symbol, ((Int32)timeFrame).ToString(), ma_period.ToString(), ma_shift.ToString(), ((int)ma_method).ToString(), ((int)appliedPrice).ToString(), shift.ToString() };
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public Double BollingerBands(String symbol, TimeFrame timeFrame, int period, int deviation, int bands_shift, AppliedPrice applied_price, IndicatorLine indicatorLine, int shift)
        {
            String[] message = new String[] { "BollingerBands", symbol, ((Int32)timeFrame).ToString(), period.ToString(), deviation.ToString(), ((int)bands_shift).ToString(), ((int)applied_price).ToString(), ((int)indicatorLine).ToString(), shift.ToString() };
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public Double Rsi(String symbol, TimeFrame timeFrame, int period, AppliedPrice appliedPrice, int shift)
        {
            String[] message = new String[] { "Rsi", symbol, ((Int32)timeFrame).ToString(), period.ToString(), ((int)appliedPrice).ToString(), shift.ToString()};
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public Double BullsPower(String symbol, TimeFrame timeFrame, int period, AppliedPrice appliedPrice, int shift)
        {
            String[] message = new String[] { "BullsPower", symbol, ((Int32)timeFrame).ToString(), period.ToString(), ((int)appliedPrice).ToString(), shift.ToString() };
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public Double BearsPower(String symbol, TimeFrame timeFrame, int period, AppliedPrice appliedPrice, int shift)
        {
            String[] message = new String[] { "BearsPower", symbol, ((Int32)timeFrame).ToString(), period.ToString(), ((int)appliedPrice).ToString(), shift.ToString() };
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public Double Envelopes(String symbol, TimeFrame timeFrame, int period, MovingAverageType type, int ma_shift, AppliedPrice appliedPrice, double deviation, IndicatorLine indicatorLine, int shift)
        {
            String[] message = new String[] { "Envelopes", symbol, ((Int32)timeFrame).ToString(), period.ToString(), ((int)type).ToString(), ma_shift.ToString(), ((int)appliedPrice).ToString(), deviation.ToString(), ((int)indicatorLine).ToString(), shift.ToString() };
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public Double Atr(String symbol, TimeFrame timeFrame, int period,int shift)
        {
            String[] message = new String[] { "Atr", symbol, ((Int32)timeFrame).ToString(), period.ToString(),shift.ToString() };
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public Double Adx(String symbol, TimeFrame timeFrame, int period, AppliedPrice appliedPrice, IndicatorLine indicatorLine, int shift)
        {
            String[] message = new String[] { "Adx", symbol, ((Int32)timeFrame).ToString(), period.ToString(), ((int)appliedPrice).ToString(), ((int)indicatorLine).ToString(), shift.ToString() };
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public Double Lowest(String symbol, TimeFrame timeFrame, SeriesType type, int count, int from)
        {
            String[] message = new String[] { "lowest", symbol, ((Int32)timeFrame).ToString(), ((int)type).ToString(), count.ToString(), from.ToString()};

            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public Double Highest(String symbol, TimeFrame timeFrame, SeriesType type, int count, int from)
        {
            String[] message = new String[] { "highest", symbol, ((Int32)timeFrame).ToString(), ((int)type).ToString(), count.ToString(), from.ToString() };

            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public Double High(String symbol, TimeFrame timeFrame, int shift)
        {
            String[] message = new String[] { "ihigh", symbol, ((Int32)timeFrame).ToString(), shift.ToString() };

            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public Double Low(String symbol, TimeFrame timeFrame, int shift)
        {
            String[] message = new String[] { "ilow", symbol, ((Int32)timeFrame).ToString(), shift.ToString() };

            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public Double Open(String symbol, TimeFrame timeFrame, int shift)
        {
            String[] message = new String[] { "iopen", symbol, ((Int32)timeFrame).ToString(), shift.ToString() };

            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public Double Volume(String symbol, TimeFrame timeFrame, int shift)
        {
            String[] message = new String[] { "volume", symbol, ((Int32)timeFrame).ToString(), shift.ToString() };

            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public Double MediumMinMovement(String symbol, TimeFrame timeFrame, int period)
        {
            Double sum = 0;
            for (int i = 1; i <= period; i++)
            {
                Double open = Open(symbol, timeFrame,i);
                Double high = High(symbol, timeFrame, i);
                Double low = Low(symbol, timeFrame,i);

                Double highDiff = Math.Abs(high - open);
                Double lowDiff = Math.Abs(open - low);

                if (highDiff > lowDiff)
                {
                    sum += lowDiff;
                }
                else
                {
                    sum += highDiff;
                }
            }
            return sum / period;
        }

        public Double MediumMaxMovement(String symbol, TimeFrame timeFrame, int period)
        {
            Double sum = 0;
            for (int i = 1; i <= period; i++)
            {
                Double open = Open(symbol, timeFrame, i);
                Double high = High(symbol, timeFrame, i);
                Double low = Low(symbol, timeFrame, i);


                Double highDiff = Math.Abs(high - open);
                Double lowDiff = Math.Abs(open - low);

                if (highDiff > lowDiff)
                {
                    sum += highDiff;
                }
                else
                {
                    sum += lowDiff;
                }
            }
            return sum / period;
        }
    }
}
