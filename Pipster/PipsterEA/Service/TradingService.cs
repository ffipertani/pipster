﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using PipsterEA.Utils;

namespace PipsterEA.Service
{
    public class TradingService
    {
        NetworkService networkService = NetworkService.GetInstance();

        public Double AccountSize()
        {
            String[] message = new String[] { "accountSize"};
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);            
        }

        public DateTime getNow()
        {
            String[] message = new String[] { "timeCurrent" };
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            long time = long.Parse(response[0]);
            DateTime now = new DateTime(1970,1,1);
            now=  now.AddSeconds(time);
            return now;
        }

        public List<Order> ListOrders()
        {
            List<Order> orders = new List<Order>();
            String[] message = new String[] { "listOrders" };
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.unmarshalOrders(response);
        }

        
        public Order LoadOrder(int ticket)
        {
            String[] message = new String[] { "loadOrder", ticket.ToString() };
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            List<Order> toreturn =  Conversion.unmarshalOrders(response);
            if (toreturn.Count > 0)
            {
                return toreturn[0];
            }
            return null;
        }
       
        public Boolean CloseOrder(int ticket, double lots, double price)
        {
            String[] message = new String[] { "closeOrder",ticket.ToString(),Conversion.getString(lots),Conversion.getString(price)};
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            if (response[0].Equals("OK"))
            {
                return true;
            }
            else
            {
                return false;
            }            
        }

        public void DeleteOrder(int ticket)
        {
            String[] message = new String[] { "deleteOrder", ticket+ ""};
            networkService.writeCommand(message);
        }

        public void EditOrder(Order order)
        {
            String[] strOrder = Conversion.marshalOrder(order);
            String[] message = new String[strOrder.Length + 1];
            message[0] = "editOrder";
            strOrder.CopyTo(message, 1);
            networkService.writeCommand(message);
        }

        public int OpenOrder(Order order) 
        {
            Double sl = order.StopLoss;
            Double tp = order.TakeProfit;
            order.StopLoss = 0;
            order.TakeProfit = 0;

            String[] strOrder = Conversion.marshalOrder(order);
            String[] message = new String[strOrder.Length + 1];
            message[0] = "openOrder";
            strOrder.CopyTo(message, 1);
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();

            int ticket = Int32.Parse(response[0]);
            if (ticket > 0)
            {
                order.Ticket = ticket;
                order.StopLoss = sl;
                order.TakeProfit = tp;
                EditOrder(order);
            }
            return ticket;
        }

        public Double Ask(String symbol)
        {
            String[] message = new String[] { "ask", symbol};
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public Double Bid(String symbol)
        {
            String[] message = new String[] { "bid", symbol };
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public Double Point(String symbol)
        {
            String[] message = new String[] { "point", symbol };
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0])*10;
        }

        public Double Digits(String symbol)
        {
            String[] message = new String[] { "digits", symbol };
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public Double Spread(String symbol)
        {
            String[] message = new String[] { "spread", symbol };
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDouble(response[0]);
        }

        public DateTime Time(String symbol, TimeFrame timeFrame, int shift)
        {
            String[] message = new String[] { "time", symbol, shift.ToString() };
            networkService.writeCommand(message);
            String[] response = networkService.readResponse();
            return Conversion.getDate(response[0]);
        }


    }
}
