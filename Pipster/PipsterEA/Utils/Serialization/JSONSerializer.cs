﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace PipsterEA.Utils.Serialization
{
    public class JSONSerializer
    {
        public static object DeserializeObject(String text, Type type)
        {
           return JavaScriptConvert.DeserializeObject(text, type);
        }

        public static String SerializeObject(object o)
        {
            return JavaScriptConvert.SerializeObject(o);
        }
    }
}
