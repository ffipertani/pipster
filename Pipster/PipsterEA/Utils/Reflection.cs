﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;

namespace PipsterEA.Utils
{
    public class Reflection
    {

        public static Object getInstance(String dllName, String className)
        {
            FileInfo fi = new FileInfo(dllName);

            Assembly moduleAssembly = Assembly.LoadFile(fi.FullName);

            return moduleAssembly.CreateInstance(className);          
        }

        public static List<Object> getAllInstance(String dllName)
        {
            List<Object> objs = new List<Object>();
            FileInfo fi = new FileInfo(dllName);
            Assembly moduleAssembly = Assembly.LoadFile(fi.FullName);
            Type[] types = moduleAssembly.GetTypes();

            foreach (Type type in types)
            {
                try
                {
                    Object o = Activator.CreateInstance(type);
                    objs.Add(o);
                }catch(Exception e)
                {
                    //NOTHING TO DO
                }
            }

            return objs;
        }

        public static Object Clone(Object o)
        {                      
            Object dest = Activator.CreateInstance(o.GetType());
           
            foreach (PropertyInfo pid in o.GetType().GetProperties())
            {                
                if (pid.CanWrite)
                {
                    pid.SetValue(dest, pid.GetValue(o,null), null);
                }                               
            }

            return dest;
         }
    }
}
