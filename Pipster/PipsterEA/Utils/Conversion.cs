﻿using System;
using System.Collections.Generic;
using System.Text;
using PipsterEA.Model;
using System.Globalization;

namespace PipsterEA.Utils
{
    public class Conversion
    {
        private static int ORDERS_SIZE = 16;
       
        public static List<Order> unmarshalOrders(String[] response)
        {
            List<Order> orders = new List<Order>();
            int count = (response.Length) / ORDERS_SIZE;
            for (int i = 0; i < count; i++)
            {
                Order order = new Order();
                order.ClosePrice = Conversion.getDouble(response[0 + (i * ORDERS_SIZE)]);
                order.Comment = response[1 + (i * ORDERS_SIZE)];
                order.Commission = Conversion.getDouble(response[2 + (i * ORDERS_SIZE)]);
                order.Expiration = Conversion.getDate(response[3 + (i * ORDERS_SIZE)]);
                order.Lots = Conversion.getDouble(response[4 + (i * ORDERS_SIZE)]);
                order.MagicNumber = Int32.Parse(response[5 + (i * ORDERS_SIZE)]);
                order.OpenPrice = Conversion.getDouble(response[6 + (i * ORDERS_SIZE)]);
                order.OpenTime = Conversion.getDate(response[7 + (i * ORDERS_SIZE)]);
                order.Profit = Conversion.getDouble(response[8 + (i * ORDERS_SIZE)]);
                order.StopLoss = Conversion.getDouble(response[9 + (i * ORDERS_SIZE)]);
                order.Swap = Conversion.getDouble(response[10 + (i * ORDERS_SIZE)]);
                order.Symbol = response[11 + (i * ORDERS_SIZE)];
                order.TakeProfit = Conversion.getDouble(response[12 + (i * ORDERS_SIZE)]);
                order.Ticket = Int32.Parse(response[13 + (i * ORDERS_SIZE)]);
                order.Type = (TradeOperation)Int32.Parse(response[14 + (i * ORDERS_SIZE)]);
                order.CloseTime = Conversion.getDate(response[15 + (i * ORDERS_SIZE)]);
                orders.Add(order);
            }
            return orders;
        }

        public static String[] marshalOrders(List<Order> orders)
        {
            String[] result = new String[orders.Count * ORDERS_SIZE];
            for (int i = 0; i < orders.Count; i++)
            {
                Order order = orders[i];
                result[0 + (i * ORDERS_SIZE)] = getString(order.ClosePrice);
                result[1 + (i * ORDERS_SIZE)] = order.Comment;
                result[2 + (i * ORDERS_SIZE)] = getString(order.Commission);
                result[3 + (i * ORDERS_SIZE)] = getString(order.Expiration);
                result[4 + (i * ORDERS_SIZE)] = getString(order.Lots);
                result[5 + (i * ORDERS_SIZE)] = getString(order.MagicNumber);
                result[6 + (i * ORDERS_SIZE)] = getString(order.OpenPrice);
                result[7 + (i * ORDERS_SIZE)] = getString(order.OpenTime);
                result[8 + (i * ORDERS_SIZE)] = getString(order.Profit);
                result[9 + (i * ORDERS_SIZE)] = getString(order.StopLoss);
                result[10 + (i * ORDERS_SIZE)] = getString(order.Swap);
                result[11 + (i * ORDERS_SIZE)] = order.Symbol;
                result[12 + (i * ORDERS_SIZE)] = getString(order.TakeProfit);
                result[13 + (i * ORDERS_SIZE)] = getString(order.Ticket);
                result[14 + (i * ORDERS_SIZE)] = getString((Int32)order.Type);
                result[15 + (i * ORDERS_SIZE)] = getString(order.CloseTime);
            }
            return result;
        }

        public static String[] marshalOrder(Order order)
        {
            return marshalOrders(new List<Order>() { order });
        }

        public static Double getDouble(String s)
        {
            try
            {

                return Double.Parse(s, CultureInfo.InvariantCulture);
            }
            catch (Exception e)
            {
            }
            return 0;
        }


        public static DateTime getDate(String s)
        {
            try
            {
                return DateTime.Parse(s);                  
            }
            catch (Exception e)
            {
               // System.Windows.Forms.MessageBox.Show(e.ToString()) ;
            }
            return new DateTime(0);
        }

        public static DateTime getTime(String s)
        {
            try
            {
                return new DateTime(long.Parse(s));
            }
            catch (Exception e)
            {
                // System.Windows.Forms.MessageBox.Show(e.ToString()) ;
            }
            return new DateTime(0);
        }

        public static String getString(Double d)
        {
            return d.ToString(CultureInfo.InvariantCulture);
        }

        public static String getString(Int32 i)
        {
            return i.ToString();
        }

        public static String getString(DateTime date)
        {
            if (date != null)
            {
                return date.ToString();
            }
            else return "";
        }
    }
}
